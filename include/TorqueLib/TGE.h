// TGE.h: Interface to core TGE functions.
// Usable both from plugins and PluginLoader.dll.

#ifndef TORQUELIB_TGE_H
#define TORQUELIB_TGE_H

#include <cstdarg>
#include <cstring>

#include "platform/platform.h"
#include "math/mMath.h"
#include "math/color.h"
#include "util/tVector.h"

#ifdef _DEBUG
#define DEBUG_PRINTF(fmt, ...) TGE::Con::printf(fmt, __VA_ARGS__)
#define DEBUG_WARNF(fmt, ...)  TGE::Con::warnf(fmt, __VA_ARGS__)
#define DEBUG_ERRORF(fmt, ...) TGE::Con::Berrorf(fmt, __VA_ARGS__)
#else
#define DEBUG_PRINTF(fmt, ...)
#define DEBUG_WARNF(fmt, ...)
#define DEBUG_ERRORF(fmt, ...)
#endif

// Determine macros and addresses to use based on host OS
#if defined(_WIN32)
#include "win32/InterfaceMacros-win32.h"
#include "win32/Addresses-win32.h"
#elif defined(__APPLE__)
#include "linux/InterfaceMacros-linux.h"
#include "osx/Addresses-osx.h"
#elif defined(__linux)
#include "linux/InterfaceMacros-linux.h"
#include "linux/Addresses-linux.h"
#endif

namespace TGE
{
	// Class prototypes
	class SimObject;
	class BaseMatInstance;
	class GameConnection;
	class Camera;
	class ResourceObject;
	class NetConnection;
	class BitStream;
	class LocalBitStream;
	struct Move;
}

// TGE callback types
typedef const char* (*StringCallback)(TGE::SimObject *obj, S32 argc, const char *argv[]);
typedef S32         (*IntCallback)   (TGE::SimObject *obj, S32 argc, const char *argv[]);
typedef F32         (*FloatCallback) (TGE::SimObject *obj, S32 argc, const char *argv[]);
typedef void        (*VoidCallback)  (TGE::SimObject *obj, S32 argc, const char *argv[]);
typedef bool        (*BoolCallback)  (TGE::SimObject *obj, S32 argc, const char *argv[]);

typedef U32 SimObjectId;
typedef S32 NetSocket;

namespace TGE
{
	enum Constants {
	MaxConnectArgs = 16,
	MinRequiredProtocolVersion = 24,
	CurrentProtocolVersion = 24
	};

	class BitSet32
	{
	private:
		/// Internal representation of bitset.
		U32 mbits;

	public:
		BitSet32()                         { mbits = 0; }
		BitSet32(const BitSet32& in_rCopy) { mbits = in_rCopy.mbits; }
		BitSet32(const U32 in_mask)        { mbits = in_mask; }

		operator U32() const               { return mbits; }
		U32 getMask() const                { return mbits; }

		/// Set all bits to true.
		void set()                         { mbits = 0xFFFFFFFFUL; }

		/// Set the specified bit(s) to true.
		void set(const U32 m)              { mbits |= m; }

		/// Masked-set the bitset; ie, using s as the mask and then setting the masked bits
		/// to b.
		void set(BitSet32 s, bool b)       { mbits = (mbits&~(s.mbits)) | (b ? s.mbits : 0); }

		/// Clear all bits.
		void clear()                       { mbits = 0; }

		/// Clear the specified bit(s).
		void clear(const U32 m)            { mbits &= ~m; }

		/// Toggle the specified bit(s).
		void toggle(const U32 m)           { mbits ^= m; }

		/// Are any of the specified bit(s) set?
		bool test(const U32 m) const       { return (mbits & m) != 0; }

		/// Are ALL the specified bit(s) set?
		bool testStrict(const U32 m) const { return (mbits & m) == m; }

		/// @name Operator Overloads
		/// @{
		BitSet32& operator =(const U32 m)  { mbits = m;  return *this; }
		BitSet32& operator|=(const U32 m)  { mbits |= m; return *this; }
		BitSet32& operator&=(const U32 m)  { mbits &= m; return *this; }
		BitSet32& operator^=(const U32 m)  { mbits ^= m; return *this; }

		BitSet32 operator|(const U32 m) const { return BitSet32(mbits | m); }
		BitSet32 operator&(const U32 m) const { return BitSet32(mbits & m); }
		BitSet32 operator^(const U32 m) const { return BitSet32(mbits ^ m); }
		/// @}
	};


	class SimGroup;
	class Notify;
	class Namespace;
	class SimFieldDictionary;
	class PacketNotify;
	class NetEventNote;
	class ConnectionStringTable;
	class GhostInfo;
	class NetObject;
	class SimObject;
	class AbstractClassRep;
	typedef const char* StringTableEntry;

	struct NetAddress {
		int type;        ///< Type of address (IPAddress currently)

		/// Acceptable NetAddress types.
		enum {
			IPAddress,
			IPXAddress
		};

		U8 netNum[4];    ///< For IP:  sin_addr<br>
							///  For IPX: sa_netnum

		U16  port;       ///< For IP:  sin_port<br>
							///  For IPX: sa_socket
	};

	// TODO: impl ref logic
	template <class T> class SimObjectPtr
	{
	public:
		T *mObj;
		SimObjectPtr() { mObj = 0; }
		SimObjectPtr(T* ptr) { mObj = ptr; }
	};


	enum EventConstants
	{
		MaxPacketDataSize = 1500,    ///< Maximum allowed size of a packet.
		MaxConsoleLineSize = 512     ///< Maximum allowed size of a console line.
	};

    enum NetClassGroups {
        NetClassGroupGame = 0,
        NetClassGroupCommunity,
        NetClassGroup3,
        NetClassGroup4,
        NetClassGroupsCount,
    };

	class ConsoleObject
	{
	public:
		VTABLE(TGEOFF_CONSOLEOBJECT_VTABLE);
		
		VIRTFNSIMP(AbstractClassRep*, getClassRep, TGEVIRT_CONSOLEOBJECT_GETCLASSREP);
		VIRTDTOR(~ConsoleObject, TGEVIRT_CONSOLEOBJECT_DESTRUCTOR);

		STATIC_FN(ConsoleObject*, create, (const char* name), (name), TGEADDR_CONSOLEOBJECT_CREATE);
	};

	class SimFieldDictionary
	{
		public:
			struct Entry
			{
				StringTableEntry slotName;
				char *value;
				Entry *next;
			};
		enum
		{
			HashTableSize = 19
		};
		Entry *mHashTable[HashTableSize];
		U32 mVersion;

		public:

		inline S32 HashPointer(StringTableEntry ptr)
		{
			return S32(U32(ptr) >> 2);
		}

		inline U32 getHashValue( StringTableEntry slotName )
		{
			return HashPointer( slotName ) % HashTableSize;
		}

		inline const char *getFieldValue(StringTableEntry slotName)
		{
			U32 bucket = getHashValue(slotName);

			for(Entry *walk = mHashTable[bucket];walk;walk = walk->next)
				if(walk->slotName == slotName)
					return walk->value;

			return NULL;
		}
    };

	class SimObject: public ConsoleObject
	{
	public:

        /// Flags for use in mFlags
        enum {
            Deleted = BIT(0),   ///< This object is marked for deletion.
            Removed = BIT(1),   ///< This object has been unregistered from the object system.
            Added = BIT(3),   ///< This object has been registered with the object system.
            Selected = BIT(4),   ///< This object has been marked as selected. (in editor)
            Expanded = BIT(5),   ///< This object has been marked as expanded. (in editor)
            ModStaticFields = BIT(6),    ///< The object allows you to read/modify static fields
            ModDynamicFields = BIT(7)     ///< The object allows you to read/modify dynamic fields
        };


		// FIELDS
		SimGroup* mGroup;
		const char* objectName;
		SimObject* nextNameObject;
		SimObject* nextManagerNameObject;
		SimObject* nextIdObject;
		U32 mFlags;
		void* mNotifyList;
		U32 mId;
		Namespace* mNameSpace;
		U32 mTypeTask;
		SimFieldDictionary* mFieldDictionary;
		bool mCanSaveFieldDictionary;
		const char* mInternalName;

		GETTERFNSIMP(SimObjectId, getId, TGEOFF_SIMOBJECT_ID);
		MEMBERFNSIMP(const char*, getIdString, TGEADDR_SIMOBJECT_GETIDSTRING);
		MEMBERFN(void, setHidden, (bool hidden), (hidden), TGEADDR_SIMOBJECT_SETHIDDEN);

        MEMBERFNSIMP(bool, registerObject, TGEADDR_SIMOBJECT_REGISTEROBJECT);
		MEMBERFNSIMP(void, deleteObject, TGEADDR_SIMOBJECT_DELETEOBJECT);

		MEMBERFN(void, getDataField, (StringTableEntry slotName, const char *array), (slotName, array), TGEADDR_SIMOBJECT_GETDATAFIELD);
        VIRTFN(void, setDataField, (SimObject *obj, StringTableEntry slotName, const char *array, const char *value), (obj, slotName, array, value), TGEVIRT_SIMOBJECT_SETDATAFIELD);
		MEMBERFN(void, assignName, (const char* name), (name), TGEADDR_SIMOBJECT_ASSIGNNAME);


		VIRTFN(void, writeField, (StringTableEntry fieldName, const char* value), (fieldName, value), TGEVIRT_SIMOBJECT_WRITEFIELD);

        inline bool isProperlyAdded() {
            return (mFlags & Added) != 0;
        }

        inline void setInternalName(const char* newname);
	};

	class SimObjectList: public VectorPtr<SimObject*>
	{
	public:
		SimObject *at(S32 index) const
		{
			if (index >= 0 && index < size())
				return (*this)[index];
			return NULL;
		}
	};

	class SimSet: public SimObject
	{
	public:
		// FIELDS
		SimObjectList objectList;
		void* mMutex;
		// END FIELDS

		UNDEFVIRT(onRemove);
		UNDEFVIRT(onDeleteNotify);
		UNDEFVIRT(addObject);
		UNDEFVIRT(removeObject);
		UNDEFVIRT(pushObject);
		UNDEFVIRT(popObject);
		UNDEFVIRT(write);
		UNDEFVIRT(findObject);
	};

	class SimNameDictionary
	{
	public:
		// FIELDS
		SimObject** hashTable;
		int hashTableSize;
		int hashEntryCount;
		void* mutex;
		// END FIELDS
	};

	class SimGroup: public SimSet
	{
		SimNameDictionary nameDictionary;
	};

	class SimDataBlock: public SimObject
	{
		S32  modifiedKey;
		bool mServerSide;
		U32 mRefCount;
		bool mTransmit;
	};

	class GuiControlProfile;
	class LangTable;

	class GuiControl: public SimGroup
	{
	public:

		// FIELDS
		const char *mClassName;
		const char* mSuperClassName;
		GuiControlProfile* mProfile;
		GuiControlProfile* mTooltipProfile;
		int mTopHoverTime;
		bool mVisible;
		bool mActive;
		bool mAwake;
		bool mSetFirstResponder;
		bool mCanSave;
		bool mIgnoreMouse;
		int mLayer;
		RectI mBounds;
		Point2I mMinExtent;
		const char* mLangTableName;
		LangTable* mLangTable;
		GuiControl *mFirstResponder;
		int mHorizSizing;
		int mVertSizing;
		const char* mConsoleVariable;
		const char* mConsoleCommand;
		const char* mAltConsoleCommand;
		const char* mRightMouseCommand;
		const char* mAcceleratorKey;
		const char* mTooltip;
		// END FIELDS 

		GETTERFNSIMP(RectI, getBounds, TGEOFF_GUICONTROL_BOUNDS);
		GETTERFNSIMP(Point2I, getPosition, TGEOFF_GUICONTROL_POSITION);
		GETTERFNSIMP(Point2I, getExtent, TGEOFF_GUICONTROL_EXTENT);
	};

	struct GuiEvent
	{
		U16      ascii;             ///< ascii character code 'a', 'A', 'b', '*', etc (if device==keyboard) - possibly a uchar or something
		U8       modifier;          ///< SI_LSHIFT, etc
		U8       keyCode;           ///< for unprintables, 'tab', 'return', ...
		Point2I  mousePoint;        ///< for mouse events
		U8       mouseClickCount;   ///< to determine double clicks, etc...
	};

	class GuiCursor;

	class GuiCanvas: public GuiControl
	{
	public:
		///
		RectI      mOldUpdateRects[2];
		RectI      mCurUpdateRect;
		F32        rLastFrameTime;

		F32         mPixelsPerMickey;

		bool        cursorON;
		bool        mShowCursor;
		bool        mRenderFront;
		Point2F     cursorPt;
		Point2I     lastCursorPt;
		GuiCursor   *defaultCursor;
		GuiCursor   *lastCursor;
		bool        lastCursorON;

		SimObjectPtr<GuiControl>   mMouseCapturedControl;
		SimObjectPtr<GuiControl>   mMouseControl;
		SimObjectPtr<GuiControl>   mMouseDragControl; 
		
		bool                       mMouseControlClicked;
		U32                        mPrevMouseTime;
		U32                        mNextMouseTime;
		U32                        mInitialMouseDelay;
		bool                       mMouseButtonDown;
		bool                       mMouseRightButtonDown;
		bool                       mMouseMiddleButtonDown;
		GuiEvent                   mLastEvent;

		U8                         mLastMouseClickCount;
		S32                        mLastMouseDownTime;
		bool                       mLeftMouseLast;
		bool                       mMiddleMouseLast;


		GuiControl   *keyboardControl;                     ///<  All keyboard events will go to this ctrl first
		U32          nextKeyTime;

		struct AccKeyMap
		{
			GuiControl *ctrl;
			U32 index;
			U32 keyCode;
			U32 modifier;
		};
		Vector <AccKeyMap> mAcceleratorMap;

		U32 hoverControlStart;
		GuiControl* hoverControl;
		Point2I hoverPosition;
		bool hoverPositionSet;
		U32 hoverLeftControlTime;
	};

	class NetObject: public SimObject
	{
	public:

		U32 mDirtyMaskBits;
		NetObject* mPrevDirtyList;
		NetObject* mNextDirtyList;
		SimObjectPtr<NetObject> mServerObject;
		BitSet32 mNetFlags;
		U32 mNetIndex;
		GhostInfo* mFirstObjectRef;  

		UNDEFVIRT(getUpdatePriority);
		VIRTFN(U32, packUpdate, (NetConnection *conn, U32 mask, BitStream *stream), (conn, mask, stream), TGEVIRT_NETOBJECT_PACKUPDATE);
		VIRTFN(void, unpackUpdate, (NetConnection *conn, BitStream *stream), (conn, stream), TGEVIRT_NETOBJECT_UNPACKUPDATE);
		UNDEFVIRT(onCameraScopeQuery);
	};

	class ContainerLink
	{
	public:
		ContainerLink* prev;
		ContainerLink* next;
	};

	class LightingInfo
	{
	public:
		bool mUseInfo;
		ColorF mDefaultColor;
		bool mHasLastColor;
		ColorF mLastColor;
		U32 mLastTime;
	};

	class Container;
	class SceneGraph;
	class SceneObjectRef;
	class SceneState;

	class SceneObject: public NetObject, public ContainerLink
	{
	public:

		bool overrideOptions;
		bool receiveLMLighting;
		bool receiveSunLight;
		bool useAdaptiveSelfIllumination;
		bool useCustomAmbientLighting;
		bool customAmbientForSelfIllumination;
		bool noShadeLightmap;

		ColorF customAmbientLighting;
		StringTableEntry lightGroupName;
		Vector<S32> lightIds;

		bool useLightingOcclusion;
		U32 moveSnapshotId;
		LightingInfo mLightingInfo;

		Container* mContainer;
		MatrixF mObjToWorld;
		MatrixF mWorldToObj;
		Point3F mObjScale;
		Box3F mObjBox;
		Box3F mWorldBox;
		SphereF mWorldSphere;
		MatrixF mRenderObjToWorld;
		MatrixF mRenderWorldToObj;
		Box3F mRenderWorldBox;
		SphereF mRenderWorldSphere;
		SceneObjectRef* mZoneRefHead;
		SceneObjectRef* mBinRefHead;
		U32 mBinMinX;
		U32 mBinMaxX;
		U32 mBinMinY;
		U32 mBinMaxY;
		U32 mContainerSeqKey;
		S32 mCollisionCount;
		bool mGlobalBounds;
		SceneGraph* mSceneManager;
		U32 mZoneRangeStart;
		U32 mNumCurrZones;
		U32 mTraversalState;
		SceneState* mLastState;
		U32 mLastStateKey;

		VIRTFNSIMP(void, disableCollision, TGEVIRT_SCENEOBJECT_DISABLECOLLISION);
		VIRTFNSIMP(void, enableCollision, TGEVIRT_SCENEOBJECT_ENABLECOLLISION);
		UNDEFVIRT(isDisplacable);
		UNDEFVIRT(getMass);
		UNDEFVIRT(displaceObject);
		VIRTFN(void, setTransform, (const MatrixF &transform), (transform), TGEVIRT_SCENEOBJECT_SETTRANSFORM);
		UNDEFVIRT(setScale);
		UNDEFVIRT(setRenderTransform);
		UNDEFVIRT(buildConvex);
		UNDEFVIRT(buildPolyList);
		UNDEFVIRT(buildCollisionBSP);
		UNDEFVIRT(castRay);
		UNDEFVIRT(collideBox);
		UNDEFVIRT(getOverlappingZones);
		UNDEFVIRT(getPointZone);
		UNDEFVIRT(renderShadowVolumes);
		UNDEFVIRT(renderObject);
		UNDEFVIRT(prepRenderImage);
		UNDEFVIRT(scopeObject);
		UNDEFVIRT(getMaterialProperty);
		UNDEFVIRT(onSceneAdd);
		UNDEFVIRT(onSceneRemove);
		UNDEFVIRT(transformModelview);
		UNDEFVIRT(transformPosition);
		UNDEFVIRT(computeNewFrustum);
		UNDEFVIRT(openPortal);
		UNDEFVIRT(closePortal);
		UNDEFVIRT(getWSPortalPlane);
		UNDEFVIRT(installLights);
		UNDEFVIRT(uninstallLights);
		UNDEFVIRT(getLightingAmbientColor);

		GETTERFN(const MatrixF &, MatrixF, getTransform, TGEOFF_SCENEOBJECT_TRANSFORM);
		GETTERFN(const Box3F &, Box3F, getWorldBox, TGEOFF_SCENEOBJECT_WORLDBOX);
	};

	class GameBase;

	class GameBaseLink
	{
	public:
		GameBase* next;
		GameBase* prev;
	};

	class GameBaseData : public SimDataBlock
	{
	public:
		bool packed;
		StringTableEntry category;
		StringTableEntry className;
	};

	class GameBase : public SceneObject
	{
	public:
		GameBaseData* mDataBlock;
		StringTableEntry mNameTag;
		U32 mProcessTag;
		GameBaseLink mProcessLink;
		SimObjectPtr<GameBase> mAfterObject;
		GameConnection* mControllingClient;
		bool mProcessTick;
		F32 mLastDelta;
		F32 mCameraFov;

		GETTERFNSIMP(GameConnection*, getControllingClient, TGEOFF_GAMEBASE_CONTROLLINGCLIENT);

		UNDEFVIRT(onNewDataBlock);
		UNDEFVIRT(processTick);
		UNDEFVIRT(interpolateTick);
		UNDEFVIRT(advanceTime);
		UNDEFVIRT(advancePhysics);
		UNDEFVIRT(getVelocity);
		UNDEFVIRT(getForce);
		UNDEFVIRT(writePacketData);
		UNDEFVIRT(readPacketData);
		UNDEFVIRT(getPacketDataChecksum);
	};

	struct LightInfo
	{
		U32 mType;
		Point3F mPos;
		Point3F mDirection;
		ColorF mColor;
		ColorF mAmbient;
		F32 mRadius;
		S32 mScore;
		F32 sgSpotAngle;
		bool sgAssignedToTSObject;
		bool sgCastsShadows;
		bool sgDiffuseRestrictZone;
		bool sgAmbientRestrictZone;
		S32 sgZone[2];
		float sgLocalAmbientAmount;
		bool sgSmoothSpotLight;
		bool sgDoubleSidedAmbient;
		bool sgAssignedToParticleSystem;
		StringTableEntry sgLightingModelName;
		bool sgUseNormals;
		MatrixF sgLightingTransform;
		PlaneF sgSpotPlane;
		Point3F sgTempModelInfo;
		F32 sgDTSLightingOcclusionAdjust;
		S32 sgMoveSnapshotId;
		bool sgTrackMoveSnapshot;
	};

	class AudioProfile;
	class ShapeBaseData;
	class ParticleEmitter;
	class TSThread;
	class TSShapeInstance;
	class Convex;
	class CollisionTimeout;
	typedef U32 SimTime;

	class ShapeBase : public GameBase
	{
	public:

		struct Sound
		{
			bool play;
			SimTime timeout;
			AudioProfile* profile;
			U32 sound;
		};

		struct Thread
		{
			char data[40];
		};

		struct MountedImage
		{
			char data[312];
		};

		struct MountInfo
		{
			ShapeBase* list;
			ShapeBase* object;
			ShapeBase* link;
			U32 node;
		};

		ShapeBaseData* mDataBlock;
		ShapeBase* mControllingObject;
		bool mTrigger[6];
		Sound mSoundThread[2];
		Thread mScriptThread[4];

		F32 mInvincibleCount;
		F32 mInvincibleTime;
		F32 mInvincibleSpeed;
		F32 mInvincibleDelta;
		F32 mInvincibleEffect;
		F32 mInvincibleFade;
		bool mInvincibleOn;

		char shadows[252];

		U32 mPowerUpStack[4]; // OV
		S32 mPowerUpUpdate; // OV

		MountedImage mMountedImageList;
		TSShapeInstance* mShapeInstance;

		Convex* mConvexList;
		U32 mSkinNameHandle;
		U32 mShapeNameHandle;

		F32 mEnergy;
		F32 mRechargeRate;
		bool mChargeEnergy;
		
		F32 mMass;
		F32 mOneOverMass;
		F32 mDrag;
		F32 mBuoyancy;
		U32 mLiquidType;
		F32 mLiquidHeight;
		F32 mWaterCoverage;

		Point3F mAppliedForce;
		F32 mGravityMod;
		F32 mDamageFlash;
		F32 mWhiteOut;

		bool mFlipFadeVal;
		U32 mLightTime;

		Point3F mShieldNormal;

		MountInfo mMount;
		CollisionTimeout* mTimeoutList;

		F32 mDamage;
		F32 mArmor;
		F32 mRepairRate;
		F32 mRepairReserve;
		bool mRepairDamage;
		U32 mDamageState;
		TSThread* mDamageThread;
		TSThread* mHulkThread;
		Point3F damageDir;
		ParticleEmitter* mDamageEmitter; // OV
		bool colDisabled; // OV
		bool mTookDamage; // OV
		bool mCloaked;
		F32 mClockLevel;
		void* mCloakTexture;
		bool mHidden;
		bool mFadeOut;
		bool mFading;
		F32 mFadeVal;
		F32 mFadeElapsedTime;
		F32 mFadeTime;
		F32 mFadeDelay;
		F32 mCameraFov;
		bool mIsControlled;
		U32 mLastRenderFrame;
		F32 mLastRenderDistance;
		U32 mSkinHash;

		UNDEFVIRT(setImage);
		UNDEFVIRT(onImageRecoil);
		UNDEFVIRT(ejectShellCasing);
		UNDEFVIRT(updateDamageLevel);
		UNDEFVIRT(updateDamageState);
		UNDEFVIRT(blowUp);
		UNDEFVIRT(onMount);
		UNDEFVIRT(onUnmount);
		UNDEFVIRT(onImpact_SceneObject_Point3F);
		UNDEFVIRT(onImpact_Point3F);
		UNDEFVIRT(controlPrePacketSend);
		UNDEFVIRT(setEnergyLevel);
		UNDEFVIRT(mountObject);
		UNDEFVIRT(mountImage);
		UNDEFVIRT(unmountImage);
		UNDEFVIRT(getMuzzleVector);
		UNDEFVIRT(getCameraParameters);
		VIRTFN(void, getCameraTransform, (F32 *pos, MatrixF *mat), (pos, mat), TGEVIRT_SHAPEBASE_GETCAMERATRANSFORM);
		UNDEFVIRT(getEyeTransform);
		UNDEFVIRT(getRetractionTransform);
		UNDEFVIRT(getMountTransform);
		UNDEFVIRT(getMuzzleTransform);
		UNDEFVIRT(getImageTransform_uint_MatrixF);
		UNDEFVIRT(getImageTransform_uint_int_MatrixF);
		UNDEFVIRT(getImageTransform_uint_constchar_MatrixF);
		UNDEFVIRT(getRenderRetractionTransform);
		UNDEFVIRT(getRenderMountTransform);
		UNDEFVIRT(getRenderMuzzleTransform);
		UNDEFVIRT(getRenderImageTransform_uint_MatrixF);
		UNDEFVIRT(getRenderImageTransform_uint_int_MatrixF);
		UNDEFVIRT(getRenderImageTransform_uint_constchar_MatrixF);
		UNDEFVIRT(getRenderMuzzleVector);
		UNDEFVIRT(getRenderMuzzlePoint);
		UNDEFVIRT(getRenderEyeTransform);
		UNDEFVIRT(getDamageFlash);
		UNDEFVIRT(setDamageFlash);
		UNDEFVIRT(getWhiteOut);
		UNDEFVIRT(setWhiteOut);
		UNDEFVIRT(getInvincibleEffect);
		UNDEFVIRT(setupInvincibleEffect);
		UNDEFVIRT(updateInvincibleEffect);
		UNDEFVIRT(setVelocity);
		UNDEFVIRT(applyImpulse);
		UNDEFVIRT(setControllingClient);
		UNDEFVIRT(setControllingObject);
		UNDEFVIRT(getControlObject);
		UNDEFVIRT(setControlObject);
		UNDEFVIRT(getCameraFov);
		UNDEFVIRT(getDefaultCameraFov);
		UNDEFVIRT(setCameraFov);
		UNDEFVIRT(isValidCameraFov);
		UNDEFVIRT(renderMountedImage);
		UNDEFVIRT(renderImage);
		UNDEFVIRT(calcClassRenderData);
		UNDEFVIRT(onCollision);
		UNDEFVIRT(getSurfaceFriction);
		UNDEFVIRT(getBounceFriction);
		UNDEFVIRT(setHidden);
	};

	class Camera: public ShapeBase
	{
	};

	class InteriorInstance : public SceneObject
	{
	};

	class TSStatic : public SceneObject
	{
	};

	struct Collision
	{
		SceneObject* object;
		Point3F point;
		VectorF normal;
		BaseMatInstance* material;

		// Face and Face dot are currently only set by the extrudedPolyList
		// clipper.  Values are otherwise undefined.
		U32 face;                  // Which face was hit
		F32 faceDot;               // -Dot of face with poly normal
		F32 distance;

		Collision() :
			object(NULL),
			material(NULL)
		{
		}
	};

	/// Extension of the collision structure to allow use with raycasting.
	/// @see Collision
	struct RayInfo : public Collision
	{
		// The collision struct has object, point, normal & material.

		/// Distance along ray to contact point.
		F32 t;

		/// Set the point of intersection according to t and the given ray.
		///
		/// Several pieces of code will not use ray information but rather rely
		/// on contact points directly, so it is a good thing to always set
		/// this in castRay functions.
		void setContactPoint(const Point3F& start, const Point3F& end)
		{
			Point3F startToEnd = end - start;
			startToEnd *= t;
			point = startToEnd + start;
		}
	};

	class Container
	{
	public:
		MEMBERFN(bool, castRay, (const Point3F &start, const Point3F &end, U32 mask, RayInfo *info), (start, end, mask, info), TGEADDR_CONTAINER_CASTRAY);
	};

#define DECLARE_OVERLOADED_READ(type)      \
   bool read(type* out_read) {             \
      return _read(sizeof(type), out_read); \
	   }
#define DECLARE_OVERLOADED_WRITE(type)       \
   bool write(type in_write) {               \
      return _write(sizeof(type), &in_write); \
	   }

#define DECLARE_ENDIAN_OVERLOADED_READ(type)       \
   bool read(type* out_read) {                     \
      type temp;                                   \
      bool success = _read(sizeof(type), &temp);    \
      *out_read = (temp);      \
      return success;                              \
	   }
#define DECLARE_ENDIAN_OVERLOADED_WRITE(type)      \
   bool write(type in_write) {                     \
      type temp = (in_write);  \
      return _write(sizeof(type), &temp);           \
	   }

	class Stream
	{
	public:
		VTABLE(TGEOFF_STREAM_VTABLE);
		
		/// Status constants for the stream
		enum StreamStatus
		{
			Ok = 0,      ///< Ok!
			IOError,     ///< Read or Write error
			EOS,         ///< End of Stream reached (mostly for reads)
			IllegalCall, ///< An unsupported operation used. Always w/ accompanied by AssertWarn
			Closed,      ///< Tried to operate on a closed stream (or detached filter)
			UnknownError ///< Catchall
		};
		
		GETTERFNSIMP(StreamStatus, getStatus, TGEOFF_STREAM_STATUS);

		VIRTDTOR(~Stream, TGEVIRT_STREAM_DTOR);
		VIRTFN(bool, _read, (U32 size, void *buf), (size, buf), TGEVIRT_STREAM__READ);
		VIRTFN(bool, _write, (U32 size, const void *buf), (size, buf), TGEVIRT_STREAM__WRITE);
		VIRTFN(bool, hasCapability, (int capability), (capability), TGEVIRT_STREAM_HASCAPABILITY);
		VIRTFNSIMP(U32, getPosition, TGEVIRT_STREAM_GETPOSITION);
		VIRTFN(bool, setPosition, (U32 pos), (pos), TGEVIRT_STREAM_SETPOSITION);
		VIRTFNSIMP(U32, getStreamSize, TGEVIRT_STREAM_GETSTREAMSIZE);
		VIRTFN(void, readString, (char *str), (str), TGEVIRT_STREAM_READSTRING);
		VIRTFN(void, writeString, (const char *str, S32 maxLength), (str, maxLength), TGEVIRT_STREAM_WRITESTRING);



		DECLARE_OVERLOADED_WRITE(S8)
		DECLARE_OVERLOADED_WRITE(U8)

		DECLARE_ENDIAN_OVERLOADED_WRITE(S16)
		DECLARE_ENDIAN_OVERLOADED_WRITE(S32)
		DECLARE_ENDIAN_OVERLOADED_WRITE(U16)
		DECLARE_ENDIAN_OVERLOADED_WRITE(U32)
		DECLARE_ENDIAN_OVERLOADED_WRITE(F32)

		DECLARE_OVERLOADED_READ(S8)
		DECLARE_OVERLOADED_READ(U8)

		DECLARE_ENDIAN_OVERLOADED_READ(S16)
		DECLARE_ENDIAN_OVERLOADED_READ(S32)
		DECLARE_ENDIAN_OVERLOADED_READ(U16)
		DECLARE_ENDIAN_OVERLOADED_READ(U32)
		DECLARE_ENDIAN_OVERLOADED_READ(F32)

		bool read(bool* out_pRead) {
			U8 translate;
			bool success = read(&translate);
			if (success == false)
				return false;

			*out_pRead = translate != 0;
			return true;
		}
		bool write(const bool& in_rWrite) {
			U8 translate = in_rWrite ? U8(1) : U8(0);
			return write(translate);
		}
	};

	class FileStream : public Stream
	{
	public:
		MEMBERFN(bool, open, (const char *path, int accessMode), (path, accessMode), TGEADDR_FILESTREAM_OPEN);
	};

	class File
	{
	public:
		/// What is the status of our file handle?
		enum FileStatus
		{
			Ok = 0,      ///< Ok!
			IOError,     ///< Read or Write error
			EOS,         ///< End of Stream reached (mostly for reads)
			IllegalCall, ///< An unsupported operation used. Always accompanied by AssertWarn
			Closed,      ///< Tried to operate on a closed stream (or detached filter)
			UnknownError ///< Catchall
		};

		/// How are we accessing the file?
		enum AccessMode
		{
			Read = 0,       ///< Open for read only, starting at beginning of file.
			Write = 1,      ///< Open for write only, starting at beginning of file; will blast old contents of file.
			ReadWrite = 2,  ///< Open for read-write.
			WriteAppend = 3 ///< Write-only, starting at end of file.
		};

		/// Flags used to indicate what we can do to the file.
		enum Capability
		{
			FileRead = 1 << 0,
			FileWrite = 1 << 1
		};

		VTABLE(0);

		~File()
		{
			close();
			handle = NULL;
		}

		void *handle;
		FileStatus currentStatus;
		U32 capability;
		
		MEMBERFN(void, setStatus, (FileStatus status), (status), TGEADDR_FILE_SETSTATUS_1); // Technically supposed to be protected
		GETTERFNSIMP(void*, getHandle, TGEOFF_FILE_HANDLE);
		SETTERFN(void*, setHandle, TGEOFF_FILE_HANDLE);
		GETTERFNSIMP(Capability, getCapabilities, TGEOFF_FILE_CAPABILITIES);
		SETTERFN(Capability, setCapabilities, TGEOFF_FILE_CAPABILITIES);

		MEMBERFN(FileStatus, open, (const char *filename, const AccessMode openMode), (filename, openMode), TGEADDR_FILE_OPEN);
		MEMBERFNSIMP(U32, getPosition, TGEADDR_FILE_GETPOSITION);
		MEMBERFN(FileStatus, setPosition, (S32 position, bool absolutePos), (position, absolutePos), TGEADDR_FILE_SETPOSITION);
		MEMBERFNSIMP(U32, getSize, TGEADDR_FILE_GETSIZE);
		MEMBERFNSIMP(FileStatus, flush, TGEADDR_FILE_FLUSH);
		MEMBERFNSIMP(FileStatus, close, TGEADDR_FILE_CLOSE);
		MEMBERFN(FileStatus, read, (U32 size, char *dst, U32 *bytesRead), (size, dst, bytesRead), TGEADDR_FILE_READ);
		MEMBERFN(FileStatus, write, (U32 size, const char *src, U32 *bytesWritten), (size, src, bytesWritten), TGEADDR_FILE_WRITE);

		inline FileStatus getStatus() { return currentStatus; }
	};

	class BitStream : public Stream
	{
	public:


		MEMBERFN(void, writeInt, (S32 value, S32 bitCount), (value, bitCount), TGEADDR_BITSTREAM_WRITEINT);
		MEMBERFN(S32, readInt, (S32 bitCount), (bitCount), TGEADDR_BITSTREAM_READINT);
	};

	//struct FileTime;

	class DataChunker
	{
	public:
		enum { ChunkSize = 16376 };

		struct DataBlock
		{
			DataBlock *next;
			U8 *data;
			S32 curIndex;
		};

		DataBlock *curBlock;
		S32 chunkSize;
	};


	class ResDictionary
	{
		enum { DefaultTableSize = 1029 };

		ResourceObject **hashTable;
		S32 entryCount;
		S32 hashTableSize;
		DataChunker memPool;
	};

	class ResourceInstance
	{
    public:
        ResourceObject* mSourceResource;
		VTABLE(TGEOFF_CONSOLEOBJECT_VTABLE);
	};

	class ResourceObject
	{
	public:
		ResourceObject *prev, *next;

		ResourceObject *nextEntry;

		ResourceObject *nextResource;
		ResourceObject *prevResource;

		enum Flags
		{
			VolumeBlock   = BIT(0),
			File          = BIT(1),
			Added         = BIT(2),
		};
		S32 flags;

		StringTableEntry path;
		StringTableEntry name;

		StringTableEntry zipPath;
		StringTableEntry zipName;

		S32 fileOffset;
		S32 fileSize;
		S32 compressedFileSize;

		ResourceInstance *mInstance;
		S32 lockCount;
		U32 crc;

		MEMBERFN(void, getFileTimes, (FileTime * createTime, FileTime * modifyTime), (createTime, modifyTime), TGEADDR_RESOBJECT_GETFILETIMES);
	};

	class ResManager
	{
	public:
		struct RegisteredExtension
		{
			StringTableEntry     mExtension;
			void*   mCreateFn;
			RegisteredExtension  *next;
		};

		char writeablePath[1024];
		char primaryPath[1024];
		char* pathList;

		ResourceObject timeoutList;
		ResourceObject resourceList;

		ResDictionary dictionary;
		bool echoFileNames;

		Vector<char *> mMissingFileList; 
		bool mLoggingMissingFiles; 
		RegisteredExtension *registeredList;

		MEMBERFN(Stream*, openStream, (const char *path), (path), TGEADDR_RESMANAGER_OPENSTREAM_STR);
		MEMBERFN(Stream*, openStream, (ResourceObject *obj), (obj), TGEADDR_RESMANAGER_OPENSTREAM_RESOURCEOBJECT);
		MEMBERFN(void, closeStream, (Stream *stream), (stream), TGEADDR_RESMANAGER_CLOSESTREAM);
		MEMBERFN(ResourceObject*, find, (const char *path), (path), TGEADDR_RESMANAGER_FIND);
		MEMBERFN(U32, getSize, (const char *path), (path), TGEADDR_RESMANAGER_GETSIZE);
		MEMBERFN(bool, getCrc, (const char *path, U32 &crc, U32 initialValue), (path, crc, initialValue), TGEADDR_RESMANAGER_GETCRC);
		MEMBERFN(void, searchPath, (const char *path), (path), TGEADDR_RESMANAGER_SEARCHPATH);
		MEMBERFN(bool, setModZip, (const char *path), (path), TGEADDR_RESMANAGER_SETMODZIP);
		MEMBERFN(void, setModPaths, (U32 numPaths, const char **paths), (numPaths, paths), TGEADDR_RESMANAGER_SETMODPATHS);

		inline void dumpResources();
	};

	class ConnectionProtocol
	{
	public:	
		U32 mLastSeqRecvdAtSend[32];
		U32 mLastSeqRecvd;
		U32 mHighestAckedSeq;
		U32 mLastSendSeq;
		U32 mAckMask;
		U32 mConnectSequence;
		U32 mLastRecvAckAck;
		bool mConnectionEstablished;

		VTABLE(0);


		VIRTFN(void, onConnectionEstablished, (bool isInitiator), (isInitiator), TGEVIRT_CONNECTIONPROTOCOL_ONCONNECTIONESTABLISHED);
		VIRTFNSIMP(void, setEstablished, TGEVIRT_CONNECTIONPROTOCOL_SETESTABLISHED);

	};
	
	GLOBALVAR(TGE::SimObject*, mServerConnection, TGEADDR_MSERVERCONNECTION);

	class NetConnection : public ConnectionProtocol, public SimGroup
	{
	public:

		enum NetConnectionFlags
		{
			ConnectionToServer = BIT(0),
			ConnectionToClient = BIT(1),
			LocalClientConnection = BIT(2),
			NetworkConnection = BIT(3),
		};

		// FIELDS
		NetConnection *mNextConnection;        ///< Next item in list.
		NetConnection *mPrevConnection;        ///< Previous item in list.

		BitSet32 mTypeFlags;

		U32 mNetClassGroup;  ///< The NetClassGroup of this connection.

		/// @name Statistics
		/// @{

		U32 mLastUpdateTime;
		F32 mRoundTripTime;

		// NEW TO ONVERSE
		U32 mRoundTripTimeStore[15];
		U32 mPacketsIn;
		U32 mPacketsDropped;
		// END ONVERSE

		F32 mPacketLoss;
		U32 mSimulatedPing;
		F32 mSimulatedPacketLoss;

		U32 mProtocolVersion;
		U32 mSendDelayCredit;
		U32 mConnectSequence__NC;
		U32 mAddressDigest[4];

		bool mTurbo;
		bool mEstablished; // @0x17d
		bool mMissionPathsSent;

		struct NetRate
		{
			U32 updateDelay;
			S32 packetSize;
			bool changed;
		};

		NetRate mCurRate;
		NetRate mMaxRate;

		NetConnection* mRemoteConnection;
		NetAddress mNetAddress;

		U32 mPingSendCount;
		U32 mPingRetryCount;
		U32 mLastPingSendTime;

		NetConnection *mNextTableHash;

		U32 mConnectSendCount;
		U32 mConnectLastSendTime;

		U32 mConnectionState;      ///< State of the connection, from NetConnectionState.

		PacketNotify *mNotifyQueueHead;  ///< Head of packet notify list.
		PacketNotify *mNotifyQueueTail;  ///< Tail of packet notify list.

		NetEventNote *mSendEventQueueHead;
		NetEventNote *mSendEventQueueTail;
		NetEventNote *mUnorderedSendEventQueueHead;
		NetEventNote *mUnorderedSendEventQueueTail;
		NetEventNote *mWaitSeqEvents;
		NetEventNote *mNotifyEventList;


		bool mSendingEvents; // @ 0x1e4

		S32 mNextSendEventSeq;
		S32 mNextRecvEventSeq;
		S32 mLastAckedEventSeq;

		bool mTranslateStrings;
		ConnectionStringTable *mStringTable;


		GhostInfo **mGhostArray;    ///< Linked list of ghostInfos ghosted by this side of the connection

		U32 mGhostZeroUpdateIndex;  ///< Index in mGhostArray of first ghost with 0 update mask.
		U32 mGhostFreeIndex;        ///< Index in mGhostArray of first free ghost.

		U32 mGhostsActive;			///- Track actve ghosts on client side

		bool mGhosting;             ///< Am I currently ghosting objects?
		bool mScoping;              ///< am I currently scoping objects?
		U32  mGhostingSequence;     ///< Sequence number describing this ghosting session.

		NetObject **mLocalGhosts;  ///< Local ghost for remote object.
									///
									/// mLocalGhosts pointer is NULL if mGhostTo is false

		GhostInfo *mGhostRefs;           ///< Allocated array of ghostInfos. Null if ghostFrom is false.
		GhostInfo **mGhostLookupTable;   ///< Table indexed by object id to GhostInfo. Null if ghostFrom is false.

		void* mScopeObject;

		Vector<const char*> mMissingFileList;

		Stream* mCurrentDownloadingFile;

		// ONVERSE STUFF
		U64 mFileTransferWindow;// = (mbits = 0)
		U32 mWindowOffset;// = 272283648
		U32 mDownTimeSeq;// = 310665216
		U32 mCurDownSeq;// = 0
		// END ONVERSE STUFF

		void* mCurrentFileBuffer;
		U32 mCurrentFileBufferSize;
		// ONVERSE STUFF
		U32 mHighestPosSent;
		// END ONVERSE STUFF
		U32 mNumDownloadedFiles;
		char mLastFileErrorBuffer[256];
   
		struct GhostSave {
			NetObject *ghost;
			U32 index;
		};

		Vector<GhostSave> mGhostAlwaysSave;

		Stream *mDemoWriteStream;
		Stream *mDemoReadStream;
		U32 mDemoNextBlockType;
		U32 mDemoNextBlockSize;

		U32 mDemoWriteStartTime;
		U32 mDemoReadStartTime;
		U32 mDemoLastWriteTime;

		U32 mDemoRealStartTime;
		// END FIELDS


		void setSequence(U32 seq) { mConnectSequence__NC = seq; }
		U32 getSequence() { return mConnectSequence__NC; }

		void setNetAddress(const NetAddress* addr)
		{
			mNetAddress = *addr;
		}

		const NetAddress* getNetAddress()
		{
			return &mNetAddress;
		}

		void setNetworkConnection(bool net)
		{
			mTypeFlags.set(BitSet32(NetworkConnection), net);
		}

		void setConnectSequence(U32 val)
		{
			mConnectSequence = val;
			mConnectSequence__NC = val;
		}

		SimObject* castToSimObject()
		{
			return (SimObject*)this;
		}

		STATIC_FN(NetConnection*, lookup, (const NetAddress *addr), (addr), TGEADDR_NETCONNECTION_LOOKUP);	
	};

	struct Move
	{
		S32 px, py, pz, pzoom;
		U32 pyaw, ppitch, proll;
		F32 x, y, z, zoom;          // float -1 to 1
		F32 yaw, pitch, roll; // 0-2PI
		U32 id;               // sync'd between server & client - debugging tool.
		U32 sendCount;

		bool freeLook;
		bool trigger[6];
	};

	class SimDataBlock;
	class ShapeBase;

	class GameConnection : public NetConnection
	{
	public:
		// BEGIN FIELDS
		SimObjectPtr<ShapeBase> mControlObject;
		SimObjectPtr<ShapeBase> mCameraObject;
		U32 mDataBlockSequence;
		char mDisconnectReason[256];
		U32 mMissionCRC;
		U32 mLastControlRequestTime;
		int mDataBlockModifiedKey;
		int mMaxDataBlockModifiedKey;
		bool mFirstPerson;
		bool mUpdateFirstPerson;
		bool mUpdateCameraFov;
		float mCameraFov;
		float mCameraPos;
		float mCameraRelPos;
		float mCameraTargPos;
		float mCameraSpeed;
		U32 mConnectArgc;
		char* mConnectArgv[16];
		char* mUsername; // @ 0x4fc MAC, 0x500 WIN (def is ok though)
		char* mPassword;
		U32 mUserAuthLevel;
		U32 mUserAccID; // @ 0x508 MAC
		bool mIsSubscriber; // @ 0x50c MAC, @0x510 WIN

		// Datablock cache related stuff (post-2012 builds)
#ifndef INTERNAL_BUILD
		struct DatablockCacheItem
		{
			U32 unk1;   // @ 0x0
			U16 unk1_2; // @ 0x4
			U16 dbID;   // @ 0x6
			U32 unk3;   // @ 0x8
			void* dbItemPtr; // @ 0xc
			bool itemProcessFlag; // @ 0x10
			bool pad[3]; // @ 0x11
		};
		Vector<DatablockCacheItem> *mDatablockCache; // @ 0x510 MAC  (sized at 16384 elements @ 0x14 bytes each)
		U32 numCacheDatablocks; // @ 0x514 =0
		U32 numCacheMisses; // @ 0x518 =0
		bool unk1; // @ 0x51c =false (=true in readyDatablockCache)
		bool dbCacheToProcess; // @ 0x51d =false (=false in processDatablockCache - seems to be proessing flag)
		void* llBlocks; // @ 0x520  (seems to be linked list of block ptrs? i.e. void*[16384])
#endif

		U32 mLastMoveAck; // @ 0x524 MAC (0x510 INT)
		U32 mLastClientMove;
		U32 mFirstMoveIndex;
		U32 mMoveCredit; // @ 0x530 MAC (0x51C INT)
		U32 mMoveLag;
		U32 mLastControlObjectChecksum;
		Vector<SimDataBlock*> mDataBlockLoadList;

		Vector<Move> mMoveList;
		bool mAIControlled;
		int mLastPacketTime;
		bool mLagging;     // @ 0x55c MAC (0x548 INT)
		float mDamageFlash;
		float mWhiteOut;
		float mBlackOut;   // @ 0x568 MAC (0x554 INT), 0x56c win   CUR 0x558 (missing 20 chars)
		int mBlackOutTimeMS;
		int mBlackOutStartTimeMS;
		bool mFadeToBlack;
		// END FIELDS
	};


	class TypeValidator;
	struct EnumTable;


	class AbstractClassRep
	{
    public:
        VTABLE(TGEOFF_CONSOLEOBJECT_VTABLE);

		typedef bool (*SetDataNotify)( void *obj, const char *data );
		typedef const char *(*GetDataNotify)( void *obj, const char *data );

        const char *       mClassName;
        AbstractClassRep * nextClass;
        AbstractClassRep * parentClass;
        Namespace *        mNamespace;

		enum ACRFieldTypes
		{
		  StartGroupFieldType = 0xFFFFFFFD,
		  EndGroupFieldType   = 0xFFFFFFFE,
		  DepricatedFieldType = 0xFFFFFFFF
		};

		struct Field {
		  const char* pFieldname;
		  const char* pGroupname;

		  const char*    pFieldDocs;
		  bool           groupExpand;
		  U32            type;
		  U32            foffset;
		  S32            elementCount;
		  EnumTable *    table;
		  BitSet32       flag;
		  TypeValidator *validator;
		  SetDataNotify  setDataFn;
		  GetDataNotify  getDataFn;
		};
		typedef Vector<Field> FieldList;

        FieldList mFieldList;

        bool mDynamicGroupExpand;

        S32 mClassGroupMask;                ///< Mask indicating in which NetGroups this object belongs.
        S32 mClassType;                     ///< Stores the NetClass of this class.
        S32 mNetEventDir;                   ///< Stores the NetDirection of this class.
        S32 mClassId[NetClassGroupsCount];  ///< Stores the IDs assigned to this class for each group.

		GETTERFNSIMP(const char*, getClassName, TGEOFF_ABSTRACTCLASSREP_CLASSNAME);
	};

	class _StringTable
	{
	public:
		MEMBERFN(StringTableEntry, insert, (const char *string, bool caseSens), (string, caseSens), TGEADDR__STRINGTABLE_INSERT);
	};

	namespace Memory
	{
		FN(void*, opNew, (U32 size), TGEADDR_MEMORY_NEW);
		FN(void*, opNewArray, (U32 size), TGEADDR_MEMORY_NEW_ARRAY);
		FN(void,  opFree, (void* mem), TGEADDR_MEMORY_FREE);
		FN(void,  opFreeArray, (void* mem), TGEADDR_MEMORY_FREE_ARRAY);
	};

	// Event types
	enum EventType
	{
		TimeEventType = 3,
	};

	// Event base structure
	struct Event
	{
		U16 type;
		U16 size;

		Event()
		{
			size = sizeof(Event);
		}
	};

	// Structure for time events
	struct TimeEvent : public Event
	{
		U32 deltaTime;

		TimeEvent()
		{
			type = TimeEventType;
			size = sizeof(TimeEvent);
		}
	};

	struct PacketReceiveEvent : public Event
	{
		NetAddress sourceAddress;   ///< Originating address.
		U8 data[MaxPacketDataSize]; ///< Payload
		PacketReceiveEvent() { type = 2; }
	};

	class GameInterface
	{
	public:
		VTABLE(TGEOFF_GAMEINTERFACE_VTABLE);

		MEMBERFN(void, journalReadU32, (U32* ptr), (ptr), TGEADDR_GAME_JOURNALREADU32);
		MEMBERFN(void, journalWriteU32, (U32 value), (value), TGEADDR_GAME_JOURNALREADBYTES);
		MEMBERFN(void, journalReadBytes, (U32 numBytes, void* ptr), (numBytes, ptr), TGEADDR_GAME_JOURNALWRITEU32);
		MEMBERFN(void, journalWriteBytes, (U32 numBytes, const void* ptr), (numBytes, ptr), TGEADDR_GAME_JOURNALWRITEBYTES);
		MEMBERFNSIMP(Stream*, journalGetStream, TGEADDR_GAME_GETJOURNALSTREAM);

		VIRTFN(void, postEvent, (Event &ev), (ev), TGEVIRT_GAMEINTERFACE_POSTEVENT);
	};

	#define MAX_TS_SET_DWORDS 6
	#define MAX_TS_SET_SIZE   (32*MAX_TS_SET_DWORDS)

	class TSMaterialList;
	class TSLastDetail;
	class TSMesh;

	class TSIntegerSet
	{
	public:
	   U32 bits[MAX_TS_SET_DWORDS];
	};

	template<class A> class ToolVector
	{
	   public:
	      A * addr;
	      S32 sz;
	};

	struct Quat16
	{
		enum { MAX_VAL = 0x7fff };
		S16 x, y, z, w;
	};

	class TSShape : public ResourceInstance
	{
		public:
			enum { UniformScale=0x01, AlignedScale=0x02, ArbitraryScale=0x04,
			       Blend=0x08, Cyclic=0x10, MakePath=0x20,
			       IflInit=0x40, HasTranslucency=0x80,
			       AnyScale=UniformScale|AlignedScale|ArbitraryScale };

		struct Node
		{
			S32 nameIndex;
			S32 parentIndex;

			S32 firstObject;
			S32 firstChild;
			S32 nextSibling;
		};

		struct Object
		{
			S32 nameIndex;
			S32 numMeshes;
			S32 startMeshIndex;
			S32 nodeIndex;
			S32 nextSibling;
			S32 firstDecal;
		};

		struct Decal
		{
			S32 nameIndex;
			S32 numMeshes;
			S32 startMeshIndex;
			S32 objectIndex;
			S32 nextSibling;
		};

		struct IflMaterial
		{
			S32 nameIndex;
			S32 materialSlot;
			S32 firstFrame;
			S32 firstFrameOffTimeIndex;
			S32 numFrames;
		};

		struct Sequence
		{
			S32 nameIndex;
			S32 numKeyframes;
			F32 duration;
			S32 baseRotation;
			S32 baseTranslation;
			S32 baseScale;
			S32 baseObjectState;
			S32 baseDecalState;
			S32 firstGroundFrame;
			S32 numGroundFrames;
			S32 firstTrigger;
			S32 numTriggers;
			F32 toolBegin;

			TSIntegerSet rotationMatters;
			TSIntegerSet translationMatters;
			TSIntegerSet scaleMatters;
			TSIntegerSet visMatters;
			TSIntegerSet frameMatters;
			TSIntegerSet matFrameMatters;
			TSIntegerSet decalMatters;
			TSIntegerSet iflMatters;

			S32 priority;
			U32 flags;
			U32 dirtyFlags;
		};

		struct ObjectState
		{
			F32 vis;
			S32 frameIndex;
			S32 matFrameIndex;
		};

		struct DecalState
		{
			S32 frameIndex;
		};

		struct Trigger
		{
			enum { StateOn = 1 << 31, InvertOnReverse = 1 << 30, StateMask = 0x1f };

			U32 state; // see above enum
			F32 pos;
		};

		struct Detail
		{
			S32 nameIndex;
			S32 subShapeNum;
			S32 objectDetailNum;
			F32 size;
			F32 averageError;
			F32 maxError;
			S32 polyCount;
		};

		struct ConvexHullAccelerator {
			U32      numVerts;
			Point3F* vertexList;
			Point3F* normalList;
			U8**     emitStrings;
		};

		ToolVector<Node> nodes;
		ToolVector<Object> objects;
		ToolVector<Decal> decals;
		ToolVector<IflMaterial> iflMaterials;
		ToolVector<ObjectState> objectStates;
		ToolVector<DecalState> decalStates;
		ToolVector<S32> subShapeFirstNode;
		ToolVector<S32> subShapeFirstObject;
		ToolVector<S32> subShapeFirstDecal;
		ToolVector<S32> detailFirstSkin;
		ToolVector<S32> subShapeNumNodes;
		ToolVector<S32> subShapeNumObjects;
		ToolVector<S32> subShapeNumDecals;
		ToolVector<Detail> details;
		ToolVector<Quat16> defaultRotations;
		ToolVector<Point3F> defaultTranslations;

		ToolVector<S32> subShapeFirstTranslucentObject;
		ToolVector<TSMesh*> meshes;
		ToolVector<F32> alphaIn;
		ToolVector<F32> alphaOut;


		Vector<Sequence> sequences;
		Vector<Quat16> nodeRotations;
		Vector<Point3F> nodeTranslations;
		Vector<F32> nodeUniformScales;
		Vector<Point3F> nodeAlignedScales;
		Vector<Quat16> nodeArbitraryScaleRots;
		Vector<Point3F> nodeArbitraryScaleFactors;
		Vector<Quat16> groundRotations;
		Vector<Point3F> groundTranslations;
		Vector<Trigger> triggers;
		Vector<F32> iflFrameOffTimes;
		Vector<TSLastDetail*> billboardDetails;
		Vector<ConvexHullAccelerator*> detailCollisionAccelerators;
		Vector<const char *> names;

		S8 * mMemoryBlock;

		TSMaterialList * materialList;

		F32 radius;
		F32 tubeRadius;
		Point3F center;
		Box3F bounds;

		U32 mExporterVersion;
		F32 mSmallestVisibleSize;
		S32 mSmallestVisibleDL;
		S32 mReadVersion;
		U32 mFlags;
		U32 data;

		bool mSequencesConstructed;
		S32 mVertexBuffer;
		U32 mCallbackKey;
		bool mExportMerge;
		bool mMorphable;
		Vector<S32> mPreviousMerge;
		S32 mMergeBufferSize;

		inline S32 findName(const char * name) const
		{
			for (S32 i=0; i<names.size(); i++)
			{
				if (!strcasecmp(name,names[i]))
					return i;
			}
			return -1;
		}

		MEMBERFN(S32, readName, (Stream*s, bool addName), (s, addName), TGEADDR_TSSHAPE_READNAME);
	};

	class OnverseGame : public GameInterface
	{
	public:

		// FIELDS
		enum JournalMode {
			JournalOff,
			JournalSave,
			JournalPlay,
		};
		JournalMode mJournalMode;
		bool mRunning;
		bool mJournalBreak;
		bool mRequiresRestart;

		/// Events are stored here by any thread, for processing by the main thread.
		Vector<void*> eventQueue1, eventQueue2, *eventQueue;
		// END FIELDS

		JournalMode getJournalMode() { return mJournalMode; };

		/// Are we reading back from the journal?
		bool isJournalReading() { return mJournalMode == JournalPlay; }

		/// Are we writing to the journal?
		bool isJournalWriting() { return mJournalMode == JournalSave; }

		MEMBERFN(bool, scriptMain, (int argc, const char **argv), (argc, argv), TGEADDR_GAME_SCRIPTMAIN);
	};

	// Console enums
	namespace ConsoleLogEntry
	{
		enum Level
		{
			Normal = 0,
			Warning,
			Error,
			NUM_CLASS
		};

		enum Type
		{
			General = 0,
			Assert,
			Script,
			GUI,
			Network,
			NUM_TYPE
		};
	}

	namespace Con
	{
		// Initialization
		FN(void, init, (), TGEADDR_CON_INIT);
		
		// Logging
		FN(void, printf, (const char *fmt, ...), TGEADDR_CON_PRINTF);

		// _printf is fastcall on Mac
#ifndef __APPLE__
		FN(void, _printf, (ConsoleLogEntry::Level level, ConsoleLogEntry::Type type, const char *fmt, va_list argptr), TGEADDR_CON__PRINTF);
#else
		FASTCALLFN(void, _printf, (ConsoleLogEntry::Level level, ConsoleLogEntry::Type type, const char *fmt, va_list argptr), TGEADDR_CON__PRINTF);
#endif

		OVERLOAD_PTR {
			OVERLOAD_FN(void, (const char *fmt, ...),                             TGEADDR_CON_WARNF_1V);
			OVERLOAD_FN(void, (ConsoleLogEntry::Type type, const char *fmt, ...), TGEADDR_CON_WARNF_2V);
		} warnf;
		OVERLOAD_PTR {
			OVERLOAD_FN(void, (const char *fmt, ...),                             TGEADDR_CON_ERRORF_1V);
			OVERLOAD_FN(void, (ConsoleLogEntry::Type type, const char *fmt, ...), TGEADDR_CON_ERRORF_2V);
		} errorf;

		// Variables
		FN(void, setVariable,      (const char *name, const char *value), TGEADDR_CON_SETVARIABLE);
		FN(void, setLocalVariable, (const char *name, const char *value), TGEADDR_CON_SETLOCALVARIABLE);
		FN(void, setBoolVariable,  (const char *name, bool value),        TGEADDR_CON_SETBOOLVARIABLE);
		FN(void, setIntVariable,   (const char *name, S32 value),         TGEADDR_CON_SETINTVARIABLE);
		FN(void, setFloatVariable, (const char *name, F32 value),         TGEADDR_CON_SETFLOATVARIABLE);
		FN(bool, getBoolVariable,  (const char *name, bool defvalue),     TGEADDR_CON_GETBOOLVARIABLE);

		// Misc
		FN(const char*, execute,              (S32 argc, const char *argv[]),                        TGEADDR_CON_EXECUTE);
		FN(const char*, executeObj,           (SimObject*, S32 argc, const char *argv[]),            TGEADDR_CON_EXECUTE_SIMOBJECT);
		FN(bool, exec,                 (const char* filename, bool noCalls, bool inJournal, bool baseFrame),            TGEADDR_CON_EXEC);
		FN(const char*, evaluate,             (const char *string, bool echo, const char *fileName), TGEADDR_CON_EVALUATE);
		FN(const char*, evaluatef,            (const char* string, ...),                             TGEADDR_CON_EVALUATEF);
		FN(char*,       getReturnBuffer,      (U32 bufferSize),                                      TGEADDR_CON_GETRETURNBUFFER);
		FN(bool,        expandScriptFilename, (char *filename, U32 size, const char *src),           TGEADDR_CON_EXPANDSCRIPTFILENAME);


		FN(Namespace*, lookupNamespace, (const char* name), TGEADDR_CON_LOOKUPNAMESPACE);
	}

	namespace Platform
	{
		FN(bool, dumpPath, (const char *path, Vector<FileInfo>& fileVector), TGEADDR_PLATFORM_DUMPPATH);
		FN(const char*, getWorkingDirectory, (), TGEADDR_PLATFORM_GETWORKINGDIRECTORY);
		FN(bool, isSubDirectory, (const char *parent, const char *child), TGEADDR_PLATFORM_ISSUBDIRECTORY);
		FN(bool, getFileTimes, (const char *path, FileTime *createTime, FileTime *modifyTime), TGEADDR_PLATFORM_GETFILETIMES);

		FN(bool, alertOK, (const char*, const char*), TGEADDR_PLATFORM_ALERTOK);
		//FN(S32, compareFileTimes, (const FileTime*, const FileTime *), TGEADDR_PLATFORM_COMPAREFILETIMES);

#ifdef _WIN32
        inline static S32 compareFileTimes(const FileTime &a, const FileTime &b)
        {
            if (a.high > b.high)
                return 1;
            if (a.high < b.high)
                return -1;
            if (a.low > b.low)
                return 1;
            if(a.low < b.low)
                return -1;
            return 0;
        }
#else
        inline S32 compareFileTimes(const FileTime &a, const FileTime &b)
        {
            if (a > b)
                return 1;
            if (a < b)
                return -1;
            return 0;
        }
#endif

		FN(bool, excludeOtherInstances, (const char* name), TGEADDR_PLATFORM_EXCLUDEOTHERINSTANCES);

		FN(U32, getRealMilliseconds, (), TGEADDR_PLATFORM_GETREALMILLISECONDS);
		FN(U32, getBackgroundSleepTime, (), TGEADDR_PLATFORM_GETBACKGROUNDSLEEPTIME);

#if !defined(__linux)
		FN(const char*, GetPrefDir, (), TGEADDR_PLATFORM_GETPREFDIR);
#endif
	}

	class CodeBlock
	{
	public:
		VTABLE(TGEOFF_CONSOLEOBJECT_VTABLE);

		char* globalStrings;
		char* functionStrings;
		double* globalFloats;
		double* functionFloats;
		U32 codeSize;
		U32* code;
		U32 refCount;
		U32 lineBreakPairCount;
		U32* lineBreakPairs;
		U32 breakListSize;
		U32* breakList;
		CodeBlock* nextFile;
		const char* mRoot;

		MEMBERFNSIMP(CodeBlock*, allocBlock, TGEADDR_CODEBLOCK_CONSTRUCTOR);

		MEMBERFN(bool, compile, (const char* codeFileName, const char* fileName, const char* script), (codeFileName, fileName, script), TGEADDR_CODEBLOCK_COMPILE);
		MEMBERFN(const char*, compileExec, (StringTableEntry fileName, const char *string, bool noCalls, int setFrame), (fileName, string, noCalls, setFrame), TGEADDR_CODEBLOCK_COMPILEEXEC);
		MEMBERFN(bool, read, (const char* fileName, Stream *st), (fileName, st), TGEADDR_CODEBLOCK_READ);
		MEMBERFN(const char*, exec, (U32 ip, const char *functionName, Namespace *thisNamespace, U32 argc, const char **argv, bool noCalls, StringTableEntry packageName, S32 setFrame), (ip, functionName, thisNamespace, argc, argv, noCalls, packageName, setFrame), TGEADDR_CODEBLOCK_EXEC);

		MEMBERFNSIMP(void, destroyBlock, TGEADDR_CODEBLOCK_DESTRUCTOR);
	};

	class Namespace
	{
	public:
		// BEGIN FIELDS
		StringTableEntry mName;
		StringTableEntry mPackage;

		Namespace *mParent;
		Namespace *mNext;
		AbstractClassRep *mClassRep;
		U32 mRefCountToParent;

		struct Entry
		{
			enum {
				InvalidFunctionType = -1,
				ScriptFunctionType,
				StringCallbackType,
				IntCallbackType,
				FloatCallbackType,
				VoidCallbackType,
				BoolCallbackType
			};

			Namespace *mNamespace;
			Entry *mNext;
			StringTableEntry mFunctionName;
			S32 mType;
			S32 mMinArgs;
			S32 mMaxArgs;
			const char *mUsage;

			CodeBlock *mCode;
			U32 mFunctionOffset;
			union {
				StringCallback mStringCallbackFunc;
				IntCallback mIntCallbackFunc;
				VoidCallback mVoidCallbackFunc;
				FloatCallback mFloatCallbackFunc;
				BoolCallback mBoolCallbackFunc;
			} cb;
		};
		Entry *mEntryList;

		Entry **mHashTable;
		U32 mHashSize;
		U32 mHashSequence;
		char* lastUsage;
		// END FIELDS

		MEMBERFN(void*, lookup, (const char* name), (name), TGEADDR_NAMESPACE_LOOKUP); // This is the Entry* version

		MEMBERFN(void, addCommandSTR,  (StringTableEntry name,void* ptr, const char *usage, S32 minArgs, S32 maxArgs), (name, ptr, usage, minArgs, maxArgs), TGEADDR_NAMESPACE_ADDCOMMAND_STR);
		MEMBERFN(void, addCommandINT,  (StringTableEntry name,void* ptr, const char *usage, S32 minArgs, S32 maxArgs), (name, ptr, usage, minArgs, maxArgs), TGEADDR_NAMESPACE_ADDCOMMAND_INT);
		MEMBERFN(void, addCommandFLT,  (StringTableEntry name,void* ptr, const char *usage, S32 minArgs, S32 maxArgs), (name, ptr, usage, minArgs, maxArgs), TGEADDR_NAMESPACE_ADDCOMMAND_FLOAT);
		MEMBERFN(void, addCommandVOID, (StringTableEntry name,void* ptr, const char *usage, S32 minArgs, S32 maxArgs), (name, ptr, usage, minArgs, maxArgs), TGEADDR_NAMESPACE_ADDCOMMAND_VOID);
		MEMBERFN(void, addCommandBOOL, (StringTableEntry name,void* ptr, const char *usage, S32 minArgs, S32 maxArgs), (name, ptr, usage, minArgs, maxArgs), TGEADDR_NAMESPACE_ADDCOMMAND_BOOL);
	};

	namespace NamespaceStatic
	{
		FN(void, init, (), TGEADDR_NAMESPACE_INIT);
		FN(Namespace*, find, (StringTableEntry nsName, StringTableEntry pkgName), TGEADDR_NAMESPACE_FIND);
	};

	namespace ParticleEngine
	{
		// Initialization
		FN(void, init, (), TGEADDR_PARTICLEENGINE_INIT);
		FN(void, destroy, (), TGEADDR_PARTICLEENGINE_DESTROY);
	}

	namespace Sim
	{
		FN(SimObject*, findObject, (const char *name), TGEADDR_SIM_FINDOBJECT);
	}

	namespace Net
	{
		enum Error
		{
			NoError,
			WrongProtocolType,
			InvalidPacketProtocol,
			WouldBlock,
			NotASocket,
			UnknownError
		};

		FN(bool, init, (), TGEADDR_NET_INIT);
		FN(Error, bind, (NetSocket socket, U16 port), TGEADDR_NET_BIND);
		
		void DLLSPEC addressToString(const NetAddress *address, char addressString[256]);
	}

	namespace TimeManager
	{
		FN(void, process, (), TGEADDR_TIMEMANAGER_PROCESS);
	}

	class NetInterface
	{
	public:
		VTABLE(TGEOFF_CONSOLEOBJECT_VTABLE);

		Vector<NetConnection *> mPendingConnections;
		U32                     mLastTimeoutCheckTime;
		U32                     mRandomHashData[12];
		bool                    mRandomDataInitialized;
		bool                    mAllowConnections;
		U8                      mAuthChallenge[8];

		// TGEADDR_NETINTERFACE_PROCESSPACKETRECEIVEEVENT
		MEMBERFN(void, sendConnectAccept, (NetConnection* conn), (conn), TGEADDR_NETINTERFACE_SENDCONNECTACCEPT);
		// TOFIX
		MEMBERFN(void, sendConnectReject, (NetConnection* conn, const char* reason), (conn, reason), TGEADDR_NETINTERFACE_SENDCONNECTACCEPT);
		MEMBERFN(void, computeNetMD5, (const NetAddress* address, U32 h1, U32*h2), (address, h1, h2), TGEADDR_NETINTERFACE_COMPUTEMD5);
		MEMBERFN(void, processPacketReceiveEvent, (PacketReceiveEvent* prEvent), (prEvent), TGEADDR_NETINTERFACE_PROCESSPACKETRECEIVEEVENT);

	};

	namespace Members
	{
		/*namespace OpenGLDevice
		{
			RAWMEMBERFNSIMP(TGE::OpenGLDevice, void, initDevice, 0x4033BE);
		}*/
		namespace GameBase
		{
			RAWMEMBERFN(TGE::GameBase, U32, packUpdate, (TGE::NetConnection *conn, U32 mask, TGE::BitStream *stream), TGEADDR_GAMEBASE_PACKUPDATE);
			RAWMEMBERFN(TGE::GameBase, void, unpackUpdate, (TGE::NetConnection *conn, TGE::BitStream *stream), TGEADDR_GAMEBASE_UNPACKUPDATE);
		}

		namespace FileStream
		{
			RAWMEMBERFN(TGE::FileStream, bool, open, (const char *path, int accessMode), TGEADDR_FILESTREAM_OPEN);
		}

		namespace AbstractClassRep
		{
			FN(void, initialize, (), TGEADDR_ABSTRACTCLASSREP_INITIALIZE);
        }

        namespace SimObject
        {
            RAWMEMBERFNSIMP(TGE::SimObject, bool, registerObject, TGEADDR_SIMOBJECT_REGISTEROBJECT);
            RAWMEMBERFNSIMP(TGE::SimObject, void, deleteObject, TGEADDR_SIMOBJECT_DELETEOBJECT);
        }

		namespace TSShape
		{
			RAWMEMBERFN(TGE::TSShape, S32, readName, (Stream * s, bool addName), TGEADDR_TSSHAPE_READNAME);
		}

		namespace File
		{
			RAWMEMBERFN(TGE::File, TGE::File::FileStatus, open, (const char *filename, const TGE::File::AccessMode openMode), TGEADDR_FILE_OPEN);
			RAWMEMBERFNSIMP(TGE::File, U32, getPosition, TGEADDR_FILE_GETPOSITION);
			RAWMEMBERFN(TGE::File, TGE::File::FileStatus, setPosition, (S32 position, bool absolutePos), TGEADDR_FILE_SETPOSITION);
			RAWMEMBERFNSIMP(TGE::File, U32, getSize, TGEADDR_FILE_GETSIZE);
			RAWMEMBERFNSIMP(TGE::File, TGE::File::FileStatus, flush, TGEADDR_FILE_FLUSH);
			RAWMEMBERFNSIMP(TGE::File, TGE::File::FileStatus, close, TGEADDR_FILE_CLOSE);
			RAWMEMBERFN(TGE::File, void, setStatus, (TGE::File::FileStatus status), TGEADDR_FILE_SETSTATUS_1); // Technically supposed to be protected
			RAWMEMBERFN(TGE::File, TGE::File::FileStatus, read, (U32 size, char *dst, U32 *bytesRead), TGEADDR_FILE_READ);
			RAWMEMBERFN(TGE::File, TGE::File::FileStatus, write, (U32 size, const char *src, U32 *bytesWritten), TGEADDR_FILE_WRITE);
			RAWMEMBERFNSIMP(TGE::File, void, destructor_, TGEADDR_FILE_DTOR);
		}
		
		namespace BitStream
		{
			//MEMBERFNSIMP(BitStream*, allocBitStream, TGEADDR_BITSTREAM_ALLOC);
			//VIRTDTOR(~BitStream, TGEADDR_BITSTREAM_DESTRUCTOR);

			RAWMEMBERFN(TGE::BitStream, void, writeInt, (S32 value, S32 bitCount), TGEADDR_BITSTREAM_WRITEINT);
			RAWMEMBERFN(TGE::BitStream, S32, readInt, (S32 bitCount), TGEADDR_BITSTREAM_READINT);
		}
		
		namespace Camera
		{
			RAWMEMBERFN(TGE::Camera, void, advancePhysics, (const TGE::Move *move, U32 delta), TGEADDR_CAMERA_ADVANCEPHYSICS);
		}
		
		namespace ResManager
		{
			// TODO: Allow overloading raw member pointers
			/*RAWMEMBERFN(TGE::ResManager, Stream*, openStream, (const char *path), 0x407E37);
			RAWMEMBERFN(TGE::ResManager, Stream*, openStream, (ResourceObject *obj), 0x4079EB);*/
			RAWMEMBERFN(TGE::ResManager, void, closeStream, (Stream *stream), TGEADDR_RESMANAGER_CLOSESTREAM);
			RAWMEMBERFN(TGE::ResManager, ResourceObject*, find, (const char *path), TGEADDR_RESMANAGER_FIND);
			RAWMEMBERFN(TGE::ResManager, U32, getSize, (const char *path), TGEADDR_RESMANAGER_GETSIZE);
			RAWMEMBERFN(TGE::ResManager, bool, getCrc, (const char *path, U32 &crc, U32 initialValue), TGEADDR_RESMANAGER_GETCRC);
			RAWMEMBERFN(TGE::ResManager, void, searchPath, (const char *path), TGEADDR_RESMANAGER_SEARCHPATH);
			RAWMEMBERFN(TGE::ResManager, bool, setModZip, (const char *path), TGEADDR_RESMANAGER_SETMODZIP);
		}

		namespace NetInterface
		{
			RAWMEMBERFN(TGE::NetInterface, void, processPacketReceiveEvent, (TGE::PacketReceiveEvent *prEvent), TGEADDR_NETINTERFACE_PROCESSPACKETRECEIVEEVENT);
		}

		namespace OnverseGame
		{
			RAWMEMBERFN(TGE::OnverseGame, U32, scriptMain, (int argc, const char **argv), TGEADDR_GAME_SCRIPTMAIN);
		}
	}

	FN(void, shutdownGame, (), TGEADDR_SHUTDOWNGAME);
	FN(void, clientProcess, (U32 timeDelta), TGEADDR_CLIENTPROCESS);

	// Platform functions
	FN(int, dSprintf, (char *buffer, size_t bufferSize, const char *format, ...), TGEADDR_DSPRINTF);
	FN(int, dVsprintf, (char *buffer, size_t maxSize, const char *format, void *args), TGEADDR_DVSPRINTF);
	FN(void, dFree, (void *ptr), TGEADDR_DFREE);
	FN(void, dQsort, (void *base, U32 nelem, U32 width, int (QSORT_CALLBACK *fcmp)(const void*, const void*)), TGEADDR_DQSORT);
	FN(bool, VectorResize, (U32 *aSize, U32 *aCount, void **arrayPtr, U32 newCount, U32 elemSize), TGEADDR_VECTORRESIZE);

	// Global variables
	GLOBALVAR(Container, gClientContainer, TGEADDR_GCLIENTCONTAINER);
	GLOBALVAR(Container, gServerContainer, TGEADDR_GSERVERCONTAINER);
	GLOBALVAR(Namespace*, gGlobalNS, TGEADDR_GLOBALNS);

	GLOBALVAR(_StringTable*, StringTable, TGEADDR_STRINGTABLE);
	GLOBALVAR(ResManager*, ResourceManager, TGEADDR_RESOURCEMANAGER);
	GLOBALVAR(NetInterface*, GNet, TGEADDR_GNET);
	GLOBALVAR(OnverseGame*, Game, TGEADDR_GAME);
	GLOBALVAR(GuiCanvas*, Canvas, TGEADDR_CANVAS);

	GLOBALVAR(int, Con_execDepth, TGEADDR_CON_EXECDEPTH);
	GLOBALVAR(int, Con_journalDepth, TGEADDR_CON_JOURNALDEPTH);
	GLOBALVAR(bool, Con_gCompilerMode, TGEADDR_CON_COMPILERMODE);

	GLOBALVAR(bool, Platform_UsePrefDir, TGEADDR_PLATFORM_USEPREFDIR);

	GLOBALVAR(void*, NetInterface_vTable, TGEADDR_NETINTERFACE_VTABLE);



    inline void SimObject::setInternalName(const char* newname)
    {
        if (newname)
            mInternalName = StringTable->insert(newname, false);
        else
            mInternalName = StringTable->insert("", false);
    }

	#if defined(__linux)
	GLOBALVAR(char, sgPrefDir, TGEADDR_PLATFORM_PREFDIRBUFFER);

	namespace Platform
	{
		inline const char* GetPrefDir()
		{
			return &sgPrefDir;
		}
	}
	#endif

#ifndef _WIN32
	struct MacCarbPlatState
	{
		U32          hDisplay;
		U32          cgDisplay;

		bool              captureDisplay;
		bool              fadeWindows;

		U32               appWindow;   
		char              appWindowTitle[256];
		U32               torqueWindowGroup;

		bool              quit;

		U32               ctx;
		bool              ctxNeedsUpdate;
		bool              headless;

		S32               desktopBitsPixel;
		S32               desktopWidth;
		S32               desktopHeight;
		U32               currentTime;

		U32               osVersion;

		U32               tsmDoc;
		bool              tsmActive;

		U32               firstThreadId;
		U32               torqueThreadId;

		void*             alertSemaphore;
		S32               alertHit;
		U32               alertDlg;
		U32               mainEventQueue;

		U64               platRandom;

		bool              mouseLocked;
		bool              backgrounded;
		bool              minimized;

		S32               sleepTicks;
		S32               lastTimeTick;

		U32               windowSize[2];

		U32               appReturn;

		U32               argc;
		char**            argv;
	};
#endif
}

// ConsoleFunction() can't be used from inside PluginLoader.dll without crashes
#ifndef IN_PLUGIN_LOADER

namespace TGE
{
	// Psuedo class used to implement the ConsoleFunction macro.
	class _ConsoleConstructor
	{
	public:
		_ConsoleConstructor(const char *name, StringCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			StringTableEntry nsNameENT = StringTable->insert(name, false);
			gGlobalNS->addCommandSTR(nsNameENT, (void*)cb, usage, minArgs, maxArgs);
		}

		_ConsoleConstructor(const char *name, VoidCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			StringTableEntry nsNameENT = StringTable->insert(name, false);
			gGlobalNS->addCommandVOID(nsNameENT, (void*)cb, usage, minArgs, maxArgs);
		}

		_ConsoleConstructor(const char *name, IntCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			StringTableEntry nsNameENT = StringTable->insert(name, false);
			gGlobalNS->addCommandINT(nsNameENT, (void*)cb, usage, minArgs, maxArgs);
		}

		_ConsoleConstructor(const char *name, FloatCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			StringTableEntry nsNameENT = StringTable->insert(name, false);
			gGlobalNS->addCommandFLT(nsNameENT, (void*)cb, usage, minArgs, maxArgs);
		}

		_ConsoleConstructor(const char *name, BoolCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			StringTableEntry nsNameENT = StringTable->insert(name, false);
			gGlobalNS->addCommandBOOL(nsNameENT, (void*)cb, usage, minArgs, maxArgs);
		}

		_ConsoleConstructor(const char *nsName, const char *name, StringCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			StringTableEntry nsNameENT = StringTable->insert(name, false);
			Namespace* ns = NamespaceStatic::find(StringTable->insert(nsName, false), NULL);
			ns->addCommandSTR(nsNameENT, (void*)cb, usage, minArgs, maxArgs);
		}

		_ConsoleConstructor(const char *nsName, const char *name, VoidCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			StringTableEntry nsNameENT = StringTable->insert(name, false);
			Namespace* ns = NamespaceStatic::find(StringTable->insert(nsName, false), NULL);
			ns->addCommandVOID(nsNameENT, (void*)cb, usage, minArgs, maxArgs);
		}

		_ConsoleConstructor(const char *nsName, const char *name, IntCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			StringTableEntry nsNameENT = StringTable->insert(name, false);
			Namespace* ns = NamespaceStatic::find(StringTable->insert(nsName, false), NULL);
			ns->addCommandINT(nsNameENT, (void*)cb, usage, minArgs, maxArgs);
		}

		_ConsoleConstructor(const char *nsName, const char *name, FloatCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			StringTableEntry nsNameENT = StringTable->insert(name, false);
			Namespace* ns = NamespaceStatic::find(StringTable->insert(nsName, false), NULL);
			ns->addCommandFLT(nsNameENT, (void*)cb, usage, minArgs, maxArgs);
		}

		_ConsoleConstructor(const char *nsName, const char *name, BoolCallback cb, const char *usage, S32 minArgs, S32 maxArgs)
		{
			StringTableEntry nsNameENT = StringTable->insert(name, false);
			Namespace* ns = NamespaceStatic::find(StringTable->insert(nsName, false), NULL);
			ns->addCommandBOOL(nsNameENT, (void*)cb, usage, minArgs, maxArgs);
		}
	};

	inline void ResManager::dumpResources()
	{
		ResourceObject *walk = resourceList.nextResource;
		for (ResourceObject *walk = resourceList.nextResource; walk = walk->nextResource; walk != NULL)
		{
			if (strncmp(walk->path, "onverse/data", 12) == 0)
				continue;
			TGE::Con::printf("Resource: %s/%s (%d) INST %x\n", walk->path, walk->name, walk->lockCount, walk->mInstance);
		}
	}
}

#if defined(__APPLE__)
#define PLATFORM_NO_CUSTOM_ALLOC
#endif

// Memory
template<typename T> inline T* TorqueAllocArray(unsigned long count)
{
#ifdef PLATFORM_NO_CUSTOM_ALLOC
	return new T[count];
#else
	return (T*)TGE::Memory::opNew(sizeof(T) * count);
#endif
}
template<typename T> inline T* TorqueAlloc()
{
#ifdef PLATFORM_NO_CUSTOM_ALLOC
	return new T;
#else
	return (T*)TGE::Memory::opNewArray(sizeof(T));
#endif
}
template<typename T> inline T* TorqueFreeArray(T* value)
{
#ifdef PLATFORM_NO_CUSTOM_ALLOC
	delete[] value;
#else
	TGE::Memory::opFree(value);
#endif
}
template<typename T> inline void TorqueFree(T* value)
{
#ifdef PLATFORM_NO_CUSTOM_ALLOC
	delete value;
#else
	TGE::Memory::opFreeArray(value);
#endif
}
inline char* TorqueStringDup(const char* value)
{
	int len = strlen(value)+1;
	char* out = (char*)TGE::Memory::opNew(len);
	memcpy(out, value, len);
	return out;
}

// Defines a console function.
#define ConsoleFunction(name, returnType, minArgs, maxArgs, usage)                         \
	static returnType c##name(TGE::SimObject *, S32, const char **argv);                   \
	static TGE::_ConsoleConstructor g##name##obj(#name, c##name, usage, minArgs, maxArgs); \
	static returnType c##name(TGE::SimObject *, S32 argc, const char **argv)

// Hacks to handle properly returning different value types from console methods
#define CONMETHOD_NULLIFY(val)
#define CONMETHOD_RETURN_const return (const
#define CONMETHOD_RETURN_S32   return (S32
#define CONMETHOD_RETURN_F32   return (F32
#define CONMETHOD_RETURN_void  CONMETHOD_NULLIFY(void
#define CONMETHOD_RETURN_bool  return (bool

// Defines a console method.
#define ConsoleMethod(className, name, type, minArgs, maxArgs, usage)                                                                \
	static type c##className##name(TGE::className *, S32, const char **argv);                                                        \
	static type c##className##name##caster(TGE::SimObject *object, S32 argc, const char **argv) {                                    \
		if (!object)                                                                                                                 \
			TGE::Con::warnf("Object passed to " #name " is null!");                                                                  \
		CONMETHOD_RETURN_##type ) c##className##name(static_cast<TGE::className*>(object), argc, argv);                              \
	};                                                                                                                               \
	static TGE::_ConsoleConstructor g##className##name##obj(#className, #name, c##className##name##caster, usage, minArgs, maxArgs); \
	static type c##className##name(TGE::className *object, S32 argc, const char **argv)

#endif // IN_PLUGIN_LOADER


inline char* dStrcpyl(char *dst, size_t dstSize, ...)
{
	const char* src;
	char *p = dst;

	AssertFatal(dstSize > 0, "dStrcpyl: destination size is set zero");
	dstSize--;  // leave room for string termination

	va_list args;
	va_start(args, dstSize);

	// concatenate each src to end of dst
	while ((src = va_arg(args, const char*)) != NULL)
	{
		while (dstSize && *src)
		{
			*p++ = *src++;
			dstSize--;
		}
	}

	va_end(args);

	// make sure the string is terminated 
	*p = 0;

	return dst;
}


#endif // TORQUELIB_TGE_H
