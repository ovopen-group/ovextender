#pragma once

//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include <TorqueLib/math/color.h>


#define DECLARE_OVERLOADED_READ(type)      \
   bool read(type* out_read) {             \
      return read(sizeof(type), out_read); \
      }
#define DECLARE_OVERLOADED_WRITE(type)       \
   bool write(type in_write) {               \
      return write(sizeof(type), &in_write); \
      }

#define DECLARE_ENDIAN_OVERLOADED_READ(type)       \
   bool read(type* out_read) {                     \
      type temp;                                   \
      bool success = read(sizeof(type), &temp);    \
      *out_read = (temp);      \
      return success;                              \
      }
#define DECLARE_ENDIAN_OVERLOADED_WRITE(type)      \
   bool write(type in_write) {                     \
      type temp = (in_write);  \
      return write(sizeof(type), &temp);           \
      }


class DLLSPEC InternalStream
{
	// Public structs and enumerations...
public:
	/// Status constantants for the stream
	enum Status {
		Ok = 0,           ///< Ok!
		IOError,          ///< Read or Write error
		EOS,              ///< End of Stream reached (mostly for reads)
		IllegalCall,      ///< An unsupported operation used.  Always w/ accompanied by AssertWarn
		Closed,           ///< Tried to operate on a closed stream (or detached filter)
		UnknownError      ///< Catchall
	};

	enum Capability {
		StreamWrite = BIT(0), ///< Can this stream write?
		StreamRead = BIT(1), ///< Can this stream read?
		StreamPosition = BIT(2)  ///< Can this stream position?
	};

	// Accessible only through inline accessors
private:
	Status m_streamStatus;


public:

	virtual bool _read(const U32 size, void* d) = 0;
	virtual bool _write(const U32 size, const void* d) = 0;

	// Overloaded write and read ops..
public:
	bool read(const U32 in_numBytes, void* out_pBuffer) {
		return _read(in_numBytes, out_pBuffer);
	}
	bool write(const U32 in_numBytes, const void* in_pBuffer) {
		return _write(in_numBytes, in_pBuffer);
	}

	bool write(const ColorI& rColor)
	{
		bool success = write(rColor.red);
		success |= write(rColor.green);
		success |= write(rColor.blue);
		success |= write(rColor.alpha);

		return success;
	}

	bool write(const ColorF& rColor)
	{
		ColorI temp = rColor;
		return write(temp);
	}

	bool read(ColorI* pColor)
	{
		bool success = read(&pColor->red);
		success |= read(&pColor->green);
		success |= read(&pColor->blue);
		success |= read(&pColor->alpha);

		return success;
	}

	bool read(ColorF* pColor)
	{
		ColorI temp;
		bool success = read(&temp);

		*pColor = temp;
		return success;
	}


	DECLARE_OVERLOADED_WRITE(S8)
	DECLARE_OVERLOADED_WRITE(U8)

	DECLARE_ENDIAN_OVERLOADED_WRITE(S16)
	DECLARE_ENDIAN_OVERLOADED_WRITE(S32)
	DECLARE_ENDIAN_OVERLOADED_WRITE(U16)
	DECLARE_ENDIAN_OVERLOADED_WRITE(U32)
	DECLARE_ENDIAN_OVERLOADED_WRITE(F32)

	DECLARE_OVERLOADED_READ(S8)
	DECLARE_OVERLOADED_READ(U8)

	DECLARE_ENDIAN_OVERLOADED_READ(S16)
	DECLARE_ENDIAN_OVERLOADED_READ(S32)
	DECLARE_ENDIAN_OVERLOADED_READ(U16)
	DECLARE_ENDIAN_OVERLOADED_READ(U32)
	DECLARE_ENDIAN_OVERLOADED_READ(F32)

	bool read(bool* out_pRead) {
		U8 translate;
		bool success = read(&translate);
		if (success == false)
			return false;

		*out_pRead = translate != 0;
		return true;
	}
	bool write(const bool& in_rWrite) {
		U8 translate = in_rWrite ? U8(1) : U8(0);
		return write(translate);
	}
};

typedef const char* StringTableEntry;



inline bool mathRead(InternalStream& stream, Point2I* p)
{
	bool success = stream.read(&p->x);
	success &= stream.read(&p->y);
	return success;
}

inline bool mathRead(InternalStream& stream, Point3I* p)
{
	bool success = stream.read(&p->x);
	success &= stream.read(&p->y);
	success &= stream.read(&p->z);
	return success;
}

inline bool mathRead(InternalStream& stream, Point2F* p)
{
	bool success = stream.read(&p->x);
	success &= stream.read(&p->y);
	return success;
}

inline bool mathRead(InternalStream& stream, Point3F* p)
{
	bool success = stream.read(&p->x);
	success &= stream.read(&p->y);
	success &= stream.read(&p->z);
	return success;
}

inline bool mathRead(InternalStream& stream, Point4F* p)
{
	bool success = stream.read(&p->x);
	success &= stream.read(&p->y);
	success &= stream.read(&p->z);
	success &= stream.read(&p->w);
	return success;
}

inline bool mathRead(InternalStream& stream, RectI* r)
{
	bool success = mathRead(stream, &r->point);
	success &= mathRead(stream, &r->extent);
	return success;
}

inline bool mathRead(InternalStream& stream, RectF* r)
{
	bool success = mathRead(stream, &r->point);
	success &= mathRead(stream, &r->extent);
	return success;
}

inline bool mathRead(InternalStream& stream, MatrixF* m)
{
	bool success = true;
	F32* pm = *m;
	for (U32 i = 0; i < 16; i++)
		success &= stream.read(&pm[i]);
	return success;
}

inline bool mathRead(InternalStream& stream, QuatF* q)
{
	bool success = stream.read(&q->x);
	success &= stream.read(&q->y);
	success &= stream.read(&q->z);
	success &= stream.read(&q->w);
	return success;
}