#pragma once
//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "internalStream.h"
#include <TorqueLib/util/tVector.h>
#include <TorqueLib/TGE.h>

// should be 52 bytes
class DLLSPEC InternalBitStream : public InternalStream
{
public:
	U8 *dataPtr;
	S32  bitNum;
	S32  bufSize;
	bool error;
	S32  maxReadBitNum;
	S32  maxWriteBitNum;
	char *stringBuffer;
	bool mCompressRelative;
	Point3F mCompressPoint;

	friend class HuffmanProcessor;
public:
	static InternalBitStream *getPacketStream(U32 writeSize = 0);
	static void sendPacketStream(const TGE::NetAddress *addr);

	void setBuffer(void *bufPtr, S32 bufSize, S32 maxSize = 0);
	U8*  getBuffer() { return dataPtr; }
	U8*  getBytePtr();

	U32 getReadByteSize();

	S32  getCurPos() const;
	void setCurPos(const U32);

	InternalBitStream(void *bufPtr, S32 bufSize, S32 maxWriteSize = -1) { setBuffer(bufPtr, bufSize, maxWriteSize); stringBuffer = NULL; }
	void clear();

	inline void setStringBuffer(char buffer[256]);
	void writeInt(S32 value, S32 bitCount);
	S32  readInt(S32 bitCount);

	/// Use this method to write out values in a concise but ass backwards way...
	/// Good for values you expect to be frequently zero, often small. Worst case
	/// this will bloat values by nearly 20% (5 extra bits!) Best case you'll get
	/// one bit (if it's zero).
	///
	/// This is not so much for efficiency's sake, as to make life painful for
	/// people that want to reverse engineer our network or file formats.
	void writeCussedU32(U32 val)
	{
		// Is it zero?
		if (writeFlag(val == 0))
			return;

		if (writeFlag(val <= 0xF)) // 4 bit
			writeRangedU32(val, 0, 0xF);
		else if (writeFlag(val <= 0xFF)) // 8 bit
			writeRangedU32(val, 0, 0xFF);
		else if (writeFlag(val <= 0xFFFF)) // 16 bit
			writeRangedU32(val, 0, 0xFFFF);
		else if (writeFlag(val <= 0xFFFFFF)) // 24 bit
			writeRangedU32(val, 0, 0xFFFFFF);
		else
			writeRangedU32(val, 0, 0xFFFFFFFF);
	}

	U32 readCussedU32()
	{
		if (readFlag())
			return 0;

		if (readFlag())
			return readRangedU32(0, 0xF);
		else if (readFlag())
			return readRangedU32(0, 0xFF);
		else if (readFlag())
			return readRangedU32(0, 0xFFFF);
		else if (readFlag())
			return readRangedU32(0, 0xFFFFFF);
		else
			return readRangedU32(0, 0xFFFFFFFF);
	}

	void writeSignedInt(S32 value, S32 bitCount);
	S32  readSignedInt(S32 bitCount);

	void writeRangedU32(U32 value, U32 rangeStart, U32 rangeEnd);
	U32  readRangedU32(U32 rangeStart, U32 rangeEnd);

	/// Writes a clamped signed integer to the stream using 
	/// an optimal amount of bits for the range.
	void writeRangedS32(S32 value, S32 min, S32 max);

	/// Reads a ranged signed integer written with writeRangedS32.
	S32 readRangedS32(S32 min, S32 max);

	// read and write floats... floats are 0 to 1 inclusive, signed floats are -1 to 1 inclusive

	F32  readFloat(S32 bitCount);
	F32  readSignedFloat(S32 bitCount);

	void writeFloat(F32 f, S32 bitCount);
	void writeSignedFloat(F32 f, S32 bitCount);

	/// Writes a clamped floating point value to the 
	/// stream with the desired bits of precision.
	void writeRangedF32(F32 value, F32 min, F32 max, U32 numBits);

	/// Reads a ranged floating point value written with writeRangedF32.
	F32 readRangedF32(F32 min, F32 max, U32 numBits);

	void writeClassId(U32 classId, U32 classType, U32 classGroup);
	S32 readClassId(U32 classType, U32 classGroup); // returns -1 if the class type is out of range

	// writes a normalized vector
	void writeNormalVector(const Point3F& vec, S32 bitCount);
	void readNormalVector(Point3F *vec, S32 bitCount);

	void clearCompressionPoint();
	void setCompressionPoint(const Point3F& p);

	// Matching calls to these compression methods must, of course,
	// have matching scale values.
	void writeCompressedPoint(const Point3F& p, F32 scale = 0.01f);
	void readCompressedPoint(Point3F* p, F32 scale = 0.01f);

	// Uses the above method to reduce the precision of a normal vector so the server can
	//  determine exactly what is on the client.  (Pre-dumbing the vector before sending
	//  to the client can result in precision errors...)
	static Point3F dumbDownNormal(const Point3F& vec, S32 bitCount);


	// writes a normalized vector using alternate method
	void writeNormalVector(const Point3F& vec, S32 angleBitCount, S32 zBitCount);
	void readNormalVector(Point3F *vec, S32 angleBitCount, S32 zBitCount);

	void readVector(Point3F * vec, F32 minMag, F32 maxMag, S32 magBits, S32 angleBits, S32 zBits);
	void writeVector(Point3F vec, F32 minMag, F32 maxMag, S32 magBits, S32 angleBits, S32 zBits);

	virtual void writeBits(S32 bitCount, const void *bitPtr);
	virtual void readBits(S32 bitCount, void *bitPtr);
	virtual bool writeFlag(bool val);
	virtual bool readFlag();

	void writeAffineTransform(const MatrixF&);
	void readAffineTransform(MatrixF*);

	void setBit(S32 bitCount, bool set);
	bool testBit(S32 bitCount);

	bool isFull() { return bitNum > (bufSize << 3); }
	bool isValid() { return !error; }

	bool _read(const U32 size, void* d);
	bool _write(const U32 size, const void* d);

	void readString(char stringBuf[256]);
	void writeString(const char *stringBuf, S32 maxLen = 255);

	bool hasCapability(const Capability) const { return true; }
	U32  getPosition() const;
	bool setPosition(const U32 in_newPosition);
	U32  getStreamSize();

	template<typename T> static void copyState(InternalBitStream* in, T* out)
	{
		out->dataPtr = in->dataPtr;
		out->bitNum = in->bitNum;
		out->bufSize = in->bufSize;
		out->error = in->error;
		out->maxReadBitNum = in->maxReadBitNum;
		out->maxWriteBitNum = in->maxWriteBitNum;
		out->stringBuffer = in->stringBuffer;
		out->mCompressRelative = in->mCompressRelative;
		out->mCompressPoint = in->mCompressPoint;
	}

	StringTableEntry readSTString();
};

//------------------------------------------------------------------------------
//-------------------------------------- INLINES
//
inline S32 InternalBitStream::getCurPos() const
{
	return bitNum;
}

inline void InternalBitStream::setCurPos(const U32 in_position)
{
	AssertFatal(in_position < (U32)(bufSize << 3), "Out of range bitposition");
	bitNum = S32(in_position);
}

inline bool InternalBitStream::readFlag()
{
	if (bitNum > maxReadBitNum)
	{
		error = true;
		AssertFatal(false, "Out of range read");
		return false;
	}
	S32 mask = 1 << (bitNum & 0x7);
	bool ret = (*(dataPtr + (bitNum >> 3)) & mask) != 0;
	bitNum++;
	return ret;
}

// InternalBitStream utility functions

inline void InternalBitStream::setStringBuffer(char buffer[256])
{
	stringBuffer = buffer;
}

typedef InternalBitStream*  (*fpGetPacketStream)(void);

inline InternalBitStream *InternalBitStream::getPacketStream(U32 writeSize)
{
	// hook into orig ver
	// @ 0x000adc02
	const fpGetPacketStream _func = (fpGetPacketStream)0x53780;
	return _func();
}

typedef const char*  (*fpSendPacketStream)(const TGE::NetAddress*);

inline void InternalBitStream::sendPacketStream(const TGE::NetAddress *addr)
{
	// hook into orig ver
	const fpSendPacketStream _func = (fpSendPacketStream)0x53aa0;
	_func(addr);
}

// FIXMEFIXMEFIXME MATH

inline bool IsEqual(F32 a, F32 b) { return a == b; }


class HuffmanProcessor
{
	static const U32 csm_charFreqs[256];
	bool   m_tablesBuilt;

	void buildTables();

	struct HuffNode {
		U32 pop;

		S16 index0;
		S16 index1;
	};
	struct HuffLeaf {
		U32 pop;

		U8  numBits;
		U8  symbol;
		U32 code;   // no code should be longer than 32 bits.
	};
	// We have to be a bit careful with these, mSince they are pointers...
	struct HuffWrap {
		HuffNode* pNode;
		HuffLeaf* pLeaf;

	public:
		HuffWrap() : pNode(NULL), pLeaf(NULL) { }

		void set(HuffLeaf* in_leaf) { pNode = NULL; pLeaf = in_leaf; }
		void set(HuffNode* in_node) { pLeaf = NULL; pNode = in_node; }

		U32 getPop() { if (pNode) return pNode->pop; else return pLeaf->pop; }
	};

	TGE::Vector<HuffNode> m_huffNodes;
	TGE::Vector<HuffLeaf> m_huffLeaves;

	S16 determineIndex(HuffWrap&);

	void generateCodes(InternalBitStream&, S32, S32);

public:
	HuffmanProcessor() : m_tablesBuilt(false) { }

	static HuffmanProcessor g_huffProcessor;

	bool readHuffBuffer(InternalBitStream* pStream, char* out_pBuffer);
	bool writeHuffBuffer(InternalBitStream* pStream, const char* out_pBuffer, S32 maxLen);
};


//-----------------------------------------------------------------------------

enum NetClassTypes {
	NetClassTypeObject = 0,
	NetClassTypeDataBlock,
	NetClassTypeEvent,
	NetClassTypesCount,
};

//-----------------------------------------------------------------------------

enum NetClassGroups {
	NetClassGroupGame = 0,
	NetClassGroupCommunity,
	NetClassGroup3,
	NetClassGroup4,
	NetClassGroupsCount,
};
extern U32 __NetClassBitSize[NetClassGroupsCount][NetClassTypesCount];


enum SimObjectsConstants
{
	DataBlockObjectIdFirst = 3,
	DataBlockObjectIdBitSize = 14, // NOTE: internal = 13, 0.9 = 14!!
	DataBlockObjectIdLast = DataBlockObjectIdFirst + (1 << DataBlockObjectIdBitSize) - 1,

	DynamicObjectIdFirst = DataBlockObjectIdLast + 1,
	InvalidEventId = 0,
	RootGroupId = 0xFFFFFFFF,
};