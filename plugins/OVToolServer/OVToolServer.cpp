/*
OVExtender
Copyright (C) 2020 mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>
#include <TorqueLib/math/mMath.h>
#include <TorqueLib/math/mathUtils.h>
#include <TorqueLib/TGE.h>
#include <TorqueLib/QuickOverride.h>
#include <PluginLoader/PluginInterface.h>

#include <unordered_map>

#include "ovopen/packetHandlers.hpp"
#include "OVOnlinePlugin/OVClientBase.h"

using namespace OVClientBase;
std::mutex gScriptMutex;

void InitToolServer(uint16_t port)
{
	TGE::Con::printf("[OVToolServer] Listening on port %u\n", (uint32_t)port);
	new OVServer<OVServerGameConnectionState>("tool",
		gIOService,
		port,
		OVProtocolHandler::PROTOCOL_BACKEND_TOOL,
		0xFFFF);
}

class RPC_CompileScript : public RPCPacketHandler
{
public:
	DECLARE_RPC_PACKET_HANDLER(11005, OVProtocolHandler::PROTOCOL_BACKEND_TOOL);

	enum
	{
		RESPONSE_OK = 0,
		RESPONSE_ERROR = -1
	};

	struct ScriptInfo
	{
		std::string srcPath;
		std::string fullSrcPath;
		std::string fullDsoPath;
		std::string fullDestDsoPath;
		std::string dsoPath;
		char* script;
	};

	std::string scriptPath;
	static asio::io_service gScriptQueue;
	static bool gLastScriptCompiled;
	static std::vector<ScriptInfo> gScriptList;

	virtual RPCTicketPtr fire(OVProtocolHandler& client) { return RPCTicketPtr(); }

	static void ExpendQueue()
	{
		TGE::Con::printf("[OVToolServer] Running script queue...\n");
         gScriptQueue.run_one();
        if (gScriptQueue.stopped())
        	gScriptQueue.restart();
		gScriptQueue.run_one();
	}

	virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
	{
		TGE::Con::printf("[OVToolServer] RPC_CompileScript handle\n");
		MemStream stream(info.size, info.data);
		char scriptName[512];
		char scriptDSOName[512];
		snprintf(scriptName, sizeof(scriptName), "onverse/tmp/%u.cs", (uint32_t)requestTicket->getRequestID());
		snprintf(scriptDSOName, sizeof(scriptDSOName), "onverse/tmp/%u.cs.dso", (uint32_t)requestTicket->getRequestID());

		if (info.code == 1)
		{
			//getcwd(&rootPath[0], sizeof(rootPath));

			std::string scriptSourcePath;
			std::string scriptDestPath;
			stream.readString(scriptSourcePath);
			stream.readString(scriptDestPath);

			std::string prefDir = TGE::Platform::GetPrefDir();

			std::string outP = prefDir + std::string("/") + std::string(scriptDSOName);


			TGE::Con::printf("[OVToolServer] Will attempt to compile %s to %s then copy to %s\n", scriptSourcePath.c_str(), outP.c_str(), scriptDestPath.c_str());

			FILE* fp = fopen(scriptSourcePath.c_str(), "rb");
			ScriptInfo info;
			info.fullSrcPath = prefDir + std::string("/") + std::string(scriptName);
			info.fullDsoPath = prefDir + std::string("/") + std::string(scriptDSOName);
			info.fullDestDsoPath = scriptDestPath;
			info.srcPath = std::string(scriptName);
			info.dsoPath = std::string(scriptDSOName);
			info.script = scriptName;

			if (!fp)
			{
				endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERROR, false);
				return;
			}

			size_t sz = 0;
			fseek(fp, 0, SEEK_END);
			sz = ftell(fp);
			fseek(fp, 0, SEEK_SET);

			info.script = new char[sz+1];
			fread(info.script, 1, sz, fp);
			fclose(fp);
			info.script[sz] = '\0';

			if (true)
			{
				std::lock_guard<std::mutex> guard(gScriptMutex);
				gScriptList.push_back(info);
			}

			std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
			gScriptQueue.post([this, clientHandler, requestTicket, outP, info]{
				// Send reply with path of dso
				MemStream packetStream(4096);
				PacketBuilder packet(&packetStream);

				if (gLastScriptCompiled)
				{
					packet.beginRPC(getRPCID(), requestTicket->getRequestID(), 2, false);
					packetStream.writeString(outP.c_str());
					packet.commit();

					PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
					clientHandler->queuePacketData(responseData);
				}

				TGE::Con::printf("Sending reply %s...\n", gLastScriptCompiled ? "RESPONSE_OK" : "RESPONSE_ERROR");
				endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, gLastScriptCompiled ? RESPONSE_OK : RESPONSE_ERROR);
				delete[] info.script;
			});

			// Do all the main stuff on the main thread
			const char* args[] = {"OnToolServerCompile", scriptName};
			TGE::Con::execute(3, args);
		}
	}
};
IMPL_RPC_PACKET_HANDLER(RPC_CompileScript);


asio::io_service RPC_CompileScript::gScriptQueue;
bool RPC_CompileScript::gLastScriptCompiled;
std::vector<RPC_CompileScript::ScriptInfo> RPC_CompileScript::gScriptList;

ConsoleFunction(ToolServerWriteHeadScript, bool, 2, 2, "")
{
    std::lock_guard<std::mutex> guard(gScriptMutex);
	RPC_CompileScript::ScriptInfo &info = RPC_CompileScript::gScriptList[0];

	// Now compile
	TGE::OnverseGame* Game = TGE::Game;
	TGE::ResManager* ResourceManager = TGE::ResourceManager;
	TGE::_StringTable* StringTable = TGE::StringTable;


	TGE::StringTableEntry scriptFileName = StringTable->insert(info.srcPath.c_str(), false);
	TGE::StringTableEntry dsoName = StringTable->insert(info.dsoPath.c_str(), false);

	// compile this baddie.
	TGE::Con::printf("[OVToolServer] Compiling %s....\n", scriptFileName);
	TGE::CodeBlock *code = TorqueAlloc<TGE::CodeBlock>();
	code->allocBlock();
	code->compile(dsoName, scriptFileName, info.script);
	delete code;
	TorqueFree<TGE::CodeBlock>(code);

	TGE::ResourceObject *rScr = ResourceManager->find(dsoName);
	if (!rScr)
	{
		RPC_CompileScript::gScriptList.erase(RPC_CompileScript::gScriptList.begin());
		return false;
	}
	else
	{
		// Copy to out
		TGE::Con::printf("[OVToolServer] Writing %s to %s\n", info.fullDsoPath.c_str(), info.fullDestDsoPath.c_str());
		FILE* fp = fopen(info.fullDsoPath.c_str(), "rb");
		FILE* out_fp = fopen(info.fullDestDsoPath.c_str(), "wb");
		char buffer[4096];

		size_t sz = 0;
		fseek(fp, 0, SEEK_END);
		sz = ftell(fp);
		fseek(fp, 0, SEEK_SET);

		uint8_t* tmp = new uint8_t[sz];
		fread(tmp, 1, sz, fp);
		fclose(fp);

		fwrite(tmp, 1, sz, out_fp);
		fclose(out_fp);
		delete[] tmp;

		RPC_CompileScript::gScriptList.erase(RPC_CompileScript::gScriptList.begin());
		return true;
	}
}

ConsoleFunction(ToolServerSendResponse, void, 2, 2, "")
{
   uint32_t compiledFlag = strtoul(argv[1], 0, NULL);
   RPC_CompileScript::gLastScriptCompiled = compiledFlag != 0;

   gIOService.post([]{
   	RPC_CompileScript::ExpendQueue();
   });
}

void LogToStdout(uint32_t level, LogEntry *logEntry)
{
   printf("%s\n", logEntry->mData);
}

bool gAddedLog;

ConsoleFunction(StartToolServer, void, 2, 2, "")
{
   if (!gAddedLog) Log::addConsumer(LogToStdout);
   uint32_t port = strtoul(argv[1], 0, NULL);

   InitToolServer(port);
   gAddedLog = true;
   RPC_CompileScript::gScriptList.reserve(512);

   TGE::Con::printf("[OVToolServer] pref dir is %s\n", TGE::Platform::GetPrefDir());
}
