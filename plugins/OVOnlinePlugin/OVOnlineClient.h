/*
OVExtender
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _OVONLINECLIENT_H_
#define _OVONLINECLIENT_H_

#include "OVOnlinePlugin/OVClientBase.h"

EXT_NS(OVOnlineClient)


struct MasterUserInfo
{
   std::string username;
   uint8_t code[20];
   uint8_t isVIP;
   uint8_t avatarSex;
   uint32_t uid;
   uint32_t permissions;
   uint32_t invID;
   uint32_t connectionID;
};


class OVClientGameConnectionState : public OVConnectionState
{
public:
   OVClientGameConnectionState(asio::io_service &svc) {;}
   
   ~OVClientGameConnectionState() {;}
   
   std::string mLoginUsername;
   std::string mLoginPassword;
};


class MasterClientConnector : public std::enable_shared_from_this<OVProtocolHandler>
{
public:

   MasterClientConnector(asio::io_service &svc) : mResolver(svc)
   {
   }

   void beginConnect(asio::io_service &svc, const char *address, const char *port, const char* username, const char* password)
   {
	   OVClientGameConnectionState* state = new OVClientGameConnectionState(OVClientBase::gIOService);
       mHandler = std::make_shared<OVProtocolHandler>(OVClientBase::gIOService,
		  tcp::socket(OVClientBase::gIOService),
      OVProtocolHandler::PROTOCOL_BACKEND | OVProtocolHandler::PROTOCOL_CLIENT, 
      state, 
      0);

      state->mLoginUsername = username;
      state->mLoginPassword = password;

      printf("resolving %s:%s\n", address, port);

      tcp::resolver::query query(address, port);
      mResolver.async_resolve(query,
        std::bind(&MasterClientConnector::onResolved, this,
          std::placeholders::_1,
          std::placeholders::_2));
   }

   void onConnected(const std::error_code &ec)
   {
      if (ec)
      {
         printf("Failed to connect!\n");
      }
      else
      {
         printf("Connected to server, starting reading...\n");
         mHandler->start(0xFFFF);
      }
   }

   void onResolved(const std::error_code& err,
      tcp::resolver::iterator endpoint_iterator)
  {
    if (!err)
    {
      // Attempt a connection to the first endpoint in the list. Each endpoint
      // will be tried until we successfully establish a connection.
      tcp::endpoint endpoint = *endpoint_iterator;

      while (!endpoint.address().is_v4() && endpoint_iterator != tcp::resolver::iterator())
      {
         endpoint_iterator++;
         endpoint = *endpoint_iterator;
      }

      if (endpoint_iterator == tcp::resolver::iterator())
      {
         printf("No suitable ipv4 endpoint found\n");
         return;
      }

      mHandler->mSocket.async_connect(endpoint,
          std::bind(&MasterClientConnector::onConnected, this,
            std::placeholders::_1));
    }
    else
    {
      std::cout << "Error: " << err.message() << "\n";
    }
  }

   tcp::resolver mResolver;
   std::shared_ptr<OVProtocolHandler> mHandler;
};

// State

extern std::unordered_map<std::string, uint32_t> gUserHashNameLookup;
extern std::unordered_map<uint32_t, MasterUserInfo> gUserHashIDLookup;

extern MasterClientConnector gMasterConnector;

extern U32 gAuthCode;
extern S32 gSecureLoginOnly;
extern U32 gLocationReadyState;

EXT_NS_END()

#endif