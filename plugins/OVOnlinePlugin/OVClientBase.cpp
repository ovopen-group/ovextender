/*
OVExtender
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "OVClientBase.h"
#include "ovopen/packetHandlers.hpp"

EXT_NS(OVClientBase)

asio::io_service gIOService;
std::shared_ptr<OVProtocolHandler> gProtocolClient;
std::shared_ptr<asio::io_service::work> gDummyWork;
std::thread* gIOThread;
tcp::resolver gTCPResolver(gIOService);

void ASIOLoop()
{
   printf("ASIOLoop started\n");
   gDummyWork = std::make_shared<asio::io_service::work>(gIOService);
   gIOService.run();
   printf("ASIOLoop ended\n");
}

EXT_NS_END()

using namespace OVClientBase;

ConsoleFunction(initASIO, void, 1, 1, "initASIO()")
{
	gIOThread = new std::thread(ASIOLoop);
	PacketHandler::initKlasses();
	RPCPacketHandler::initKlasses();
}
