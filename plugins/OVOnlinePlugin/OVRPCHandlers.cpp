/*
OVExtender
Copyright (C) 2020 mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "OVRPCHandlers.h"
#include "OVOnlineClient.h"
#include "OVOnlinePlugin.h"

// RPC
IMPL_RPC_PACKET_HANDLER(RPC_RegisterLocationServer);
IMPL_RPC_PACKET_HANDLER(RPC_SetLocationReadyState);
IMPL_RPC_PACKET_HANDLER(RPC_RequestInventory);
IMPL_RPC_PACKET_HANDLER(RPC_NotifyTransaction);
IMPL_RPC_PACKET_HANDLER(RPC_CommitInventory);
IMPL_RPC_PACKET_HANDLER(RPC_ExpendUserLoginQueue);
IMPL_RPC_PACKET_HANDLER(RPC_NotifyUserPresence);
IMPL_RPC_PACKET_HANDLER(RPC_CommitHousePlot);

// Packet
IMPL_PACKET_HANDLER(Packet_UserLoginQueued);
IMPL_PACKET_HANDLER(Packet_UserShouldBeKicked);
IMPL_PACKET_HANDLER(Packet_InventoryData);
IMPL_PACKET_HANDLER(Packet_HousePlot);
IMPL_PACKET_HANDLER(Packet_UserInventoryId);
IMPL_PACKET_HANDLER(Packet_ProcessManagement);
IMPL_PACKET_HANDLER(Packet_ServerInfo);
IMPL_PACKET_HANDLER(Packet_UserBalance);

using namespace OVOnlineClient;

RPCTicketPtr RPC_RegisterLocationServer::fire(OVProtocolHandler& client)
{
   RPCTicketPtr ticket = createFireTicket(client);
   if (!ticket)
   {
      TGE::Con::printf("Error: can't make RPC_RegisterLocationServer ticket!\n");
      return RPCTicketPtr();
   }

   TGE::Con::printf("Firing ticket with request id %u\n", ticket->mInfo.mRequestID);
   
   StackMemStream<sizeof(OVPacketHeader) + 6 + sizeof(OVPacketHeader) + 6 + 5> packetStream;
   PacketBuilder packet(&packetStream);
   packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, 1, false);

   packetStream.writeUintBE(paramDBID); //
   packetStream.writeUintBE(paramPort);
   packetStream.writeUintBE(paramAllowedMask);
   
   packet.commit();
   
   client.onQueueWriteData( PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr) );
   return ticket;
}

void RPC_RegisterLocationServer::handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
{
   TGE::Con::printf("Handling incomming RPC_RegisterLocationServer packet %u\n", info.code);

   if (info.code != 0)
   {
      TGE::Con::printf("Warning: unknown RPC code %u\n", info.code);

      char errBuff[64];
      snprintf(errBuff, sizeof(errBuff), "%i", info.code);
      
      const char* args[] = {"OnMasterServerRegistrationFailed", errBuff};
      TGE::Con::execute(2, args);
   }
   else
   {
      TGE::Con::printf("Setting permissions for server to %u vip=%u\n", gLocationReadyState, 0);
      RPC_SetLocationReadyState* handler = (RPC_SetLocationReadyState*)RPCPacketHandler::getHandlerForRPCCommand(9002);
      handler->paramLocationReadyState = gLocationReadyState;
      handler->paramVipOnly = false;
      RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());
   }

   client.endRPCTicket(requestTicket);
}

RPCTicketPtr RPC_SetLocationReadyState::fire(OVProtocolHandler& client)
{
   RPCTicketPtr ticket = createFireTicket(client);
   if (!ticket)
   {
      TGE::Con::printf("Error: can't make RPC_SetLocationReadyState ticket!\n");
      return RPCTicketPtr();
   }

   TGE::Con::printf("Firing ticket with request id %u\n", ticket->mInfo.mRequestID);

   StackMemStream<sizeof(OVPacketHeader) + 6 + sizeof(OVPacketHeader) + 5> packetStream;
   PacketBuilder packet(&packetStream);
   packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, 1, false);

   bool vipOnly = false;
   packetStream.writeUintBE(paramLocationReadyState);
   packetStream.write(paramVipOnly);

   packet.commit();
   
   client.onQueueWriteData( PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr) );
   return ticket;
}

void RPC_SetLocationReadyState::handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
{
   TGE::Con::printf("Handling incomming RPC_SetLocationReadyState packet %u\n", info.code);

   if (info.code != 0)
   {
      TGE::Con::printf("Warning: unknown RPC code %u\n", info.code);
   }

   client.endRPCTicket(requestTicket);
}

RPCTicketPtr RPC_RequestInventory::fire(OVProtocolHandler& client)
{
   RPCTicketPtr ticket = createFireTicket(client);
   if (!ticket)
   {
      TGE::Con::printf("Error: can't make RPC_RequestInventory ticket!\n");
      return RPCTicketPtr();
   }

   TGE::Con::printf("Firing ticket with request id %u\n", ticket->mInfo.mRequestID);

   if (paramQueryID == RPC_RequestInventory::Request_InventoryID)
   {
      StackMemStream<sizeof(OVPacketHeader) + 6 + sizeof(OVPacketHeader) + 4> packetStream;
      PacketBuilder packet(&packetStream);
      packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, 2, false);

      packetStream.writeUintBE(paramUID);

      packet.commit();
      
      client.onQueueWriteData( PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr) );
   }
   else
   {
      StackMemStream<sizeof(OVPacketHeader) + 6 + sizeof(OVPacketHeader) + 8> packetStream;
      PacketBuilder packet(&packetStream);
      packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, 1, false);

      packetStream.writeUintBE(paramUID);
      packetStream.writeUintBE(paramObjectType);

      packet.commit();
      client.onQueueWriteData( PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr) );
   }

   return ticket;
}

void RPC_RequestInventory::handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
{
   MemStream stream(info.size, info.data);

   TGE::Con::printf("Handling incomming RPC_RequestInventory packet %u\n", info.code);

   if (info.code != 0)
   {
      TGE::Con::printf("Warning: unknown RPC code %u\n", info.code);
   }

   client.endRPCTicket(requestTicket);
}

void RPC_NotifyTransaction::fireQueryBalance(OVProtocolHandler& client, RPCTicketPtr ticket)
{
   StackMemStream<sizeof(OVPacketHeader) + 6 + sizeof(OVPacketHeader) + 4> packetStream;
   PacketBuilder packet(&packetStream);
   packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, 3, false);

   packetStream.writeUintBE(paramUID);

   packet.commit();
   client.onQueueWriteData( PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr) );
}

void RPC_NotifyTransaction::fireTransactionInfo(OVProtocolHandler& client, RPCTicketPtr ticket)
{
   StackMemStream<sizeof(OVPacketHeader) + 6 + sizeof(OVPacketHeader) + (4*6)> packetStream;
   PacketBuilder packet(&packetStream);
   packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, 1, false);

   packetStream.writeUintBE(paramUID);
   packetStream.writeUintBE(paramObjectType);
   packetStream.writeUintBE(paramActionID);
   packetStream.writeIntBE(paramCCDiff);
   packetStream.writeIntBE(paramPPDiff);
   packetStream.writeUintBE(paramServerID);

   packet.commit();
   client.onQueueWriteData( PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr) );
}

RPCTicketPtr RPC_NotifyTransaction::fire(OVProtocolHandler& client)
{
   RPCTicketPtr ticket = createFireTicket(client);
   if (!ticket)
   {
      TGE::Con::printf("Error: can't make RPC_NotifyTransaction ticket!\n");
      return RPCTicketPtr();
   }

   TGE::Con::printf("Firing ticket with request id %u\n", ticket->mInfo.mRequestID);
   ticket->mInfo.mUserVar1 = paramUID;
   
   if (paramQueryBalance)
   {
      fireQueryBalance(client, ticket);
   }
   else
   {
      fireTransactionInfo(client, ticket);
   }

   return ticket;
}

void RPC_NotifyTransaction::handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
{
   TGE::Con::printf("Handling incomming RPC_NotifyTransaction packet %u size %u\n", info.code, info.size);

   if (info.code != 0 && info.code != 2)
   {
      TGE::Con::printf("Warning: unknown RPC code %u\n", info.code);
   }

   if (info.code == 2)
   {
      MemStream stream(info.size, info.data);
      uint64_t txid=0;
      stream.readUintBE(txid);

      uint32_t connectionID = 0;
      std::unordered_map<uint32_t, MasterUserInfo>::iterator userInfoItr = gUserHashIDLookup.find(requestTicket->mInfo.mUserVar1);
      if (userInfoItr != gUserHashIDLookup.end())
      {
         connectionID = userInfoItr->second.connectionID;
      }

      char buf1[64];
      snprintf(buf1, 64, "%u", connectionID);
      char buf2[64];
      snprintf(buf2, 64, "%u", requestTicket->mInfo.mUserVar1);
      char buf3[64];
      snprintf(buf3, 64, "%u", requestTicket->mInfo.mRequestID);
      char buf4[64];
      snprintf(buf4, 64, "%llu", txid);

      const char* args[] = {"OnMasterTransaction", buf1, buf2, buf3, buf4};
	  TGE::Con::execute(5, args);

   }

   client.endRPCTicket(requestTicket);
}

void RPC_CommitInventory::fireStringRequest(OVProtocolHandler& client, RPCTicketPtr ticket, uint16_t packet_id, uint32_t uid, uint32_t type_id, std::string &values)
{
   MemStream packetStream(4096);
   PacketBuilder packet(&packetStream);
   packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, packet_id, false);

   packetStream.writeUintBE(uid);
   packetStream.writeUintBE(type_id);
   packetStream.write(values.size(), values.c_str());

   OVFreeableData* dat = packet.commitToEncodedPacket(&client.mEncryptionKey);
   if (dat) client.onQueueWriteData( dat );
}

void RPC_CommitInventory::fireParamRequest(OVProtocolHandler& client, RPCTicketPtr ticket, uint16_t packet_id, uint32_t uid, uint32_t value)
{
   StackMemStream<sizeof(OVPacketHeader) + 6 + sizeof(OVPacketHeader) + (4*2)> packetStream;
   PacketBuilder packet(&packetStream);
   packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, packet_id, false);

   packetStream.writeUintBE(uid);
   packetStream.writeUintBE(value);

   packet.commit();
   client.onQueueWriteData( PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr) );
}

RPCTicketPtr RPC_CommitInventory::fire(OVProtocolHandler& client)
{
   if (paramSubmitType > SUBMIT_UPDATE_ID)
   {
      TGE::Con::printf("Error: invalid submit type!\n");
      return RPCTicketPtr();
   }

   RPCTicketPtr ticket = createFireTicket(client);
   if (!ticket)
   {
      TGE::Con::printf("Error: can't make RPC_CommitInventory ticket!\n");
      return RPCTicketPtr();
   }

   TGE::Con::printf("Firing ticket with request id %u\n", ticket->mInfo.mRequestID);

   switch (paramSubmitType)
   {
      case SUBMIT_REPLACE:
         fireStringRequest(client, ticket, SUBMIT_REPLACE, paramUID, paramObjectType, paramInventoryData);
      break;
      case SUBMIT_ADD:
         fireStringRequest(client, ticket, SUBMIT_ADD, paramUID, paramObjectType, paramInventoryData);
      break;
      case SUBMIT_REMOVE:
         fireStringRequest(client, ticket, SUBMIT_REMOVE, paramUID, paramObjectType, paramInventoryData);
      break;
      case SUBMIT_UPDATE_ID:
         fireParamRequest(client, ticket, SUBMIT_UPDATE_ID, paramUID, paramLastID);
      break;
   }

   return ticket;
}

void RPC_CommitInventory::handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
{
   TGE::Con::printf("Handling incomming RPC_CommitInventory packet %u\n", info.code);

   if (info.code != 0)
   {
      TGE::Con::printf("Warning: unknown RPC code %u\n", info.code);
   }

   client.endRPCTicket(requestTicket);
}

RPCTicketPtr RPC_ExpendUserLoginQueue::fire(OVProtocolHandler& client)
{
   RPCTicketPtr ticket = createFireTicket(client);
   if (!ticket)
   {
      TGE::Con::printf("Error: can't make RPC_ExpendUserLoginQueue ticket!\n");
      return RPCTicketPtr();
   }

   TGE::Con::printf("Firing ticket with request id %u\n", ticket->mInfo.mRequestID);
   
   StackMemStream<sizeof(OVPacketHeader) + 6 + sizeof(OVPacketHeader) + 4> packetStream;
   PacketBuilder packet(&packetStream);
   packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, 1, false);

   packetStream.writeUintBE(paramAcceptState);

   packet.commit();
   
   client.onQueueWriteData( PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr) );
   return ticket;
}

void RPC_ExpendUserLoginQueue::handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
{
   TGE::Con::printf("Handling incomming RPC_ExpendUserLoginQueue packet %u\n", info.code);

   if (info.code != 0)
   {
      TGE::Con::printf("Warning: unknown RPC code %u\n", info.code);
   }

   client.endRPCTicket(requestTicket);
}

RPCTicketPtr RPC_NotifyUserPresence::fire(OVProtocolHandler& client)
{
   RPCTicketPtr ticket = createFireTicket(client);
   if (!ticket)
   {
      TGE::Con::printf("Error: can't make RPC_NotifyUserPresence ticket!\n");
      return RPCTicketPtr();
   }

   TGE::Con::printf("Firing ticket with request id %u\n", ticket->mInfo.mRequestID);
   
   StackMemStream<sizeof(OVPacketHeader) + 6 + sizeof(OVPacketHeader) + 4 + 1> packetStream;
   PacketBuilder packet(&packetStream);
   packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, 1, false);

   packetStream.writeUintBE(paramUID);
   packetStream.write(paramUIDPresence);

   packet.commit();

   
   client.onQueueWriteData( PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr) );
   return ticket;
}

void RPC_NotifyUserPresence::handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
{
   TGE::Con::printf("Handling incomming RPC_NotifyUserPresence packet %u\n", info.code);

   if (info.code != 0)
   {
      TGE::Con::printf("Warning: unknown RPC code %u\n", info.code);
   }

   client.endRPCTicket(requestTicket);
}

RPCTicketPtr RPC_CommitHousePlot::fire(OVProtocolHandler& client)
{
   RPCTicketPtr ticket = createFireTicket(client);
   if (!ticket)
   {
      TGE::Con::printf("Error: can't make RPC_CommitHousePlot ticket!\n");
      return RPCTicketPtr();
   }

   TGE::Con::printf("Firing ticket with request id %u\n", ticket->mInfo.mRequestID);
   
   MemStream packetStream(4096);
   PacketBuilder packet(&packetStream);
   packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, 1, false);

   packetStream.writeUintBE(paramOwnerID);
   packetStream.writeUintBE(paramPlotID);
   packetStream.write(paramData.size(), paramData.c_str());

   OVFreeableData* dat = packet.commitToEncodedPacket(&client.mEncryptionKey);
   if (dat) client.onQueueWriteData(dat);

   return ticket;
}

void RPC_CommitHousePlot::handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
{
   TGE::Con::printf("Handling incomming RPC_CommitHousePlot packet %u\n", info.code);

   if (info.code != 0)
   {
      TGE::Con::printf("Warning: unknown RPC code %u\n", info.code);
   }

   client.endRPCTicket(requestTicket);
}

void Packet_UserLoginQueued::handle(OVProtocolHandler& client, OVPacketInfo info)
{
   MemStream stream(info.size, info.data);
   MasterUserInfo master_info;

   stream.readUintBE(master_info.uid);
   stream.readUintBE(master_info.permissions);
   stream.readUintBE(master_info.invID);
   stream.read(master_info.isVIP);
   stream.read(master_info.avatarSex);
   stream.read(20, &master_info.code);
   stream.readString(master_info.username);
   
   master_info.permissions &= UserPermissions::LEVEL_VALUE_MASK;

   TGE::Con::printf("Expecting user %s(%u) to login with hash %x%x%x\n", master_info.username.c_str(), master_info.uid, master_info.code[0], master_info.code[1], master_info.code[2]);

   gUserHashNameLookup[master_info.username] = master_info.uid;
   gUserHashIDLookup[master_info.uid] = master_info; 

   RPC_ExpendUserLoginQueue* handler = (RPC_ExpendUserLoginQueue*)RPCPacketHandler::getHandlerForRPCCommand(9006);
   handler->paramAcceptState = 1;
   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());
   if (!ticket)
   {
      TGE::Con::printf("Couldn't submit advance queue ticket!\n");
   }

}

void Packet_UserShouldBeKicked::handle(OVProtocolHandler& client, OVPacketInfo info)
{
   MemStream stream(info.size, info.data);

   uint32_t uid;
   stream.readUintBE(uid);

   std::unordered_map<uint32_t, MasterUserInfo>::iterator userInfoItr = gUserHashIDLookup.find(uid);
   if (userInfoItr != gUserHashIDLookup.end())
   {
      TGE::Con::printf("Expecting UID %u(%s) to be kicked\n", uid, userInfoItr->second.username.c_str());

      char buf[64];
      snprintf(buf, 64, "%u", userInfoItr->second.connectionID);

      const char* args[] = {"OnMasterServerShouldKickUser", buf};
	  TGE::Con::execute(2, args);


      gUserHashNameLookup.erase(userInfoItr->second.username);
      gUserHashIDLookup.erase(userInfoItr);
   }
   else
   {
      TGE::Con::printf("Expecting UID %u to be kicked but we don't know about them\n", uid);
   }

   // Make an RPC_ExpendUserLoginQueue request to confirm

}

std::string gTempString;

void Packet_InventoryData::handle(OVProtocolHandler& client, OVPacketInfo info)
{
   MemStream stream(info.size, info.data);

   uint32_t uid;
   uint32_t inv_id;
   stream.readUintBE(uid);
   stream.readUintBE(inv_id);
   TGE::Con::printf("Packet_InventoryData %u %u...\n", uid, inv_id);

   std::unordered_map<uint32_t, MasterUserInfo>::iterator userInfoItr = gUserHashIDLookup.find(uid);
   if (userInfoItr != gUserHashIDLookup.end())
   {
      uint32_t string_size = info.size - stream.getPosition();
      gTempString = std::string(string_size+1, 'x');
      stream.read(string_size, &gTempString[0]);
      gTempString[string_size] = '\0';

      char buf1[64];
      char buf2[64];
      snprintf(buf1, 64, "%u", userInfoItr->second.connectionID);
      snprintf(buf2, 64, "%u", inv_id);

      const char* args[] = {"OnMasterUserInventory", buf1, buf2, gTempString.c_str()};
	  TGE::Con::execute(4, args);
   }
}

void Packet_HousePlot::handle(OVProtocolHandler& client, OVPacketInfo info)
{
   MemStream stream(info.size, info.data);

   uint32_t deed_id;
   uint32_t plot_id;
   uint32_t owner_id;
   uint32_t type_id;
   uint32_t max_furniture;
   uint32_t cc;
   uint32_t pp;
   std::string name;

   stream.readUintBE(deed_id);
   stream.readUintBE(plot_id);
   stream.readUintBE(owner_id);
   stream.readUintBE(type_id);
   stream.readUintBE(max_furniture);
   stream.readUintBE(cc);
   stream.readUintBE(pp);
   stream.readString(name);

   uint32_t string_size = info.size - stream.getPosition();
   gTempString = std::string(string_size+1, 'x');
   stream.read(string_size, &gTempString[0]);
   gTempString[string_size] = '\0';

   char buf1[64];
   char buf2[64];
   char buf3[64];
   char buf4[64];
   char buf5[64];
   char buf6[64];
   char buf7[64];
   snprintf(buf1, 64, "%u", deed_id);
   snprintf(buf2, 64, "%u", plot_id);
   snprintf(buf3, 64, "%u", owner_id);
   snprintf(buf4, 64, "%u", type_id);
   snprintf(buf5, 64, "%u", max_furniture);
   snprintf(buf6, 64, "%u", cc);
   snprintf(buf7, 64, "%u", pp);

   const char* args[] = {"OnMasterHousePlotData", buf1, buf2, buf3, buf4, buf5, buf6, buf7, name.c_str(), gTempString.c_str()};
   TGE::Con::execute(10, args);
}

void Packet_UserInventoryId::handle(OVProtocolHandler& client, OVPacketInfo info)
{
   MemStream stream(info.size, info.data);

   uint32_t uid;
   uint32_t inv_id;
   stream.readUintBE(uid);
   stream.readUintBE(inv_id);

   std::unordered_map<uint32_t, MasterUserInfo>::iterator userInfoItr = gUserHashIDLookup.find(uid);
   if (userInfoItr != gUserHashIDLookup.end())
   {
      userInfoItr->second.invID = inv_id;
      char buf1[64];
      char buf2[64];
      snprintf(buf1, 64, "%u", userInfoItr->second.connectionID);
      snprintf(buf2, 64, "%u", inv_id);

      const char* args[] = {"OnMasterUserInvId", buf1, buf2};
	  TGE::Con::execute(3, args);
   }
}

void Packet_ProcessManagement::handle(OVProtocolHandler& client, OVPacketInfo info)
{
   MemStream stream(info.size, info.data);

   uint32_t code = 0;
   uint32_t param = 0;

   stream.readUintBE(code);
   stream.readUintBE(param);

   char buf1[64];
   char buf2[64];
   snprintf(buf1, 64, "%u", code);
   snprintf(buf2, 64, "%u", param);
   
   const char* args[] = {"OnMasterProcessManagement", buf1, buf2};
   TGE::Con::execute(3, args);
}

void Packet_ServerInfo::handle(OVProtocolHandler& client, OVPacketInfo info)
{
   MemStream stream(info.size, info.data);

   MasterUserInfo master_info;

   int32_t locationID=-1;
   int32_t instanceID=-1;
   char locationBuf[64];
   char instanceBuf[64];
   std::string serverURL;

   stream.readString(serverURL);

   stream.readIntBE(locationID);
   stream.readIntBE(instanceID);

   TGE::Con::printf("DBG: ServerInfo url=%s lid=%i iid=%i\n", serverURL.c_str(), locationID, instanceID);

   snprintf(locationBuf, sizeof(locationBuf), "%i", locationID);
   snprintf(instanceBuf, sizeof(instanceBuf), "%i", instanceID);
   
   const char* args[] = {"OnMasterServerRegistered", locationBuf, instanceBuf, serverURL.c_str()};
   TGE::Con::execute(4, args);
}

void Packet_UserBalance::handle(OVProtocolHandler& client, OVPacketInfo info)
{
   MemStream stream(info.size, info.data);

   uint32_t uid=0;
   int32_t cc=0;
   int32_t pp=0;
   stream.readUintBE(uid);
   stream.readIntBE(cc);
   stream.readIntBE(pp);

   std::unordered_map<uint32_t, MasterUserInfo>::iterator userInfoItr = gUserHashIDLookup.find(uid);
   if (userInfoItr != gUserHashIDLookup.end())
   {
      char buf1[64];
      char buf2[64];
      char buf3[64];
      snprintf(buf1, 64, "%u", userInfoItr->second.connectionID);
      snprintf(buf2, 64, "%i", cc);
      snprintf(buf3, 64, "%i", pp);

      const char* args[] = {"OnMasterUserBalance", buf1, buf2, buf3};
	  TGE::Con::execute(4, args);
   }
}
