#include "OVOnlinePlugin.h"
#include "OVOnlineClient.h"


#include "sha1.hpp"

InternalBitStream gPacketStream(NULL, 0);
U8 gPacketBuffer[TGE::MaxPacketDataSize];

using namespace OVOnlineClient;

TorqueOverrideMember(U32, OnverseGame::scriptMain, (TGE::OnverseGame* thisObj, int argc, const char **argv), replacementGame_scriptMain)
{
	const char* entryScript = "onverse/client/clientMain.cs";
	const char* modPathList[] = {
		"cache",
		"cfg",
		"onverse",
		"admin",
		"uiEditor",
		"builder"
	};

	const char* utilScripts[] = {
		"uiEditor/main.cs",
		"admin/main.cs",
		"builder/main.cs"
	};


	// Start asio async service

	// NOTE: this needs to be set to avoid the patcher running (without -nopatch)
	// it also does something with console prints...
	bool* gCompilerMode = (bool*)TGEADDR_CON_COMPILERMODE;
	//Con::evaluate("enableWinConsole(1);", false, NULL);

	// Special case for if we just specify a script name
	if (argc == 2)
	{
		U32 len = strlen(argv[1]);
		if (len > 3) // e.g. "shit"
		{
			const char* extV = argv[1] + len - 3;
			if (strcmp(extV, ".cs") == 0 || strcmp(extV, ".tmp") == 0)
			{
				*gCompilerMode = true;
				entryScript = argv[1];
			}

		}
		argc--;
	}

	// Generic case for when we have flags
	if (argc > 1)
	{
		if (strlen(argv[1]) == 7 && strcasecmp(argv[1], "--entry") == 0)
		{
			const char* extV = strrchr(argv[2], '.');
			if (extV && (strcmp(extV, ".cs") == 0 || strcmp(extV, ".tmp") == 0))
			{
				*gCompilerMode = true;
				entryScript = argv[2];
			}
		}
	}

	// Set up the command line args for the console scripts...
	TGE::Con::setIntVariable("Game::argc", argc);
	for (U32 i = 0; i < argc; i++)
	{
		char buf[64];
		snprintf(buf, 64, "Game::argv%d", i);
		TGE::Con::setVariable(buf, argv[i]);

		if (strcasecmp(argv[i], "--headless") == 0)
		{
#ifdef __APPLE__
			TGE::MacCarbPlatState* platState = ((TGE::MacCarbPlatState*)kPlatState);
			platState->headless = true;
#endif
		}
	}

	TGE::ResManager* ResourceManager = TGE::ResourceManager;
	TGE::_StringTable* StringTable = TGE::StringTable;
	ResourceManager->setModPaths(sizeof(modPathList) / sizeof(modPathList[0]), modPathList);

	FILE* entryFS = fopen(entryScript, "rb");
	if (entryFS)
	{
		fseek(entryFS, 0, SEEK_END);
		int size = ftell(entryFS);
		fseek(entryFS, 0, SEEK_SET);

		char* script = new char[size + 1];
		script[size] = '\0';
		fread(script, size, 1, entryFS);
		fclose(entryFS);

		TGE::Con::evaluate(script, false, entryScript);

		if (script != NULL)
			delete[] script;

		if (!*gCompilerMode)
		{
			for (int i = 0; i<sizeof(utilScripts) / sizeof(utilScripts[0]); i++)
			{
				TGE::Con::exec(utilScripts[i], 0, 0, 1);
			}

			TGE::Con::evaluate("onStart();", false, NULL);
		}

	}
	else
	{
		if (TGE::Con::exec(entryScript, 0, 0, true) == 0)
		{
			char buffer[1024];
			snprintf(buffer, sizeof(buffer), "Failed to open \"%s\".", entryScript);
			TGE::Con::errorf(buffer);
			TGE::Platform::alertOK("Error", buffer);
			return 1;
		}
		else
		{

			if (!*gCompilerMode)
			{
				for (int i = 0; i<sizeof(utilScripts) / sizeof(utilScripts[0]); i++)
				{
					TGE::Con::exec(utilScripts[i], 0, 0, true);
				}

				TGE::Con::evaluate("onStart();", false, NULL);
			}

		}
	}

	return 0;
}

// Allow network listen to work

// netNum = ([0] = '\x7f', [1] = '\0', [2] = '\0', [3] = '\x01')
// port = 58066

bool OnReadNetConnectRequest(TGE::GameConnection* connection, InternalBitStream &pStream, const char **errorString)
{
	U32 classGroup, classCRC;
	pStream.read(&classGroup);
	pStream.read(&classCRC);

	if (classGroup == connection->mNetClassGroup)// && classCRC == AbstractClassRep::getClassCRC(mNetClassGroup))
		return true;

	*errorString = "CHR_INVALID";
	return false;
}

bool OnReadConnectRequest(TGE::GameConnection* connection, InternalBitStream &pStream, const char **errorString)
{
	if (!OnReadNetConnectRequest(connection, pStream, errorString))
		return false;

	U32 currentProtocol = 0, minProtocol = 0;

	pStream.read(&currentProtocol);
	pStream.read(&minProtocol);

	TGE::Con::printf("protocol=%u\n", currentProtocol);

	if (currentProtocol < TGE::MinRequiredProtocolVersion)
	{
		*errorString = "CHR_PROTOCOL_LESS";
		return false;
	}
	if (minProtocol > TGE::CurrentProtocolVersion)
	{
		*errorString = "CHR_PROTOCOL_GREATER";
		return false;
	}

	connection->mProtocolVersion = currentProtocol < TGE::CurrentProtocolVersion ? currentProtocol : TGE::CurrentProtocolVersion;

	char uname[256];
	pStream.readString(uname);

	// SHA1(AddrDigest + SHA1(PASSWORD))
	uint8_t pwdHash[20];
	pStream.readBits(20 * 8, pwdHash);

	std::unordered_map<std::string, uint32_t>::iterator userIDItr = gUserHashNameLookup.find(uname);
	uint32_t userID = userIDItr == gUserHashNameLookup.end() ? 0 : userIDItr->second;
	std::unordered_map<uint32_t, MasterUserInfo>::iterator userInfoItr = gUserHashIDLookup.find(userID);
	MasterUserInfo userInfo = (userInfoItr == gUserHashIDLookup.end()) ? MasterUserInfo() : userInfoItr->second;

	// Set user info
	if (userInfoItr != gUserHashIDLookup.end())
	{
		// Need to SHA the addr
		uint8_t fullDigest[36];
		uint8_t ourHash[20];
		memcpy(&fullDigest[0], &userInfo.code[0], 20);
		memcpy(&fullDigest[20], &connection->mAddressDigest[0], 16);

        sha1 hash;
        hash.add(&fullDigest[0], 36);
        hash.finalize();
        hash.dump(&ourHash[0]);

		TGE::Con::printf("Address digest is: %x %x %x %x\n", connection->mAddressDigest[0], connection->mAddressDigest[1], connection->mAddressDigest[2], connection->mAddressDigest[3]);
		TGE::Con::printf("pwd hash is: %x %x %x %x\n", userInfo.code[0], userInfo.code[1], userInfo.code[2], userInfo.code[3]);

		if (memcmp(&ourHash[0], &pwdHash[0], 20) == 0)
		{
			TGE::Con::printf("Password matched!\n");
			connection->mUsername = TorqueStringDup(userInfo.username.c_str());
			connection->mUserAccID = userInfo.uid;
			connection->mUserAuthLevel = userInfo.permissions;
			connection->mIsSubscriber = userInfo.isVIP;
		}
		else
		{
			TGE::Con::printf("Incorrect password used for user %s\n", uname);
			*errorString = "CHR_LOGIN";
			return false;
		}
	}
	else if (gSecureLoginOnly != 0)
	{
		TGE::Con::printf("Unregistered user %s tried to login!\n", uname);
		*errorString = "CHR_LOGIN";
		return false;
	}
	else
	{
		TGE::Con::printf("Allowing anonymous login for user %s\n", uname);
	}

	pStream.read(&connection->mConnectArgc);
	if (connection->mConnectArgc > TGE::MaxConnectArgs)
	{
		*errorString = "CR_INVALID_ARGS";
		return false;
	}

	const char *connectArgv[TGE::MaxConnectArgs + 3];

	for (int i = 0; i<connection->mConnectArgc; i++) {
		char argString[256];
		pStream.readString(argString);
		connection->mConnectArgv[i] = TorqueStringDup(argString);
		connectArgv[i + 3] = connection->mConnectArgv[i];
		TGE::Con::printf("argv[%i] = \"%s\"\n", i + 3, connectArgv[i + 3]);
	}

	TGE::Con::printf("connection argc == %u\n", connection->mConnectArgc);

	connectArgv[0] = "onConnectRequest";
	char buffer[256];
	TGE::Net::addressToString(connection->getNetAddress(), buffer);
	connectArgv[2] = buffer;

	const char *ret = TGE::Con::executeObj(connection, connection->mConnectArgc + 3, connectArgv);
	if (ret[0])
	{
		*errorString = ret;
		return false;
	}

	return true;
}

void OnConnectionRequest(void* in_stream, TGE::PacketReceiveEvent* in_prEvent)
{
	InternalBitStream* stream = (InternalBitStream*)in_stream;
	TGE::PacketReceiveEvent* prEvent = (TGE::PacketReceiveEvent*)in_prEvent;
	TGE::NetAddress address = in_prEvent->sourceAddress;

	InternalBitStream pStream(stream->dataPtr, stream->bufSize);

	U8 packetNum; U32 connectSequence;

	pStream.readBits(8, &packetNum);
	pStream.read(&connectSequence);

	// We're gonna have to make a connection
	TGE::NetConnection *connect = TGE::NetConnection::lookup(&prEvent->sourceAddress);

	if (connect && connect->getSequence() == connectSequence)
	{
		TGE::Con::printf("Connection already exists, sending accept...\n");
		TGE::GNet->sendConnectAccept(connect);
		return;
	}

	TGE::Con::printf("OnConnectionRequest checking digest sequence=%u\n", connectSequence);

	U32 addressDigest[4];
	U32 computedAddressDigest[4];

	pStream.read(&addressDigest[0]);
	pStream.read(&addressDigest[1]);
	pStream.read(&addressDigest[2]);
	pStream.read(&addressDigest[3]);

	TGE::Con::printf("REQUEST Address digest is: %x %x %x %x\n", addressDigest[0], addressDigest[1], addressDigest[2], addressDigest[3]);


	TGE::GNet->computeNetMD5(&address, connectSequence, &computedAddressDigest[0]);
	if (addressDigest[0] != computedAddressDigest[0] ||
		addressDigest[1] != computedAddressDigest[1] ||
		addressDigest[2] != computedAddressDigest[2] ||
		addressDigest[3] != computedAddressDigest[3])
	{
		return;
	}

	TGE::Con::printf("OnConnectionRequest seems valid digest\n");

	if (connect)
	{
		TGE::Con::printf("Connect already exists, checking sequence\n");
		if (connect->getSequence() > connectSequence)
			return; // the existing connection should be kept - the incoming request is stale.
		else
			connect->deleteObject(); // disconnect this one, and allow the new one to be created.
	}

	TGE::Con::printf("Digest and sequence ok, checking other stuff...\n");


	// At this point we need to read the class & init it.

	char __klass[512];
	pStream.readString(__klass);

	// This is the only thing it sends really
	if (strcasecmp(__klass, "GameConnection") != 0)
	{
		TGE::Con::printf("Invalid connect class, aborting...\n");
		if (connect) connect->deleteObject();
		return;
	}

	// CREATE CO
	TGE::ConsoleObject *_co = TGE::ConsoleObject::create("GameConnection");
	U8* ptr = ((U8*)_co) - 160;

	TGE::GameConnection *conn = (TGE::GameConnection*)(_co);
	if (!conn)
	{
		return;
	}

	TGE::Con::printf("co=%x, nc=%x\n", _co, ptr);

	if (!conn->registerObject())
	{
		TGE::Con::printf("Error registering net connection!!\n");
		return;
	}

	conn->setNetAddress(&prEvent->sourceAddress);
	conn->setNetworkConnection(true);
	conn->setSequence(connectSequence);
	memcpy(&conn->mAddressDigest[0], &addressDigest[0], sizeof(addressDigest));


	const char *errorString = NULL;

	TGE::Con::printf("Reading connect request\n");
	if (!OnReadConnectRequest(conn, pStream, &errorString))
	{
		TGE::Con::printf("Sending connect reject reason:%s\n", errorString);
		TGE::GNet->sendConnectReject(conn, errorString);
		conn->deleteObject();
		return;
	}

	TGE::Con::printf("Everything seems ok, proceeding...\n");

	conn->setNetworkConnection(true); // NetConnection
	TGE::Con::printf("setNetworkConnection..\n");
	conn->onConnectionEstablished(false); //
	TGE::Con::printf("onConnectionEstablished..\n");
	conn->setEstablished();
	TGE::Con::printf("setEstablished..\n");
	conn->setConnectSequence(connectSequence);
	TGE::Con::printf("setConnectSequence..\n");
	TGE::GNet->sendConnectAccept(conn);
	TGE::Con::printf("sendConnectAccept..\n");
}

TorqueOverrideMember(void, NetInterface::processPacketReceiveEvent, (TGE::NetInterface* thisObj, TGE::PacketReceiveEvent *prEvent), origprocessPacketReceiveEvent)
{
	U32 dataSize = prEvent->size - 16;
	InternalBitStream pStream(prEvent->data, dataSize);

	if (*pStream.dataPtr == 32)
	{
		TGE::Con::printf("processPacketReceiveEvent OnConnectionRequest\n");
		OnConnectionRequest(&pStream, prEvent);
	}
	else
	{
		origprocessPacketReceiveEvent(thisObj, prEvent);
	}
}



TorqueOverride(bool, Con::exec, (const char* filename, bool noCalls, bool inJournal, bool baseFrame), origCon_exec)
{
	bool journal = false;
	bool ret = false;
	char scriptFilenameBuffer[1024];
	U32* execDepth = (U32*)TGEADDR_CON_EXECDEPTH;
	U32* journalDepth = (U32*)TGEADDR_CON_JOURNALDEPTH;
	TGE::OnverseGame* Game = TGE::Game;
	TGE::ResManager* ResourceManager = TGE::ResourceManager;
	TGE::_StringTable* StringTable = TGE::StringTable;

	execDepth[0]++;
	if (journalDepth[0] >= execDepth[0])
		journalDepth[0] = execDepth[0] + 1;
	else
		journal = true;


	if (inJournal && !journal)
	{
		journal = true;
		journalDepth[0] = execDepth[0];
	}


	// Determine the filename we actually want...
	TGE::Con::expandScriptFilename(scriptFilenameBuffer, sizeof(scriptFilenameBuffer), filename);

	const char *ext = strrchr(scriptFilenameBuffer, '.');

	if (!ext)
	{
		// We need an extension!
		TGE::Con::errorf(TGE::ConsoleLogEntry::Script, "exec: invalid script file name %s.", scriptFilenameBuffer);
		execDepth[0]--;
		return false;
	}


	// Check Editor Extensions
	bool isEditorScript = false;
	// If the script file extention is '.ed.cs' then compile it to a different compiled extention
	const char *edExt = strchr(scriptFilenameBuffer, '.');
	if (edExt != NULL && ((strcasecmp(edExt, ".ed.cs") == 0) || (strcasecmp(edExt, ".ed.gui") == 0)))
		isEditorScript = true;


	TGE::StringTableEntry scriptFileName = StringTable->insert(scriptFilenameBuffer, false);

	// Is this a file we should compile?
	bool compiled = strcasecmp(ext, ".mis") && !journal && !TGE::Con::getBoolVariable("Scripts::ignoreDSOs", false);

	// If we're in a journalling mode, then we will read the script
	// from the journal file.
	if (journal && Game->isJournalReading())
	{
		char fileNameBuf[256];
		U8 fileRead;
		U32 fileSize;

		Game->journalGetStream()->readString(fileNameBuf);
		Game->journalGetStream()->read(&fileRead);
		if (!fileRead)
		{
			TGE::Con::errorf(TGE::ConsoleLogEntry::Script, "Journal script read (failed) for %s", fileNameBuf);
			execDepth[0]--;
			return false;
		}
		Game->journalReadU32(&fileSize);
		char *script = new char[fileSize + 1];
		Game->journalReadBytes(fileSize, script);
		script[fileSize] = 0;
		TGE::Con::printf("Executing (journal-read) %s.", scriptFileName);
		TGE::CodeBlock *newCodeBlock = TorqueAlloc<TGE::CodeBlock>();
		newCodeBlock->allocBlock();
		newCodeBlock->compileExec(scriptFileName, script, noCalls, baseFrame ? 0 : -1);
		delete[] script;

		execDepth[0]--;
		return true;
	}

	// Ok, we let's try to load and compile the script.
	TGE::ResourceObject *rScr = ResourceManager->find(scriptFileName);
	TGE::ResourceObject *rCom = NULL;

	char nameBuffer[512];
	char* script = NULL;
	U32 scriptSize = 0;
	U32 version;

	TGE::Stream *compiledStream = NULL;
	TGE::FileTime comModifyTime, scrModifyTime;

	// Check here for .edso
	bool edso = false;
	if (strcasecmp(ext, ".edso") == 0)
	{
		edso = true;
		rCom = rScr;
		rScr = NULL;

		rCom->getFileTimes(NULL, &comModifyTime);
		strcpy(nameBuffer, scriptFileName);
	}

	// If we're supposed to be compiling this file, check to see if there's a DSO
	if (compiled && !edso)
	{
		if (isEditorScript)
		{
			dStrcpyl(nameBuffer, sizeof(nameBuffer), scriptFileName, ".edso", NULL);
			rCom = ResourceManager->find(nameBuffer);
		}
		else
		{
			dStrcpyl(nameBuffer, sizeof(nameBuffer), scriptFileName, ".dso", NULL);
			rCom = ResourceManager->find(nameBuffer);
		}

		if (rCom)
			rCom->getFileTimes(NULL, &comModifyTime);
		if (rScr)
			rScr->getFileTimes(NULL, &scrModifyTime);
	}

	// Let's do a sanity check to complain about DSOs in the future.
	//
	// MM:   This doesn't seem to be working correctly for now so let's just not issue
	//    the warning until someone knows how to resolve it.
	//
	//if(compiled && rCom && rScr && Platform::compareFileTimes(comModifyTime, scrModifyTime) < 0)
	//{
	//Con::warnf("exec: Warning! Found a DSO from the future! (%s)", nameBuffer);
	//}

	// If we had a DSO, let's check to see if we should be reading from it.
    if (compiled && rCom)
    {
        // comModifyTime > scrModifyTime?
        S32 comp = TGE::Platform::compareFileTimes(comModifyTime, scrModifyTime);

        //TGE::Con::printf("compareFileTimes=%i %s,%s", comp, filename, nameBuffer);

        if (!rScr || (comp >= 0))
        {
            compiledStream = ResourceManager->openStream(nameBuffer);
            if (compiledStream)
            {
                // Check the version!
                compiledStream->_read(4, &version);
                if (version != 0x25)
                {
                    TGE::Con::warnf("exec: Found an old DSO (%s, ver %d < %d), ignoring.", nameBuffer, version, 0x25);
                    ResourceManager->closeStream(compiledStream);
                    compiledStream = NULL;
                }
            }
        }
	}

	// If we're journalling, let's write some info out.
	if (journal && Game->isJournalWriting())
		Game->journalGetStream()->writeString(scriptFileName, 255);




	if (rScr && !compiledStream)
	{
		// If we have source but no compiled version, then we need to compile
		// (and journal as we do so, if that's required).

		TGE::Stream *s = ResourceManager->openStream(scriptFileName);
		if (journal && Game->isJournalWriting())
			Game->journalGetStream()->write(bool(s != NULL));

		if (s)
		{
			scriptSize = ResourceManager->getSize(scriptFileName);
			script = new char[scriptSize + 1];
			s->_read(scriptSize, script);

			if (journal && Game->isJournalWriting())
			{
				Game->journalWriteU32(scriptSize);
				Game->journalWriteBytes(scriptSize, script);
			}
			ResourceManager->closeStream(s);
			script[scriptSize] = 0;
		}

		if (!scriptSize || !script)
		{
			delete[] script;
			TGE::Con::errorf(TGE::ConsoleLogEntry::Script, "exec: invalid script file %s.", scriptFileName);
			execDepth[0]--;
			return false;
		}

		if (compiled)
		{
			// compile this baddie.
			TGE::Con::printf("Compiling %s...", scriptFileName);
			TGE::CodeBlock *code = TorqueAlloc<TGE::CodeBlock>();
			code->allocBlock();
			code->compile(nameBuffer, scriptFileName, script);
			code->destroyBlock();
			TorqueFree<TGE::CodeBlock>(code);
			code = NULL;

			compiledStream = ResourceManager->openStream(nameBuffer);
			if (compiledStream)
			{
				compiledStream->read(&version);
			}
			else
			{
				// We have to exit out here, as otherwise we get double error reports.
				delete[] script;
				execDepth[0]--;
				return false;
			}
		}
	}
	else
	{

		if (journal && Game->isJournalWriting())
			Game->journalGetStream()->write(bool(false));
	}

	if (compiledStream)
	{
		// Delete the script object first to limit memory used
		// during recursive execs.
		if (script) delete[] script;
		script = 0;


		// We're all compiled, so let's run it.
		TGE::Con::printf("Loading compiled script %s.", scriptFileName);
		TGE::CodeBlock *code = TorqueAlloc<TGE::CodeBlock>();
		code->allocBlock();
		code->read(scriptFileName, compiledStream);
		ResourceManager->closeStream(compiledStream);
		code->exec(0, scriptFileName, NULL, 0, NULL, noCalls, NULL, baseFrame ? -1 : 0);
		execDepth[0]--;
		return true;
	}
	else
	{
		if (rScr)
		{
			// No compiled script,  let's just try executing it
			// directly... this is either a mission file, or maybe
			// we're on a readonly volume.
			TGE::Con::printf("Executing %s.", scriptFileName);
			TGE::CodeBlock *newCodeBlock = TorqueAlloc<TGE::CodeBlock>();
			newCodeBlock->allocBlock();
			TGE::StringTableEntry name = StringTable->insert(scriptFileName, false);

			newCodeBlock->compileExec(name, script, noCalls, baseFrame ? -1 : 0);
			execDepth[0]--;
			delete[] script;
			return true;
		}
		else
		{
			// Don't have anything.
			TGE::Con::warnf(TGE::ConsoleLogEntry::Script, "Missing file: %s!", scriptFileName);
			execDepth[0]--;
			return false;
		}
	}
}

TorqueOverride(bool, Platform::excludeOtherInstances, (const char* name), originalExcludeOtherInstances)
{
	return true;
}

TorqueOverrideMember(S32, TSShape::readName, (TGE::TSShape* thisObj, TGE::Stream *s, bool addName), replacementTSShape_readName)
{
   static char buffer[256];
   uint32_t sz=0;
   S32 nameIndex = -1;
   s->read(&sz);

   if (sz)
   {
      sz = std::min<uint32_t>(sz, 255);
      s->_read(sz,buffer);
      buffer[sz] = '\0';
      nameIndex = thisObj->findName(buffer);
      if (nameIndex<0 && addName)
      {
         nameIndex = thisObj->names.size();
         thisObj->names.increment();
         thisObj->names.last() = TGE::StringTable->insert(buffer,false);
      }
   }

   return nameIndex;
}

PLUGINCALLBACK void preEngineInit(PluginInterface *plugin)
{
}

PLUGINCALLBACK void postEngineInit(PluginInterface *plugin)
{
	// This might seem useless, but it forces the plugin to import from TorqueLib.dll
	// So it lets us test TorqueLib.dll loading
	MathUtils::randomPointInSphere(10);

	TGE::Con::printf("      Hello from %s!", plugin->getPath());

	//TGE::Con::evaluate("schedule(1000, 0, pluginTest);", false, "");
}

PLUGINCALLBACK void engineShutdown(PluginInterface *plugin)
{
}


