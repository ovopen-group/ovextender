/*
OVExtender
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <TorqueLib/TGE.h>
#include "ovopen/packet.hpp"
#include "ovopen/protocolHandler.hpp"
#include <memory>
#include <string>

EXT_NS(OVClientBase)

extern asio::io_service gIOService;
extern std::shared_ptr<asio::io_service::work> gDummyWork;
extern std::shared_ptr<OVProtocolHandler> gProtocolClient;

extern std::thread* gIOThread;
extern tcp::resolver gTCPResolver;
void ASIOLoop();

EXT_NS_END()