/*
OVExtender
Copyright (C) 2020 mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _CLIENT_RPCHANDLER_H_
#define _CLIENT_RPCHANDLER_H_

#include "ovopen/packetHandlers.hpp"
#include "OVOnlineClient.h"

class RPC_RegisterLocationServer : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9001 , OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0
   };

   U32 paramDBID;
   U16 paramPort;
   
   // NEW (only value part used for game client)
   U32 paramAllowedMask;
   
   virtual RPCTicketPtr fire(OVProtocolHandler& client);

   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket);
};

class RPC_SetLocationReadyState : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9002, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0
   };

   U32 paramLocationReadyState;
   bool paramVipOnly;
   
   virtual RPCTicketPtr fire(OVProtocolHandler& client);

   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket);
};

class RPC_RequestInventory : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9003, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0
   };

   enum
   {
      Request_InventoryInfo=1,
      Request_InventoryID=2
   };

   U32 paramQueryID;
   U32 paramUID;
   U32 paramObjectType;

   virtual RPCTicketPtr fire(OVProtocolHandler& client);

   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket);
};

class RPC_NotifyTransaction : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9004, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0
   };

   bool paramQueryBalance;
   U32 paramUID;
   U32 paramObjectType;
   U32 paramActionID;
   S32 paramCCDiff;
   S32 paramPPDiff;
   U32 paramServerID;

   void fireQueryBalance(OVProtocolHandler& client, RPCTicketPtr ticket);

   void fireTransactionInfo(OVProtocolHandler& client, RPCTicketPtr ticket);
   
   virtual RPCTicketPtr fire(OVProtocolHandler& client);

   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket);
};

class RPC_CommitInventory : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9005, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0
   };

   enum
   {
      SUBMIT_REPLACE=1,
      SUBMIT_ADD=2,
      SUBMIT_REMOVE=3,
      SUBMIT_QUERY_ID=4,
      SUBMIT_UPDATE_ID=5
   };

   U32 paramSubmitType;
   U32 paramUID;
   U32 paramObjectType;
   U32 paramLastID;
   std::string paramInventoryData;

   void fireStringRequest(OVProtocolHandler& client, RPCTicketPtr ticket, uint16_t packet_id, uint32_t uid, uint32_t type_id, std::string &values);

   void fireParamRequest(OVProtocolHandler& client, RPCTicketPtr ticket, uint16_t packet_id, uint32_t uid, uint32_t value);

   virtual RPCTicketPtr fire(OVProtocolHandler& client);

   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket);
};

class RPC_ExpendUserLoginQueue : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9006, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0
   };

   U32 paramAcceptState;
   
   virtual RPCTicketPtr fire(OVProtocolHandler& client);

   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket);
};

class RPC_NotifyUserPresence : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9007, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0
   };

   U32 paramUID;
   uint8_t paramUIDPresence;
   
   virtual RPCTicketPtr fire(OVProtocolHandler& client);

   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket);
};

class RPC_CommitHousePlot : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9008, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0
   };

   U32 paramOwnerID;
   U32 paramPlotID;
   std::string paramData;
   
   virtual RPCTicketPtr fire(OVProtocolHandler& client);

   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket);
};

class Packet_UserLoginQueued : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(500, true, OVProtocolHandler::PROTOCOL_BACKEND);
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info);
};

class Packet_UserShouldBeKicked : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(501, true, OVProtocolHandler::PROTOCOL_BACKEND);
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info);
};

class Packet_InventoryData: public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(502, true, OVProtocolHandler::PROTOCOL_BACKEND);
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info);
};

class Packet_HousePlot: public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(503, true, OVProtocolHandler::PROTOCOL_BACKEND);
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info);
};

class Packet_UserInventoryId : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(504, true, OVProtocolHandler::PROTOCOL_BACKEND);
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info);
};

class Packet_ProcessManagement : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(505, true, OVProtocolHandler::PROTOCOL_BACKEND);
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info);
};

class Packet_ServerInfo : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(506, true, OVProtocolHandler::PROTOCOL_BACKEND);
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info);
};

class Packet_UserBalance : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(507, true, OVProtocolHandler::PROTOCOL_BACKEND);
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info);
};

#endif
