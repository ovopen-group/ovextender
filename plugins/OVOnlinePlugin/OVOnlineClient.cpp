/*
OVExtender
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "OVOnlineClient.h"
#include "sha1.hpp"

using namespace OVClientBase;
using namespace OVOnlineClient;

EXT_NS(OVOnlineClient)

MasterClientConnector gMasterConnector(gIOService);

std::unordered_map<std::string, uint32_t> gUserHashNameLookup;
std::unordered_map<uint32_t, MasterUserInfo> gUserHashIDLookup;

U32 gAuthCode = 0;
S32 gSecureLoginOnly = 0;
U32 gLocationReadyState = 255;

EXT_NS_END()


bool OVProtocolHandler::onPreloginPacket(OVPacketHeader* header)
{
	U8 securityCode[8];

	if ((mProtocol & OVProtocolHandler::PROTOCOL_BACKEND_TOOL) != 0)
	{
		TGE::Con::printf("Got tool packet!\n");
		return false;
	}

	TGE::Con::printf("Got prelogin packet!\n");
	OVClientGameConnectionState* state = (OVClientGameConnectionState*)(mState);
	MemStream stream(header->getNativeSize(), ((uint8_t*)header) + 6);
	if (header->getNativeId() == 101 && header->isAuto())
	{
		// Begin authenticaion
		unsigned char hashBuffer[28];
		unsigned char actualHash[20];
		TGE::Con::printf("Got auth code, trying to login...\n");

		stream.read(8, &securityCode[0]);
		memcpy(&hashBuffer[20], &securityCode[0], 8);


        sha1 hash;
        hash.add((const unsigned char*)state->mLoginPassword.c_str(), state->mLoginPassword.size());
        hash.finalize();
        hash.dump(&hashBuffer[0]);

        hash = sha1();
        hash.add(&hashBuffer[0], 28);
        hash.finalize();
        hash.dump(&actualHash[0]);

		//SHA1((const unsigned char*)state->mLoginPassword.c_str(), state->mLoginPassword.size(), &hashBuffer[0]);
		//SHA1(&hashBuffer[0], 28, &actualHash[0]);

		char name[33];
		snprintf(name, 33, "%s", state->mLoginUsername.c_str());
		stream.write(32, name);

		MemStream responsePacket(64);
		OVPacketHeader responseHeader;
		responseHeader.setBasicPacket();
		responseHeader.setNativeId(102);
		responseHeader.setNativeSize(32 + 20);
		responsePacket.write(6, &responseHeader);
		responsePacket.write(32, name);
		responsePacket.write(20, &actualHash[0]);

		PacketMallocData* responseData = PacketMallocData::createFromData(responsePacket.getPosition(), responsePacket.mPtr);
		queuePacketData(responseData);
	}
	else if (header->getNativeId() == 1) // ServerError
	{
		TGE::Con::printf("Got server error\n");
		U32 errorCode = 0;
		stream.readUintBE(errorCode);

		if (gAuthCode != 0)
		{
			const char* args[] = { "OnMasterServerLoginFail" };
			TGE::Con::execute(1, args);
		}
	}
	else if (header->getNativeId() == 103) // AuthSuccess
	{
		stream.readUintBE(mUid);
		TGE::Con::printf("AUTH SUCCESS uid=%u!\n", mUid);
		const char* args[] = { "OnMasterAuthenticated" };
		TGE::Con::execute(1, args);
	}
	else if (header->getNativeId() == 104) // AuthFailure
	{
		gAuthCode = 0;
		stream.readUintBE(gAuthCode);
		TGE::Con::printf("AUTH FAIL code=%u!\n", gAuthCode);
	}

	return true;
}

void OVProtocolHandler::onLogout()
{
	if ((mProtocol & OVProtocolHandler::PROTOCOL_BACKEND_TOOL) == 0)
		return;

	const char* args[] = { "OnMasterServerDisconnect" };
	TGE::Con::execute(1, args);
}
