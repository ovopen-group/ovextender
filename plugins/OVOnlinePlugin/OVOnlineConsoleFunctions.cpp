#include "OVOnlinePlugin.h"
#include "OVOnlineClient.h"
#include "OVRPCHandlers.h"

#include "sha1.hpp"

char gTempBuf[64];
using namespace OVClientBase;
using namespace OVOnlineClient;

ConsoleFunction(MasterSetAcceptAnonymousLogins, void, 2, 2, "MasterSetAcceptAnonymousLogins(bool)")
{
   gSecureLoginOnly = !atoi(argv[1]);
}

ConsoleFunction(MasterSetReadyState, void, 2, 2, "MasterSetReadyState(ready_state)")
{
   gLocationReadyState = strtoul(argv[1], 0, NULL);
   printf("Setting permissions for server to %u vip=%u\n", gLocationReadyState, 0);
   RPC_SetLocationReadyState* handler = (RPC_SetLocationReadyState*)RPCPacketHandler::getHandlerForRPCCommand(9002);
   handler->paramLocationReadyState = gLocationReadyState;
   handler->paramVipOnly = false;
   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());
}

ConsoleFunction(MasterSetUserConnectionId, void, 2, 2, "MasterSetUserConnectionId(uid)")
{
   uint32_t uid = strtoul(argv[1], NULL, 0);
   std::unordered_map<uint32_t, MasterUserInfo>::iterator userInfoItr = gUserHashIDLookup.find(uid);
   if (userInfoItr != gUserHashIDLookup.end())
   {
      userInfoItr->second.connectionID = strtoul(argv[2], NULL, 0);
   }
}

ConsoleFunction(MasterSetUserLoginDetails, void, 5, 6, "MasterSetUserLoginDetails(username, password, uid, permissions, invID)")
{
   const char* username = argv[1];
   const char* password = argv[2];
   uint32_t uid = strtoul(argv[3], NULL, 0);
   uint32_t permissions = strtoul(argv[4], NULL, 0);
   uint32_t invID = argc > 5 ? strtoul(argv[5], NULL, 0) : 0;

   uint8_t pwdHash[20];

   sha1 hash;
   hash.add((const unsigned char*)password, strlen(password));
   hash.finalize();
   hash.dump(&pwdHash[0]);

   //SHA1((const unsigned char*)password, strlen(password), &pwdHash[0]);

   MasterUserInfo master_info;
   master_info.uid = uid;
   master_info.permissions = permissions;
   master_info.username = username;
   master_info.invID = invID;
   master_info.avatarSex = 0;
   memcpy(&master_info.code[0], &pwdHash[0], 20);

   gUserHashNameLookup[username] = master_info.uid;
   gUserHashIDLookup[master_info.uid] = master_info;
}

static char urbuf[512];
ConsoleFunction(MasterGetUserLoginDetails, const char*, 2, 2, "MasterGetUserLoginDetails(uid)")
{
   uint32_t uid = strtoul(argv[1], 0, NULL);

   std::unordered_map<uint32_t, MasterUserInfo>::iterator userInfoItr = gUserHashIDLookup.find(uid);
   if (userInfoItr != gUserHashIDLookup.end())
   {
      MasterUserInfo info = userInfoItr->second;
      snprintf(urbuf, sizeof(urbuf), "%u\t%s\t%u\t%u\t%u\t%u", info.permissions, info.username.c_str(), info.invID, info.isVIP, info.avatarSex, info.connectionID);
      return urbuf;
   }
   else
   {
      printf("MasterGetUserLoginDetails unknown uid %u\n", uid);
	  return "";
   }
}


ConsoleFunction(MasterClearUserLoginDetails, void, 2, 2, "MasterClearUserLoginDetails(uid)")
{
   uint32_t uid = strtoul(argv[1], 0, NULL);

   std::unordered_map<uint32_t, MasterUserInfo>::iterator userInfoItr = gUserHashIDLookup.find(uid);
   if (userInfoItr != gUserHashIDLookup.end())
   {
      gUserHashNameLookup.erase(userInfoItr->second.username);
      gUserHashIDLookup.erase(userInfoItr);
   }
}

ConsoleFunction(MasterSetUsersAllowed, void, 2, 2, "MasterSetUsersAllowed(ready_state)")
{
   gLocationReadyState = strtoul(argv[1], 0, NULL);
}

ConsoleFunction(MasterNotifyUserPresence, const char*, 3, 3, "MasterNotifyUserPresence(uid, type_id)")
{
   uint32_t uid = strtoul(argv[1], 0, NULL);
   uint32_t presence = strtoul(argv[2], 0, NULL);

   if (gMasterConnector.mHandler.get() == NULL)
	   return "";

   RPC_NotifyUserPresence* handler = (RPC_NotifyUserPresence*)RPCPacketHandler::getHandlerForRPCCommand(9007);
   handler->paramUID = uid;
   handler->paramUIDPresence = presence;

   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}

ConsoleFunction(MasterAddUserInventory, const char*, 4, 4, "MasterAddUserInventory(uid, type_id)")
{
   uint32_t uid = strtoul(argv[1], 0, NULL);
   uint32_t type_id = strtoul(argv[2], 0, NULL);
   const char* list = argv[3];

   if (gMasterConnector.mHandler.get() == NULL)
	   return "";

   RPC_CommitInventory* handler = (RPC_CommitInventory*)RPCPacketHandler::getHandlerForRPCCommand(9005);
   handler->paramUID = uid;
   handler->paramObjectType = type_id;
   handler->paramSubmitType = RPC_CommitInventory::SUBMIT_ADD;
   handler->paramInventoryData = std::string(list);

   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}

ConsoleFunction(MasterRemoveUserInventory, const char*, 4, 4, "MasterRemoveUserInventory(uid, type_id)")
{
   uint32_t uid = strtoul(argv[1], 0, NULL);
   uint32_t type_id = strtoul(argv[2], 0, NULL);
   const char* list = argv[3];

   if (gMasterConnector.mHandler.get() == NULL)
	   return "";

   RPC_CommitInventory* handler = (RPC_CommitInventory*)RPCPacketHandler::getHandlerForRPCCommand(9005);
   handler->paramUID = uid;
   handler->paramObjectType = type_id;
   handler->paramSubmitType = RPC_CommitInventory::SUBMIT_REMOVE;
   handler->paramInventoryData = std::string(list);

   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}

ConsoleFunction(MasterGetLastInventoryID, const char*, 2, 2, "MasterGetLastInventoryID(uid)")
{
   uint32_t uid = strtoul(argv[1], 0, NULL);

   if (gMasterConnector.mHandler.get() == NULL)
	   return "";

   RPC_RequestInventory* handler = (RPC_RequestInventory*)RPCPacketHandler::getHandlerForRPCCommand(9005);
   handler->paramUID = uid;
   handler->paramQueryID = RPC_RequestInventory::Request_InventoryID;

   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}

ConsoleFunction(MasterUpdateLastInventoryID, const char*, 3, 3, "MasterUpdateLastInventoryID(uid, lastID)")
{
   uint32_t uid = strtoul(argv[1], 0, NULL);
   uint32_t lastID = strtoul(argv[2], 0, NULL);

   if (gMasterConnector.mHandler.get() == NULL)
	   return "";

   RPC_CommitInventory* handler = (RPC_CommitInventory*)RPCPacketHandler::getHandlerForRPCCommand(9005);
   handler->paramUID = uid;
   handler->paramLastID = lastID;
   handler->paramSubmitType = RPC_CommitInventory::SUBMIT_UPDATE_ID;

   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}

ConsoleFunction(MasterResetUserInventory, const char*, 4, 4, "MasterResetUserInventory(uid, type_id)")
{
   uint32_t uid = strtoul(argv[1], 0, NULL);
   uint32_t type_id = strtoul(argv[2], 0, NULL);
   const char* list = argv[3];

   if (gMasterConnector.mHandler.get() == NULL)
	   return "";

   RPC_CommitInventory* handler = (RPC_CommitInventory*)RPCPacketHandler::getHandlerForRPCCommand(9005);
   handler->paramUID = uid;
   handler->paramObjectType = type_id;
   handler->paramSubmitType = RPC_CommitInventory::SUBMIT_REPLACE;
   handler->paramInventoryData = std::string(list);

   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}

// Callback: onMasterUserInventory(%uid, %type, %values)
ConsoleFunction(MasterGetUserInventory, const char*, 3, 3, "MasterGetUserInventory(uid, type_id)")
{
   uint32_t uid = strtoul(argv[1], 0, NULL);
   uint32_t type_id = strtoul(argv[2], 0, NULL);

   if (gMasterConnector.mHandler.get() == NULL)
	   return "";

   RPC_RequestInventory* handler = (RPC_RequestInventory*)RPCPacketHandler::getHandlerForRPCCommand(9003);
   handler->paramUID = uid;
   handler->paramObjectType = type_id;
   handler->paramQueryID = RPC_RequestInventory::Request_InventoryInfo;

   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}

ConsoleFunction(MasterSetPlotInfo, const char*, 4, 4, "MasterSetPlotInfo(owner_id, plot_id, list)")
{
   uint32_t owner_id = strtoul(argv[1], 0, NULL);
   uint32_t plot_id = strtoul(argv[2], 0, NULL);
   const char* list = argv[3];

   if (gMasterConnector.mHandler.get() == NULL)
	   return "";

   RPC_CommitHousePlot* handler = (RPC_CommitHousePlot*)RPCPacketHandler::getHandlerForRPCCommand(9008);
   handler->paramOwnerID = owner_id;
   handler->paramPlotID = plot_id;
   handler->paramData = std::string(list);

   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}

//

ConsoleFunction(MasterLoginServer, void, 5, 5, "MasterLoginServer(address, port, username password)")
{
   const char* address = argv[1];
   const char* port = argv[2];
   const char* username = argv[3];
   const char* password = argv[4];

   gMasterConnector.beginConnect(gIOService, address, port, username, password);
}

ConsoleFunction(MasterRegisterServer, const char*, 4, 4, "MasterRegisterServer(ident, port, regMask)")
{
   uint32_t ident = strtoul(argv[1], NULL, 0);
   uint16_t port = strtoul(argv[2], NULL, 0);
   uint32_t regMask = strtoul(argv[3], NULL, 0);

   RPC_RegisterLocationServer* handler = (RPC_RegisterLocationServer*)RPCPacketHandler::getHandlerForRPCCommand(9001);
   handler->paramDBID = ident;
   handler->paramPort = port;
   handler->paramAllowedMask = regMask;
   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());
   if (!ticket)
      return "";

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}


ConsoleFunction(MasterNotifyTransaction, const char*, 6, 6, "MasterNotifyTransaction(uid, obj_type, action_id, cc_diff, pp_diff, server_id)")
{
   RPC_NotifyTransaction* handler = (RPC_NotifyTransaction*)RPCPacketHandler::getHandlerForRPCCommand(9004);
   handler->paramQueryBalance = false;
   handler->paramUID = strtoul(argv[1], NULL, 0);
   handler->paramObjectType = strtoul(argv[2], NULL, 0);
   handler->paramActionID = strtoul(argv[3], NULL, 0);
   handler->paramCCDiff = strtol(argv[4], NULL, 0);
   handler->paramPPDiff = strtol(argv[5], NULL, 0);
   handler->paramServerID = strtoul(argv[6], NULL, 0);

   if (gMasterConnector.mHandler.get() == NULL)
	   return "";

   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());
   if (!ticket)
      return "";

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}

ConsoleFunction(MasterQueryBalance, const char*, 2, 2, "MasterQueryBalance(uid)")
{
   RPC_NotifyTransaction* handler = (RPC_NotifyTransaction*)RPCPacketHandler::getHandlerForRPCCommand(9004);
   handler->paramQueryBalance = true;
   handler->paramUID = strtoul(argv[1], NULL, 0);

   if (gMasterConnector.mHandler.get() == NULL)
	   return "";

   RPCTicketPtr ticket = handler->fire(*gMasterConnector.mHandler.get());
   if (!ticket)
      return "";

   snprintf(gTempBuf, sizeof(gTempBuf), "%u", ticket->mInfo.mRequestID);
   return gTempBuf;
}
