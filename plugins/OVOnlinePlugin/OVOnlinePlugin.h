#ifndef _OV_ONLINE_PLUGIN_H_
#define _OV_ONLINE_PLUGIN_H_

#include <cstdio>
#include <TorqueLib/math/mMath.h>
#include <TorqueLib/math/mathUtils.h>
#include <TorqueLib/TGE.h>
#include <TorqueLib/QuickOverride.h>
#include <PluginLoader/PluginInterface.h>

#include <TorqueLib/util/internalBitStream.h>
#include <unordered_map>

inline void UIntToStr(uint32_t num, char* buf)
{
   snprintf(buf, 64, "%u", num);
}

inline void IntToStr(int32_t num, char* buf)
{
   snprintf(buf, 64, "%i", num);
}

#endif
