// Simple plugin for testing stuff

#include <cstdio>
#include <TorqueLib/math/mMath.h>
#include <TorqueLib/math/mathUtils.h>
#include <TorqueLib/TGE.h>
#include <TorqueLib/QuickOverride.h>
#include <PluginLoader/PluginInterface.h>

#include <TorqueLib/util/internalBitStream.h>
#include <unordered_map>

ConsoleFunction(pq, void, 1, 1, "pq()")
{
	TGE::Con::printf("PQ WHERe");
}

ConsoleFunction(testCam, void, 2, 2, "testCam(marble)")
{
	/*TGE::Marble *marble = static_cast<TGE::Marble*>(TGE::Sim::findObject(argv[1]));
	if (marble != NULL)
	{
		MatrixF mat;
		marble->getCameraTransform(NULL, &mat);
		for (int i = 0; i < 4; i++)
			TGE::Con::printf("%f %f %f %f", mat(i, 0), mat(i, 1), mat(i, 2), mat(i, 3));
	}
	else
	{
		TGE::Con::errorf("marble == NULL!");
	}*/
}

ConsoleFunction(idTest, void, 2, 2, "idTest(obj)")
{
	TGE::SimObject *obj = static_cast<TGE::SimObject*>(TGE::Sim::findObject(argv[1]));
	TGE::Con::printf("getId() -> %d", obj->getId());
	TGE::Con::printf("getIdString() -> \"%s\"", obj->getIdString());
}

ConsoleFunction(transformTest, void, 2, 2, "transformTest(obj)")
{
	TGE::SceneObject *obj = static_cast<TGE::SceneObject*>(TGE::Sim::findObject(argv[1]));
	const MatrixF &mat = obj->getTransform();
	for (int i = 0; i < 4; i++)
		TGE::Con::printf("%f %f %f %f", mat(i, 0), mat(i, 1), mat(i, 2), mat(i, 3));
	obj->setTransform(mat);

	const Box3F &box = obj->getWorldBox();
	Point3F center = box.getCenter();
	TGE::Con::printf("Box center: %f %f %f", center.x, center.y, center.z);
}

ConsoleFunction(pluginTest, void, 1, 1, "")
{
	TGE::GuiCanvas* canvas = (TGE::GuiCanvas*)TGE::Sim::findObject("Canvas");
	TGE::Con::printf("Canvas name=%s CWD=%s MS=%u", canvas->objectName, TGE::Platform::getWorkingDirectory(), TGE::Platform::getRealMilliseconds());

	TGE::Con::setBoolVariable("$fudge", true);
	if (TGE::Con::getBoolVariable("$fudge", false))
	{
		TGE::Con::printf("test OK");
	}

	//TGE::Platform::alertOK("OMFG", "Test Alert");

	const TGE::Namespace* ns = TGE::Con::lookupNamespace("GuiControl");
	if (ns)
	{
		TGE::Con::printf("Got ns %x", ns);
	}

	TGE::Con::evaluate("new GameConnection(ConTest) {};", false, "");

	TGE::GameConnection* ref = NULL;
	uintptr_t offs = (uintptr_t) &ref->mUsername;
	uintptr_t offsbo = (uintptr_t)&ref->mBlackOut;
	uintptr_t subofs = (uintptr_t)&ref->mIsSubscriber;
	uintptr_t unk1ofs = (uintptr_t)&ref->unk1;
	uintptr_t unk3ofs = (uintptr_t)&ref->llBlocks;
	uintptr_t lackofs = (uintptr_t)&ref->mLastMoveAck;
	TGE::Con::printf("Con o1=%u o2=%u o3=%u unk1=%u unk3=%u lack=%u", offs, offsbo, subofs, unk1ofs, unk3ofs, lackofs);


	TGE::GameConnection* conn = (TGE::GameConnection*)TGE::Sim::findObject("ConTest");
	TGE::Con::printf("Con name=%s", conn->objectName);
}

PLUGINCALLBACK void preEngineInit(PluginInterface *plugin)
{
}

PLUGINCALLBACK void postEngineInit(PluginInterface *plugin)
{
	// This might seem useless, but it forces the plugin to import from TorqueLib.dll
	// So it lets us test TorqueLib.dll loading
	MathUtils::randomPointInSphere(10);

	TGE::Con::printf("      Hello from %s!", plugin->getPath());

	TGE::Con::evaluate("schedule(1000, 0, pluginTest);", false, "");
}

PLUGINCALLBACK void engineShutdown(PluginInterface *plugin)
{
}