// dllmain.cpp : Defines the entry point for the DLL application.
#include <windows.h>

namespace
{
	bool verifyGame();
}

void installHooks();

namespace
{
	extern "C" __declspec(dllexport) bool initPluginLoader()
	{
		installHooks();
		return true;
	}
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hModule);
		if (!initPluginLoader())
			TerminateProcess(GetCurrentProcess(), 0);
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
