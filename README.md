OVExtender
==========

OVExtender is a fork of MBExtender which patches Onverse executables to provide additional functionality to the game engine by writing plugins in native C++ code. The Windows and Mac versions of the game are all supported. Plugins share a common interface on each platform and can be compiled for other systems with ease.

Compiling
---------

#### Prerequisites ####
OVExtender uses [CMake](http://www.cmake.org/) for its build system. You must install CMake in order to compile OVExtender.

On Windows, Visual Studio 2013 is currently required in order to compile OVExtender.

#### Windows ####
Open CMake, click "Browse Source," and select your OVExtender source folder. Also specify a separate build folder if one is not filled in automatically for you. Click "Configure" and select "Visual Studio 12 2013" as the target. If all goes well, then click Generate to create Visual Studio projects in the build directory.

After projects have been created, open OVExtender.sln in the build directory you chose. Select the configuration you want to use (Debug/Release) and then click "Build Solution" in the build menu. If compilation is successful, right-click the "INSTALL" project in Solution Explorer and build it to install the OVExtender binaries. By default, OVExtender will be installed to an "install" subfolder of your build directory.

#### Mac OS X ####
Open CMake, click "Browse Source," and select your OVExtender source folder. Also specify a separate build folder if one is not filled in automatically for you. Click "Configure" and select either "Unix Makefiles" or "Xcode" as the target. If all goes well, then click Generate to create a project or makefile in the build directory.

See the Linux section below for information on building from Unix Makefiles.

**Note: if you choose to create an Xcode project, ensure that the build target is set to 32-bit. Otherwise, build errors may occur or an incorrect binary will be built.**

By default, OVExtender will be installed to an "install" subfolder of your build directory.

#### Linux ####

You can use a predefined environment to build the extender using docker.

If you are running on an ARM systen, be sure to use something like https://github.com/tonistiigi/binfmt to emulate an i386 system.

To build the container, run:

	cd docker-env
	docker build -t ovenv -f linux.Dockerfile .
	cd ..

Then to enter the build environment:

	docker run -v`pwd`:/build -it ovenv /bin/bash

Run the following inside the container:

	mkdir build
	cd build
	cmake -B. ../
	make

Then copy all the .so files as well as the plugins folder to your target location.

Running
-------

#### Windows ####
Copy the contents of your "bin" folder (see Compiling above) to the same folder as your Marble Blast Gold executable. Next, you will need to patch the game EXE so that it loads the extender. To do this, open a command prompt in that folder and run the following command (assuming that your EXE is named marbleblast.exe and you want an output EXE named marbleblast_ex.exe):
```
MBGPatcher marbleblast.exe marbleblast_ex.exe
```
This will copy your executable to a marbleblast_ex.exe file and then modify it so that it loads the extender on launch. Double-click on it to run the game.

#### Mac OS X ####
PluginLoader.dylib, TorqueLib.dylib, and the plugins folder must be copied to the Contents/MacOS folder inside the game's application package. Edit the game's Info.plist to change the executable that is launched, re-copy the game to your Applications folder if necessary, and launch it normally.

#### Linux ####
PluginLoader.so, TorqueLib.so, and the plugins folder must be present in the same folder as your executable. Then you can run with:

	export LD_LIRBARY_PATH=`pwd`
	LD_LIBRARY_PRELOAD=PluginLoader.so ./onverse.bin

Included Plugins
----------------

+ *FrameRateUnlock* - Unlocks the game's frame rate
+ *ExternalConsole* - Creates an external console window for the game
+ *OVOnlinePlugin* - Unlocks online server functionality and interop with ovopen-frontend
+ *OVToolServer* - Runs a "tool" server intended to compile scripts
+ *TestPlugin* - Used to test various features

Creating a New Plugin
---------------------

*TODO: fill this in with a basic tutorial or something*


#### Licensing ####

All MBExtender code should be considered MIT licensed. However, everything in the plugins/OVOnlinePlugin and plugins/OVToolServer folders is licensed under the AGPL license.


