cmake_minimum_required (VERSION 2.8.8)

include (CheckIncludeFiles)
project (MBExtender C CXX)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(BOOST_DIR "/home/linuxbrew/.linuxbrew/Cellar/boost/1.71.0" CACHE PATH "boost location")

set(OVOPEN_FRONTEND_LOCATION "./ext/ovopen-frontend" CACHE PATH "ovopen-frontend location")

if (UNIX)
	# Set Unix-specific flags
	set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-unused-variable -fvisibility=hidden")
	enable_language (ASM)

	# Force 32 bit
	set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -m32")
	set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -static-libstdc++ -m32")
	set (CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} -m32")
	set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -m32")
	set (CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -m32")
	
	if (APPLE)
		# Use a 32-bit compiler
		SET(CMAKE_XCODE_ATTRIBUTE_ARCHS "\$(ARCHS_STANDARD_32_BIT)")
	endif()
elseif (MSVC)
	# Set VC++ settings
	cmake_policy (SET CMP0054 NEW) # CMake complains otherwise when we call enable_language
	enable_language (ASM_MASM)
	set (CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} /SAFESEH:NO") # SAFESEH needs to be disabled because LDE64 doesn't support it

	# Force use of static runtime library
	set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MTd")
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
	set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} /MTd")
	set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} /MT")
elseif (WIN32)
	message("WIN32")
	set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-unused-variable -fvisibility=hidden")
	enable_language (ASM)

	# Force 32 bit
	set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -m32")
	set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m32")
	set (CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} -m32")
	set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -m32 -static-libgcc -static-libstdc++")
	set (CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -m32 -static-libgcc -static-libstdc++")
	
	add_definitions(-DWIN32 -DWINDOWS -D_WINDOWS -D_WIN32_WINNT=0x0601)

	if (ASIO_NO_PTHREADS)
	add_definitions(-DASIO_HAS_THREADS)
	endif ()

endif ()

if (ASSUME_MODERN_CPU)
add_definitions(-DASSUME_MODERN_CPU)
endif()

# Set OS X dylibs to be loaded from the executable directory
set (CMAKE_MACOSX_RPATH ON)
set (CMAKE_BUILD_WITH_INSTALL_RPATH ON)
set (CMAKE_INSTALL_NAME_DIR "@executable_path")

# Check for cpuid.h or intrin.h in order to use cpuid
#set(CMAKE_REQUIRED_FLAGS "-m32")
check_include_files (cpuid.h HAVE_CPUID_H)
check_include_files (intrin.h HAVE_INTRIN_H)
if (NOT HAVE_CPUID_H AND NOT HAVE_INTRIN_H)
	message (FATAL_ERROR "Either cpuid.h or intrin.h is required.")
endif ()

# Generate a config.h file
configure_file(${PROJECT_SOURCE_DIR}/config.h.in ${PROJECT_BINARY_DIR}/config.h)
add_definitions(-DHAVE_CONFIG_H -DASIO_STANDALONE)# -DOV_NO_ENCRYPTION)

# Register public include folder and build folder
include_directories (include ${PROJECT_BINARY_DIR} src/PluginLoader ${OVOPEN_FRONTEND_LOCATION} ${OVOPEN_FRONTEND_LOCATION}/lib/catch/include ${OVOPEN_FRONTEND_LOCATION}/lib/asio/include ${BOOST_DIR} ${OVOPEN_FRONTEND_LOCATION}/lib/crypt/include ${OVOPEN_FRONTEND_LOCATION}/lib/zlib plugins)

# Launcher (Mac/Linux only)
if (UNIX)
	add_executable (MBExtender src/MBExtender/main-unix.cpp)
endif ()

# MBGPatcher (Windows only)
if (WIN32)
	add_executable (MBGPatcher
		src/MBGPatcher/main-win32.cpp
		src/MBGPatcher/PEFile.cpp
		src/MBGPatcher/PEFile.h
	)
endif ()

# Plugin loader
set (PLUGINLOADER_SRC
	src/PluginLoader/BasicPluginInterface.cpp
	src/PluginLoader/CodeAllocator.cpp
	src/PluginLoader/CodeInjectionStream.cpp
	src/PluginLoader/FuncInterceptor.cpp
	src/PluginLoader/PluginLoader.cpp
	src/PluginLoader/TrampolineGenerator.cpp
	src/PluginLoader/Filesystem-common.cpp
	src/PluginLoader/StringUtil.cpp

	include/PluginLoader/PluginInterface.h
	src/PluginLoader/BasicPluginInterface.h
	src/PluginLoader/CodeAllocator.h
	src/PluginLoader/CodeInjectionStream.h
	src/PluginLoader/FuncInterceptor.h
	src/PluginLoader/TrampolineGenerator.h
	src/PluginLoader/Filesystem.h
	src/PluginLoader/StringUtil.h
)
if (UNIX)
	set (PLUGINLOADER_SRC ${PLUGINLOADER_SRC}
		src/PluginLoader/unix/main-unix.cpp
		src/PluginLoader/unix/Memory-unix.cpp
		src/PluginLoader/unix/SharedObject-unix.cpp
		src/PluginLoader/unix/Filesystem-unix.cpp
		src/PluginLoader/unix/LDE64-as.s
	)
elseif (WIN32)
    add_definitions(-D_WIN32_WINNT=0x0501)

	if (USING_MINGW32)
		set (PLUGINLOADER_SRC ${PLUGINLOADER_SRC}
			src/PluginLoader/unix/LDE64-as.s)
	else ()
		set (PLUGINLOADER_SRC ${PLUGINLOADER_SRC}
			src/PluginLoader/win32/LDE64-masm.asm)
	endif ()

	set (PLUGINLOADER_SRC ${PLUGINLOADER_SRC}
		src/PluginLoader/win32/main-win32.cpp
		src/PluginLoader/win32/Memory-win32.cpp
		src/PluginLoader/win32/SharedObject-win32.cpp
		src/PluginLoader/win32/Filesystem-win32.cpp
	)
endif ()
add_library (PluginLoader SHARED ${PLUGINLOADER_SRC})
if (MSVC)
	set_property (TARGET PluginLoader APPEND PROPERTY LINK_FLAGS "/DEF:\"${PROJECT_SOURCE_DIR}/src/PluginLoader/win32/PluginLoader.def\"")
endif ()

# TorqueLib
set (TORQUELIB_SRC
	src/TorqueLib/TorqueLib.cpp
	src/TorqueLib/math/mAngAxis.cpp
	src/TorqueLib/math/mathUtils.cpp
	src/TorqueLib/math/mBox.cpp
	src/TorqueLib/math/mEase.cpp
	src/TorqueLib/math/mMath_C.cpp
	src/TorqueLib/math/mMathAMD.cpp
	src/TorqueLib/math/mMathSSE.cpp
	src/TorqueLib/math/mMatrix.cpp
	src/TorqueLib/math/mOrientedBox.cpp
	src/TorqueLib/math/mPlane.cpp
	src/TorqueLib/math/mPlaneTransformer.cpp
	src/TorqueLib/math/mPoint.cpp
	src/TorqueLib/math/mQuat.cpp
	src/TorqueLib/math/mRandom.cpp
	src/TorqueLib/math/mRect.cpp
	src/TorqueLib/math/mSolver.cpp
	src/TorqueLib/math/mSphere.cpp
	src/TorqueLib/math/util/quadTransforms.cpp
	src/TorqueLib/util/internalStream.cpp
	src/TorqueLib/util/internalBitStream.cpp

	include/TorqueLib/TGE.h
	include/TorqueLib/QuickOverride.h
	include/TorqueLib/platform/platform.h
	include/TorqueLib/platform/platformAssert.h
	include/TorqueLib/math/mAngAxis.h
	include/TorqueLib/math/mathUtils.h
	include/TorqueLib/math/mBox.h
	include/TorqueLib/math/mBoxBase.h
	include/TorqueLib/math/mConstants.h
	include/TorqueLib/math/mEase.h
	include/TorqueLib/math/mMath.h
	include/TorqueLib/math/mMathFn.h
	include/TorqueLib/math/mMatrix.h
	include/TorqueLib/math/mOrientedBox.h
	include/TorqueLib/math/mPlane.h
	include/TorqueLib/math/mPlaneSet.h
	include/TorqueLib/math/mPlaneTransformer.h
	include/TorqueLib/math/mPoint2.h
	include/TorqueLib/math/mPoint3.h
	include/TorqueLib/math/mPoint4.h
	include/TorqueLib/math/mQuat.h
	include/TorqueLib/math/mRandom.h
	include/TorqueLib/math/mRandomDeck.h
	include/TorqueLib/math/mRandomSet.h
	include/TorqueLib/math/mRect.h
	include/TorqueLib/math/mSilhouetteExtractor.h
	include/TorqueLib/math/mSphere.h
	include/TorqueLib/math/mTransform.h
	include/TorqueLib/math/util/quadTransforms.h
	include/TorqueLib/util/tVector.h
)
if (WIN32)
	set (TORQUELIB_SRC ${TORQUELIB_SRC}
		include/TorqueLib/win32/Addresses-win32.h
		include/TorqueLib/win32/InterfaceMacros-win32.h
	)
elseif (APPLE)
	set (TORQUELIB_SRC ${TORQUELIB_SRC}
		include/TorqueLib/osx/Addresses-osx.h
		include/TorqueLib/linux/InterfaceMacros-linux.h
	)
elseif (UNIX)
	set (TORQUELIB_SRC ${TORQUELIB_SRC}
		include/TorqueLib/linux/Addresses-linux.h
		include/TorqueLib/linux/InterfaceMacros-linux.h
	)
endif()
add_library (TorqueLib STATIC ${TORQUELIB_SRC})

set_property (TARGET TorqueLib APPEND PROPERTY INCLUDE_DIRECTORIES ${PROJECT_SOURCE_DIR}/include/TorqueLib/math ${PROJECT_SOURCE_DIR}/include/TorqueLib/math/util)

# TestPlugin
add_library (TestPlugin MODULE plugins/TestPlugin/TestPlugin.cpp)
target_link_libraries (TestPlugin TorqueLib)
set_target_properties(TestPlugin PROPERTIES LIBRARY_OUTPUT_DIRECTORY "plugins/")

# Zlib
set(ZLIB_SRCS
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/zutil.h
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/inftrees.h
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/inflate.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/compress.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/deflate.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/inffixed.h
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/trees.h
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/inffast.h
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/crc32.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/infback.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/zutil.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/deflate.h
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/zlib.h
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/inflate.h
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/inftrees.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/uncompr.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/trees.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/crc32.h
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/gzio.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/inffast.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/adler32.c
	${OVOPEN_FRONTEND_LOCATION}/lib/zlib/zconf.h
)
add_library(zlib STATIC ${ZLIB_SRCS})

# Shared onverse
set(OVFrontendShared_SRCS
	${OVOPEN_FRONTEND_LOCATION}/ovopen/packet.cpp
	${OVOPEN_FRONTEND_LOCATION}/ovopen/packetHandlers.cpp
	${OVOPEN_FRONTEND_LOCATION}/ovopen/protocolHandler.cpp
	${OVOPEN_FRONTEND_LOCATION}/ovopen/log.cpp
	${OVOPEN_FRONTEND_LOCATION}/ovopen/coreUtil.cpp
	${OVOPEN_FRONTEND_LOCATION}/ovopen/statsController.cpp
	${OVOPEN_FRONTEND_LOCATION}/lib/crypt/blowfish.c
)

# OVOnlinePlugin 
set(OvOnlinePlugin_SRCS
	plugins/OVOnlinePlugin/OVClientBase.cpp
	plugins/OVOnlinePlugin/OVClientBase.h
	plugins/OVOnlinePlugin/OVOnlineClient.cpp
	plugins/OVOnlinePlugin/OVOnlineClient.h
	plugins/OVOnlinePlugin/OVOnlineConsoleFunctions.cpp
	plugins/OVOnlinePlugin/OVOnlinePlugin.cpp
	plugins/OVOnlinePlugin/OVOnlinePlugin.h
	plugins/OVOnlinePlugin/OVRPCHandlers.cpp
	plugins/OVOnlinePlugin/OVRPCHandlers.h
)
add_library (OVOnlinePlugin MODULE ${OvOnlinePlugin_SRCS} ${OVFrontendShared_SRCS})
set_target_properties(OVOnlinePlugin PROPERTIES LIBRARY_OUTPUT_DIRECTORY "plugins/")

if (WIN32)
target_link_libraries (TorqueLib ws2_32 wsock32)
target_link_libraries (OVOnlinePlugin TorqueLib zlib ws2_32 wsock32)
else()
target_link_libraries (OVOnlinePlugin TorqueLib zlib)
endif()

# OVToolserver 
add_library (OVToolServer MODULE 
	plugins/OVToolServer/OVToolServer.cpp 
	plugins/OVOnlinePlugin/OVOnlineClient.cpp
	plugins/OVOnlinePlugin/OVOnlineClient.h 
	plugins/OVOnlinePlugin/OVClientBase.cpp 
	${OVFrontendShared_SRCS})

if (WIN32)
target_link_libraries (OVToolServer TorqueLib zlib ws2_32)
else()
target_link_libraries (OVToolServer TorqueLib zlib)
endif()
set_target_properties(OVToolServer PROPERTIES LIBRARY_OUTPUT_DIRECTORY "plugins/")

# ExternalConsole
add_library (ExternalConsole MODULE plugins/ExternalConsole/ExternalConsole.cpp)
target_link_libraries (ExternalConsole TorqueLib)
set_target_properties(ExternalConsole PROPERTIES LIBRARY_OUTPUT_DIRECTORY "plugins/")

# FrameRateUnlock
set (FRAMERATEUNLOCK_SRC
	plugins/FrameRateUnlock/FrameRateUnlock.cpp
	plugins/FrameRateUnlock/GameTimer.hpp
)
if (WIN32)
	set (FRAMERATEUNLOCK_SRC ${FRAMERATEUNLOCK_SRC}
		plugins/FrameRateUnlock/win32/HighPerformanceTimer-win32.cpp
		plugins/FrameRateUnlock/win32/HighPerformanceTimer-win32.hpp
		plugins/FrameRateUnlock/win32/MultimediaTimer-win32.cpp
		plugins/FrameRateUnlock/win32/MultimediaTimer-win32.hpp
	)
elseif (APPLE)
	set (FRAMERATEUNLOCK_SRC ${FRAMERATEUNLOCK_SRC}
		plugins/FrameRateUnlock/osx/MachTimer-osx.cpp
		plugins/FrameRateUnlock/osx/MachTimer-osx.hpp
	)
elseif (UNIX)
	set (FRAMERATEUNLOCK_SRC ${FRAMERATEUNLOCK_SRC}
		plugins/FrameRateUnlock/linux/MonotonicTimer-linux.cpp
		plugins/FrameRateUnlock/linux/MonotonicTimer-linux.hpp
	)
endif ()
add_library (FrameRateUnlock MODULE ${FRAMERATEUNLOCK_SRC})
target_link_libraries (FrameRateUnlock TorqueLib)
set_target_properties(FrameRateUnlock PROPERTIES LIBRARY_OUTPUT_DIRECTORY "plugins/")
if (WIN32)
	target_link_libraries (FrameRateUnlock winmm)
endif ()

# Remove the "lib" prefix from libraries
set_target_properties (PluginLoader TorqueLib TestPlugin OVOnlinePlugin OVToolServer ExternalConsole FrameRateUnlock PROPERTIES PREFIX "")

# Install rules 
if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
	set (CMAKE_INSTALL_PREFIX "${PROJECT_BINARY_DIR}/install" CACHE PATH "default install path" FORCE)
endif()
install (TARGETS PluginLoader RUNTIME DESTINATION bin LIBRARY DESTINATION bin)
install (TARGETS TorqueLib RUNTIME DESTINATION bin LIBRARY DESTINATION bin ARCHIVE DESTINATION lib)
install (TARGETS TestPlugin ExternalConsole FrameRateUnlock OVOnlinePlugin OVToolServer DESTINATION bin/plugins)
if (WIN32)
	install (TARGETS MBGPatcher DESTINATION bin)
elseif (UNIX)
	install (TARGETS MBExtender DESTINATION bin)
endif ()
install (DIRECTORY include/ DESTINATION include FILES_MATCHING PATTERN "*.h")
