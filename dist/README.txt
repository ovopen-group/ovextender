ovopen patched client
=====================

This patched client allows for several things:

1) Can run as a location server
2) Can compile scripts
3) Can run additional plugins to customize things further

Setting up
==========

Grab all your data files from an existing onverse installation and overwrite  
the files in onverse/data with the equivalent files from your installation.

Running a location server
=========================

A batch file is provided to run a location server. Note that by default 
the location server will run in offline mode - i.e. it won't try to connect to 
a frontend server. This mode is more useful for testing gameplay code and building solo.

Simply run:

	testLocationServer.bat


Running a client
================

To run the patched client, simply run:

	launchClient.bat

Normally the client will need a frontend server to connect to. However 
a script is provided that will connect to a location server.

Press the tilde key (~) to open up the console and enter the following:

	exec("onverse/client/testConnect.cs")

Then press return. Your client should then attempt to connect to the local location server.


